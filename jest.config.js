module.exports = {
  roots: ['<rootDir>/test'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    "\\.(gql|graphql)$": "jest-transform-graphql",
  },
  testEnvironment: 'node',
  testRegex: '((\\.|/)(test|spec))\\.ts?$',
  collectCoverageFrom: [
    '<rootDir>/src/services/**',
    '<rootDir>/src/dataStores/**',
    '<rootDir>/src/gql/**',
    '<rootDir>/src/Config.ts',
  ],
  moduleFileExtensions: ['ts', 'js', 'jsx', 'json', 'node'],
};