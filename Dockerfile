# start from our private alpine + node image with the AAAA DNS lookup disabled
FROM docker-private.smartdrivesystems.com/sd-alpine-node-image:12.16.2

#Always install BASH and TINI for alpine (cant live without it)
RUN apk add --no-cache bash
RUN apk add --no-cache tini

# Tini is now available at /sbin/tini
ENTRYPOINT ["/sbin/tini", "--"]

# Copy source code
COPY . /app

# Change working directory
WORKDIR /app

# Expose API ports to the outside
EXPOSE 80

# Launch webpack application
CMD ["node", "dist/server.js"]