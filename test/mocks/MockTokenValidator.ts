import { TokenValidator, AccessToken } from '@sd-microservice-base/security';
export class MockTokenValidator extends TokenValidator {
  getRequestTokens(req: any): AccessToken {
    return {
      rawToken: '123',
      issuer: 'AuthenticationService',
      companyHash: 'mockCompanyHash',
      username: 'mockUser',
      audience: 'https://tap-service.smartdrivesystems.com',
    };
  }
}
