import { TestConstants, InducedTestError } from './../TestConstants';
import { Company } from '../../src/models/Company';
import { injectable } from 'inversify';
import { CompanyDataStore } from '../../src/dataStores/CompanyDataStore';

@injectable()
export class CompanyDataStoreMock implements CompanyDataStore {
  public getCompanies = async (companyIds: string[]): Promise<Company[]> => {
    if (companyIds && companyIds.indexOf(TestConstants.FailingCompanyId) >= 0) {
      throw new InducedTestError('Company data store was sent failure company ID');
    }

    const companies: Company[] = [];
    companyIds.forEach(companyId => {
      companies.push({
        CompanyName: 'mockCompany',
        EntityIdentityKey: Number(companyId),
      });
    });
    return companies;
  };
}
