import { injectable } from 'inversify';
import { UserDataStoreSql } from '../../src/dataStores/UserDataStoreSql';
import { PeopleLoginCredentialDataResponse } from '../../src/models/ElasticSearch/PeopleLoginCredentialDataResponse';
import { User } from '../../src/models/User';
import * as sql from 'mssql';

@injectable()
export class UserDataStoreSqlImplMock implements UserDataStoreSql {
  public async getPeopleLoginCredential(
    companyHash: string,
    username: string
  ): Promise<PeopleLoginCredentialDataResponse> {
    throw new Error('Method not implemented.');
  }
  public async getUser(companyHash: string, username: string): Promise<User> {
    return new User(username, '1', '1', '666', 1);
  }
  gatherUserRoles = async (user: User, pool: sql.ConnectionPool): Promise<void> => {};
}
