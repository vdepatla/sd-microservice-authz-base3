import { Site } from "../../src/models/Site";
import { injectable } from "inversify";
import { SiteDataStore } from "../../src/dataStores/SiteDataStore";

@injectable()
export class SiteDataStoreMock implements SiteDataStore {
  getSites = async (siteIds: string[]): Promise<Site[]> => {
    try {
        return [
          {
            siteId: "1",
            siteName: "one"
          },
          {
            siteId: "2",
            siteName: "two"
          },
          {
            siteId: "3",
            siteName: "three"
          },
          {
            siteId: "4",
            siteName: "four"
          },
          {
            siteId: "5",
            siteName: "five"
          }
        ];
    } catch (err) {
      throw err;
    }
  }
  
  public getSiteWithChildren = async (
    companyId: string,
    siteId: string
  ): Promise<Site[]> => {
    const functionName = "SiteDataStoreMock.getSiteWithChildren";
    try {
      if (companyId === "666" || siteId === "666") {
        throw new Error(`mocked ${functionName} exception`);
      } else if (companyId === "999" || siteId === "999") {
        return [];
      } else {
        return [
          {
            siteId: "1",
            siteName: "one"
          },
          {
            siteId: "2",
            siteName: "two"
          },
          {
            siteId: "3",
            siteName: "three"
          },
          {
            siteId: "4",
            siteName: "four"
          },
          {
            siteId: "5",
            siteName: "five"
          }
        ];
      }
    } catch (err) {
      throw err;
    }
  };
}
