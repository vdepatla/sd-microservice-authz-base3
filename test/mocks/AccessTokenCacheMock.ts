import { injectable } from 'inversify';
import { AccessTokenCache } from '../../src/dataStores/AccessTokenCache';
import { AccessTokenResponse } from '../../src/models/AccessTokenResponse';
import { AccessTokenCachedResponse } from '../../src/models/AccessTokenCachedResponse';

@injectable()
export class AccessTokenCacheMock implements AccessTokenCache {
  public getAccessToken(key: string, fallbackFunction: () => Promise<AccessTokenResponse>): Promise<string> {
    return Promise.resolve('mock');
  }
}
