import { TestConstants, InducedTestError } from '../TestConstants';
import { Vehicle } from "../../src/models/Vehicle";
import { injectable } from "inversify";
import { VehicleDataStore } from "../../src/dataStores/VehicleDataStore";

@injectable()
export class VehicleDataStoreMock implements VehicleDataStore {
  public getVehicles = async (vehicleIds: string[]): Promise<Vehicle[]> => {
    if (vehicleIds && vehicleIds.indexOf(TestConstants.FailingVehicleId) >= 0) {
      throw new InducedTestError('Vehicle data store was passed failure vehicle ID');
    }
    const functionName = "VehicleDataStoreMock.getVehicles";
    try {
      return vehicleIds.map(vehicleId => {
        if (vehicleId === "666") {
          throw new Error(`mocked ${functionName} exception`);
        } else if (vehicleId === "999") {
          return undefined;
        } else {
          return {
            vehicleId,
            vehicleSn: `mocked vehicle ${vehicleId} SN`,
            vehicleVin: `mocked vehicle ${vehicleId} VIN`,
            siteId: vehicleId
          };
        }
      });
    } catch (err) {
      throw err;
    }
  };
}
