import { injectable } from 'inversify';
import { Auth0DataStore } from '../../src/dataStores/Auth0DataStore';
@injectable()
export class Auth0DataStoreMock implements Auth0DataStore {
  getM2MToken(): Promise<import('../../src/models/AccessTokenResponse').AccessTokenResponse> {
    throw new Error('Method not implemented.');
  }
}
