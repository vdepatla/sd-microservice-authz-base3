import { injectable } from 'inversify';
import { AccessDataStore } from '../../src/dataStores/AccessDataStore';
import { User } from '../../src/models/User';
import { AccessResult } from '../../src/models/AccessResult';
import { Site } from '../../src/models/Site';
@injectable()
export class AccessDataStoreMock implements AccessDataStore {
  canUserAccessCompanies(companyIds: string[], userInfo: User, superuser: boolean): Promise<AccessResult[]> {
    throw new Error('Method not implemented.');
  }
  canUserAccessSites(siteIds: string[], childSites: Site[], superuser: boolean): Promise<AccessResult[]> {
    throw new Error('Method not implemented.');
  }
  canUserAccessVehicles(vehicleIds: string[], childSites: Site[], superuser: boolean): Promise<AccessResult[]> {
    throw new Error('Method not implemented.');
  }
  canUserAccessSmartRecorders(
    smartRecorderIds: string[],
    smartRecorderSns: string[],
    childSites: Site[],
    superuser: boolean
  ): Promise<AccessResult[]> {
    throw new Error('Method not implemented.');
  }
}
