import { User } from '../../src/models/User';
import { injectable } from 'inversify';
import { UserDataStore } from '../../src/dataStores/UserDataStore';
import { PeopleLoginCredentialDataResponse } from '../../src/models/ElasticSearch/PeopleLoginCredentialDataResponse';

@injectable()
export class UserDataStoreMock implements UserDataStore {
  getPeopleLoginCredential(companyHash: string, username: string): Promise<PeopleLoginCredentialDataResponse> {
    return Promise.resolve({
      userName: 'mock',
      companyHash: 'mock',
      companySeed: 'mock',
      siteHash: 'mock',
      isLocked: false,
      companyId: 1,
      peopleId: 1,
      site_isActive: true,
      siteId: 1,
      people_isActive: true,
      createdDate: new Date(),
      emailAddress: 'email',
      role_list: '1,2,3',
      peopleHash: 'mock',
      company_isActive: 'mock',
    });
  }
  public getUser = async (companyHash: string, username: string): Promise<User> => {
    const functionName = 'UserDataStoreMock.getUser';
    try {
      if (username === 'error') {
        throw new Error(`mocked ${functionName} exception`);
      } else if (username === 'notfound') {
        return undefined;
      } else if (username === 'badsite') {
        const retVal = new User(username, '1', '1', '666', 1);
        retVal.roles.push('DRI');
        return retVal;
      } else {
        const retVal = new User(username, '1', '1', '1', 1);
        if (username === 'superuser') {
          retVal.roles.push('SDPM');
        }
        retVal.roles.push('DRI');
        return retVal;
      }
    } catch (error) {
      throw error;
    }
  };
}
