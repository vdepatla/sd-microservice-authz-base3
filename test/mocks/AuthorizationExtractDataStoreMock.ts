import { AuthorizationExtractDataStore } from '../../src/dataStores/AuthorizationExtractDataStore';
import { ExtractRefreshTime } from '../../src/models/ExtractRefreshTime';
import { injectable } from 'inversify';
import moment = require('moment');
import { InducedTestError } from '../../test/TestConstants';

@injectable()
export class AuthorizationExtractDataStoreMock implements AuthorizationExtractDataStore {
  public async getLastRefreshTime(indices: string[]): Promise<ExtractRefreshTime[]> {
    const result: ExtractRefreshTime[] = [];
    indices.forEach(element => {
      if (element === 'error') {
        throw Promise.reject(new InducedTestError());
      }
      result.push({
        index: element,
        refreshTimeUnix: moment('2000-02-01T00:00:00Z').unix(),
      });
    });
    return Promise.resolve(result);
  }
}
