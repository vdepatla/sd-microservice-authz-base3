import { injectable } from 'inversify';
import { LoginEnabled } from '../../src/models/LoginEnabled';
import { AuthorizationResult } from '../../src/models/AuthorizationResult';
import { AuthorizationDataStoreSql } from '../../src/dataStores/AuthorizationDataStoreSql';
@injectable()
export class AuthorizationDataStoreSqlImplMock implements AuthorizationDataStoreSql {
  public async getPermissions(
    companyHash: string,
    username: string,
    resources: string[]
  ): Promise<AuthorizationResult[]> {
    return [
      {
        resource: 'mockSqlResourceSQL',
        role: 'mockRoleSQL',
        permission: 'mockPermissionSQL',
        userScope: 'mockUserScopeSQL',
        permissionScope: 'mockPermissionScopeSQL',
      },
    ];
  }
  public async getLoginEnabled(companyHash: string, username: string): Promise<LoginEnabled> {
    return undefined;
  }
}
