import { injectable } from 'inversify';
import { ExtractRefreshTime } from '../../src/models/ExtractRefreshTime';
import { ExtractRefreshTimeCache } from '../../src/dataStores/ExtractRefreshTimeCache';
import moment = require('moment');
import { InducedTestError } from '../../test/TestConstants';

@injectable()
export class ExtractRefreshTimeCacheMock implements ExtractRefreshTimeCache {
  public getExtractRefreshTimes(keys: string[]): Promise<ExtractRefreshTime[]> {
    const result: ExtractRefreshTime[] = [];
    keys.forEach(element => {
      if (element === 'error') {
        throw Promise.reject(new InducedTestError());
      }
      result.push({
        index: element,
        refreshTimeUnix: moment('2000-02-01T00:00:00Z').unix(),
      });
    });
    return Promise.resolve(result);
  }

  public setExtractRefreshTimes(refreshTimes: ExtractRefreshTime[]): Promise<boolean> {
    return Promise.resolve(true);
  }
}
