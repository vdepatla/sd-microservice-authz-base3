import { TestConstants, InducedTestError } from '../TestConstants';
import { SmartRecorder } from '../../src/models/SmartRecorder';
import { injectable } from 'inversify';
import { SmartRecorderDataStore } from '../../src/dataStores/SmartRecorderDataStore';

@injectable()
export class SmartRecorderDataStoreMock implements SmartRecorderDataStore {
  public getSmartRecorders = async (srIds: string[], srSns: string[]): Promise<SmartRecorder[]> => {
    if (srIds && srIds.indexOf(TestConstants.FailingSmartRecorderId) >= 0) {
      throw new InducedTestError('Smart recorder data store was passed failure SR ID');
    }

    const functionName = 'SmartRecorderDataStoreMock.getSmartRecorders';
    try {
      const srs: SmartRecorder[] = [];
      if (srIds !== undefined && srIds !== null) {
        const srsById = srIds.map(srId => {
          if (srId === '666') {
            throw new Error(`mocked ${functionName} exception`);
          } else if (srId === '999') {
            return undefined;
          } else {
            return {
              srId,
              srSn: `mocked SR ${srId} SN`,
              siteId: srId,
            };
          }
        });
        srs.push(...srsById);
      }
      if (srSns !== undefined && srSns !== null) {
        const srsBySn = srSns.map(srSn => {
          if (srSn === '666') {
            throw new Error(`mocked ${functionName} exception`);
          } else if (srSn === '999') {
            return undefined;
          } else {
            return {
              srId: `mocked SR ${srSn} ID`,
              srSn,
              siteId: srSn,
            };
          }
        });
        srs.push(...srsBySn);
      }
      return srs;
    } catch (err) {
      throw err;
    }
  };
}
