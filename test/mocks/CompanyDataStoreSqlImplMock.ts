import { Company } from '../../src/models/Company';
import { injectable } from 'inversify';
import { CompanyDataStoreSql } from '../../src/dataStores/CompanyDataStoreSql';

@injectable()
export class CompanyDataStoreSqlImplMock implements CompanyDataStoreSql {
  public async getCompanies(companyIds: string[]): Promise<Company[]> {
    const companies: Company[] = [];
    companyIds.forEach(companyId => {
      companies.push({
        CompanyName: 'mockCompanySQL',
        EntityIdentityKey: Number(companyId),
      });
    });
    return companies;
  }
}
