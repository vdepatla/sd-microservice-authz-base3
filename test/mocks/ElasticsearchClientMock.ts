import { injectable, inject } from 'inversify';
import { Client } from '@elastic/elasticsearch';
import { ElasticsearchClient } from '../../src/models/ElasticSearch/ElasticsearchClient';

@injectable()
export class ElasticsearchClientMock implements ElasticsearchClient {
  public async getClient(): Promise<Client> {
    // forget about all the token business and settings - this client will be mocked in the tests
    return new Client();
  }
}
