import { ResourceAccessRule } from '@sd-microservice-base/security';
import { LogLevel, getLogLevelFromString, Log } from '@sd-microservice-base/instrumentation';
import { Config } from './../../src/Config';
import * as dotenv from 'dotenv';
import { ApplicationLogs } from '../../src/models/ApplicationLogs';
const fs = require('fs');

const config = dotenv.parse(fs.readFileSync('./test/.env-test'));

export class MockConfig extends Config {
  protected logLevel: LogLevel;

  private useSqlOnly: boolean;

  /** Microservice Config */

  /**
   * The environment to run as (this will map to k8s namespace).
   */
  public get ServiceEnvironment(): string {
    return config.SERVICE_ENVIRONMENT;
  }

  /**
   * The name of the service.
   */
  public get ServiceName(): string {
    return config.SERVICE_NAME;
  }

  /**
   * The service version.
   */
  public get ServiceVersion(): string {
    return config.SERVICE_VERSION;
  }

  /** SQL Config */
  public get SQLUser(): string {
    return config.DB_USERNAME;
  }

  public get SQLPassword(): string {
    return config.DB_PASSWORD;
  }

  public get SQLDatabase(): string {
    return config.DB_NAME;
  }

  public get SQLUserDomain(): string {
    return config.DB_USER_DOMAIN;
  }

  public get SQLServer(): string {
    return config.DB_SERVER;
  }

  /** Redis Config */

  /**
   * The redis host address
   */
  public get RedisHost(): string {
    return config.REDIS_HOST;
  }

  /**
   * The redis port number
   */
  public get RedisPort(): number {
    return parseInt(config.REDIS_PORT, 10);
  }

  /**
   * The redis cache TTL in seconds
   */
  public get RedisCacheTTLSeconds(): number {
    return parseInt(config.REDIS_CACHE_TTL_SECONDS, 10);
  }

  /**
   * The redis namespace
   */
  public get RedisNamespace(): string {
    return config.REDIS_NAMESPACE;
  }

  /**
   * The max retries for redis
   */
  public get MaxRetries(): number {
    return parseInt(config.REDIS_MAX_RETRIES, 10);
  }

  /** Security Config */

  /**
   * Signing secret of the TAP API family
   */
  public get JwksUri(): string {
    return config.JWKS_URI;
  }

  /**
   * Native API Audience - uniquely identifies the API
   */
  public get AuthNAudience(): string {
    return config.AUTHN_AUDIENCE;
  }

  /**
   * Auth0 API Audience - uniquely identifies the API
   */
  public get Auth0Audience(): string {
    return config.AUTH0_AUDIENCE;
  }

  /**
   * Native Authentication issuer
   */
  public get AuthNIssuer(): string {
    return config.AUTHN_ISSUER;
  }

  /**
   * Auth0 Authentication issuer
   */
  public get Auth0Issuer(): string {
    return config.AUTH0_ISSUER;
  }

  /**
   * Native Authentication signing key
   */
  public get AuthNSigningKey(): string {
    return config.AUTHN_SIGNING_KEY;
  }

  /**
   * Auth0 Client Id
   */
  public get Auth0ClientId(): string {
    return config.AUTH0_CLIENT_ID;
  }

  /**
   * Auth0 Request Timeout
   */
  public get Auth0RequestTimeout(): number {
    return Number(config.AUTH0_REQUEST_TIMEOUT);
  }

  /**
   * Auth0 Client Secret
   */
  public get Auth0SigningKey(): string {
    return config.AUTH0_SIGNING_KEY;
  }

  /**
   * Native Authorization service GQL URL
   */
  public get AuthorizationServiceGqlUrl(): string {
    return config.AUTHORIZATION_SERVICE_GQL_URL;
  }

  /** Elastic Search Config */
  public get UseSqlOnly(): boolean {
    return this.useSqlOnly;
  }

  public get ElasticDeltaRefreshDuartionMinutes(): number {
    return Number(config.ELASTIC_DELTA_REFRESH_DURATION_MINUTES);
  }

  public get ElasticFullRefreshDurationMinutes(): number {
    return Number(config.ELASTIC_FULL_REFRESH_DURATION_MINUTES);
  }

  public get ElasticIndexProcessExtractRefresh(): string {
    return config.ELASTIC_INDEX_PROCESS_EXTRACT_REFRESH;
  }

  public get ElasticDeltaRefreshTolerancePercentage(): number {
    return Number(config.ELASTIC_DELTA_REFRESH_TOLERANCE_PERCENTAGE);
  }

  public get ElasticFullRefreshTolerancePercentage(): number {
    return Number(config.ELASTIC_FULL_REFRESH_TOLERANCE_PERCENTAGE);
  }

  public get ExtractTimestampCacheTtlSeconds(): number {
    return Number(config.EXTRACT_TIMESTAMP_CACHE_TTL_SECONDS);
  }

  public get ElasticDeltaRefreshIndices(): string[] {
    return config.ELASTIC_DELTA_REFRESH_INDICES.split(',');
  }

  public get ElasticFullRefreshIndices(): string[] {
    return config.ELASTIC_FULL_REFRESH_INDICES.split(',');
  }

  public get ElasticDeltaRefreshMinutesWithTolerance(): number {
    const tolerance = this.ElasticDeltaRefreshTolerancePercentage / 100;
    return this.ElasticFullRefreshDurationMinutes + tolerance;
  }

  public get ElasticFullRefreshMinutesWithTolerance(): number {
    const tolerance = this.ElasticFullRefreshTolerancePercentage / 100;
    return this.ElasticFullRefreshDurationMinutes + tolerance;
  }

  /**
   * Elasticsearch server address
   */
  public get ElasticServer(): any {
    return config.ELASTIC_SERVER;
  }

  /**
   * Elastic search people_login_credential index
   */
  public get ElasticIndexPeopleLoginCredential(): string {
    return config.ELASTIC_INDEX_PEOPLE_LOGIN_CREDENTIAL;
  }

  /**
   * Elastic search role_permission index
   */
  public get ElasticIndexRolePermissions(): string {
    return config.ELASTIC_INDEX_ROLE_PERMISSIONS;
  }

  /**
   * Elastic search application_membership index
   */
  public get ElasticIndexApplicationMembership(): string {
    return config.ELASTIC_INDEX_APPLICATION_MEMBERSHIP;
  }

  /**
   * Elastic search company_hash index
   */
  public get ElasticIndexCompanyHash(): string {
    return config.ELASTIC_INDEX_COMPANY_HASH;
  }

  /**
   * Query limit for role_permission index query
   */
  public get RolePermissionsQueryLimit(): string {
    return config.ROLE_PERMISSIONS_QUERY_LIMIT;
  }

  /**
   * Query limit for company index query
   */
  public get CompanyQueryLimit(): string {
    return config.COMPANY_QUERY_LIMIT;
  }

  /**
   * Elastic search role_names index
   */
  public get ElasticIndexRoleNames(): string {
    return config.ELASTIC_INDEX_ROLE_NAMES;
  }

  /**
   * Query Size for role names index query
   */
  public get RoleNamesQueryLimit(): string {
    return config.ROLE_NAMES_QUERY_LIMIT;
  }

  public get ElasticMaxRetries(): number {
    return Number(config.ELASTIC_MAX_RETRIES);
  }

  public get ElasticRequestTimeout(): number {
    return Number(config.ELASTIC_REQUEST_TIMEOUT);
  }

  /** Express App Config */

  /**
   * The port the service will listen on.
   */
  public get ServicePort(): number {
    return parseInt(config.SERVICE_PORT, 10);
  }

  /**
   * The GQL endpoint.
   */
  public get GqlEndpoint(): string {
    return config.GQL_ENDPOINT;
  }

  /**
   * Enables GQL tracing.
   */
  public get EnableGqlTracing(): boolean {
    return this.toBoolean(config.ENABLE_GQL_TRACING, false);
  }

  /**
   * A set of rules to limit access to the service based on role, resource, and permission.
   * This is the primary way to configure role-based access control using the Base express application.
   *
   * The service will throw an error on startup of no rules are provided unless `DisableRoleBasedAuthorization` is set to `true`.
   */
  public get ServiceAccessRules(): ResourceAccessRule[] {
    return [];
  }

  /**
   * Disables RBAC. This is useful in development environments.
   * Setting this to `true` will disable startup errors thrown when access rules are missing.
   */
  public get DisableRoleBasedAuthorization(): boolean {
    return this.toBoolean(config.DISABLE_ROLE_BASED_AUTHORIZATION, false);
  }

  /**
   * Disable authentication. This is useful in development environments.
   * Setting this to `true` will disable token-based authentication and allow open access to the API.
   */
  public get DisableAuthentication(): boolean {
    return this.toBoolean(config.DISABLE_AUTHENTICATION, false);
  }

  /**
   * Optional distinct cache TTL for Apollo caching. Falls back to redis cache TTL in the Base GQL express microservice if not supplied.
   */
  public get ApolloCacheTTLSeconds(): number {
    return parseInt(config.APOLLO_CACHE_TTL_SECONDS, 10);
  }

  /**
   * Set to true to disable Apollo caching via Redis.
   */
  public get DisableApolloCaching(): boolean {
    return this.toBoolean(config.DISABLE_APOLLO_CACHING, false);
  }

  /**
   * The object which contains application logs. This config item is used to power the /logs rest endpoint.
   */
  public get ApplicationLogs(): { [key: string]: Log } {
    return ApplicationLogs;
  }

  public get MockGqlApi(): boolean {
    return this.toBoolean(config.MOCK_GQL_API, false);
  }

  /**
   * The initial logging level to start up with.
   */
  public get LogLevel(): LogLevel {
    if (!this.logLevel) {
      this.logLevel = getLogLevelFromString(config.LOG_LEVEL || 'info');
    }

    return this.logLevel;
  }

  /**
   * Updates the current log level.
   */
  public updateLogLevel(newLevel: LogLevel) {
    this.logLevel = newLevel;
  }

  constructor(useSqlOnly?: boolean) {
    super();
    this.useSqlOnly = useSqlOnly || false;
  }
}
