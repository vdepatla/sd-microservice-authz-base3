import { injectable } from 'inversify';
import { AuthorizationDataStore } from '../../src/dataStores/AuthorizationDataStore';
import { AuthorizationResult } from '../../src/models/AuthorizationResult';
import { LoginEnabled } from '../../src/models/LoginEnabled';

export const mockPermissions: AuthorizationResult[] = [
  {
    resource: 'test-resource',
    role: 'ROL',
    permission: 'test-permission',
    userScope: 'user-scope',
    permissionScope: 'perm-scope',
  },
];

export const mockLogin: LoginEnabled = {
  isLocked: false,
};

@injectable()
export class AuthorizationDataStoreMock implements AuthorizationDataStore {
  public getPermissions(companyHash: string, username: string, resources: string[]): Promise<AuthorizationResult[]> {
    return Promise.resolve(mockPermissions);
  }

  public getLoginEnabled(companyHash: string, username: string): Promise<LoginEnabled> {
    if (username === 'exception') {
      return Promise.reject('bad');
    }

    return Promise.resolve(mockLogin);
  }
}
