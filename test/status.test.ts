
// tslint:disable-next-line:no-import-side-effect
import "reflect-metadata";
import { MockConfig } from './mocks/Config.mock';
import { BaseGqlExpressMicroservice } from '@sd-microservice-base/express';
import { createExpressService } from './helpers/create-express-service';
import { logger} from '@sd-microservice-base/instrumentation';
import * as client from 'prom-client';
import * as request from 'supertest';

beforeAll(()=> {
  logger.transports.console.silent = true;
  client.register.clear();
});

describe('Microservice status', ()=> {
  let expressService: BaseGqlExpressMicroservice;

  beforeEach(()=> {
    expressService = createExpressService(new MockConfig());
  });

  it('Should expose a status endpoint which verifies the integrity of the service', async ()=> {
    await request(expressService.expressApp)
      .get('/status')
      .expect(200)
      .expect('Content-Type', /json/)
      .then((response: any) => {
        expect(response.body.msg).toBe('OK')
      })
  });
});