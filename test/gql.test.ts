// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { MockConfig } from './mocks/Config.mock';
import { BaseGqlExpressMicroservice } from '@sd-microservice-base/express';
import { createExpressService } from './helpers/create-express-service';
import { logger } from '@sd-microservice-base/instrumentation';
import * as client from 'prom-client';
import * as request from 'supertest';

beforeAll(() => {
  logger.transports.console.silent = true;
});

describe('GraphQL implementation', () => {
  let expressService: BaseGqlExpressMicroservice;

  beforeEach(() => {
    client.register.clear();
    expressService = createExpressService(new MockConfig());
  });

  it('Should expose a /gql endpoint which allows introspection', async () => {
    await request(expressService.expressApp)
      .post('/gql')
      .send({
        query:
          '\n    query IntrospectionQuery {\n      __schema {\n        queryType { name }\n        mutationType { name }\n        subscriptionType { name }\n        types {\n          ...FullType\n        }\n        directives {\n          name\n          description\n          locations\n          args {\n            ...InputValue\n          }\n        }\n      }\n    }\n\n    fragment FullType on __Type {\n      kind\n      name\n      description\n      fields(includeDeprecated: true) {\n        name\n        description\n        args {\n          ...InputValue\n        }\n        type {\n          ...TypeRef\n        }\n        isDeprecated\n        deprecationReason\n      }\n      inputFields {\n        ...InputValue\n      }\n      interfaces {\n        ...TypeRef\n      }\n      enumValues(includeDeprecated: true) {\n        name\n        description\n        isDeprecated\n        deprecationReason\n      }\n      possibleTypes {\n        ...TypeRef\n      }\n    }\n\n    fragment InputValue on __InputValue {\n      name\n      description\n      type { ...TypeRef }\n      defaultValue\n    }\n\n    fragment TypeRef on __Type {\n      kind\n      name\n      ofType {\n        kind\n        name\n        ofType {\n          kind\n          name\n          ofType {\n            kind\n            name\n            ofType {\n              kind\n              name\n              ofType {\n                kind\n                name\n                ofType {\n                  kind\n                  name\n                  ofType {\n                    kind\n                    name\n                  }\n                }\n              }\n            }\n          }\n        }\n      }\n    }\n  ',
        variables: {},
        operationName: null,
      })
      .expect(200)
      .expect('Content-Type', /json/);
  });

  it('Should allow for querying the graphql endpoint', async () => {
    const response: any = await request(expressService.expressApp)
      .post('/gql')
      .send({
        query: `
            query getAuthorizations {
                authorizations {
                    resource
                    role
                    permission
                    userScope
                    permissionScope
                }
            }
          `,
      })
      .expect(200)
      .expect('Content-Type', /json/);
    expect(response.body.data.authorizations).toMatchInlineSnapshot(`null`);
  });
});
