export enum TestConstants {
  FailingCompanyId = 'failing-company-id',
  FailingVehicleId = 'failing-vehicle-id',
  FailingSmartRecorderId = ' failing-sr-id',
}

export class InducedTestError extends Error {
  constructor(message?: string) {
    super(message || 'Induced test error');
  }
}
