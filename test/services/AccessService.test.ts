import 'reflect-metadata';
import { Container } from 'inversify';
import { AccessService } from '../../src/services/AccessService';
import { SiteDataStore } from '../../src/dataStores/SiteDataStore';
import { SiteDataStoreMock } from '../mocks/SiteDataStoreMock';
import { AccessDataStore } from '../../src/dataStores/AccessDataStore';
import { UserDataStoreMock } from '../mocks/UserDataStoreMock';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { UserDataStore } from '../../src/dataStores/UserDataStore';
import { AccessDataStoreImpl } from '../../src/dataStores/AccessDataStoreImpl';
import { CompanyDataStore } from '../../src/dataStores/CompanyDataStore';
import { SmartRecorderDataStore } from '../../src/dataStores/SmartRecorderDataStore';
import { VehicleDataStore } from '../../src/dataStores/VehicleDataStore';
import { CompanyDataStoreMock } from '../mocks/CompanyDataStoreMock';
import { SmartRecorderDataStoreMock } from '../mocks/SmartRecorderDataStoreMock';
import { VehicleDataStoreMock } from '../mocks/VehicleDataStoreMock';
import { AccessToken } from '@sd-microservice-base/security';

// tslint:disable:underscore-consistent-invocation
// tslint:disable:no-require-imports
// tslint:disable-next-line:no-var-requires
const httpMocks = require('node-mocks-http');

let container: Container;
let accessService: AccessService;
let loggingService: LoggingService;

const mockLogFn = jest.fn();

// setup the mock datastore in our IoC container and get our test service spun up
beforeAll(() => {
  // tslint:disable-next-line:no-string-literal
  // logger.transports["console"].silent = true;
  container = new Container();
  container.bind<SiteDataStore>(InversifyTypes.SiteDataStore).to(SiteDataStoreMock);
  container.bind<CompanyDataStore>(InversifyTypes.CompanyDataStore).to(CompanyDataStoreMock);
  container.bind<SmartRecorderDataStore>(InversifyTypes.SmartRecorderDataStore).to(SmartRecorderDataStoreMock);
  container.bind<UserDataStore>(InversifyTypes.UserDataStore).to(UserDataStoreMock);
  container.bind<VehicleDataStore>(InversifyTypes.VehicleDataStore).to(VehicleDataStoreMock);
  container.bind<AccessDataStore>(InversifyTypes.AccessDataStore).to(AccessDataStoreImpl);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });

  // get the test services that uses the Mock data store
  accessService = container.resolve(AccessService);
  loggingService = container.get(InversifyTypes.LoggingService);
});

afterAll(() => {
  // tslint:disable-next-line:no-string-literal
  container = undefined;
  accessService = undefined;
  loggingService = undefined;
});

// tslint:disable-next-line:no-any
let response: any;

beforeEach(() => {
  response = httpMocks.createResponse({
    eventEmitter: require('events').EventEmitter,
  });
});

afterEach(() => {
  response = undefined;
});

describe('Access Service', () => {
  describe('Logging', () => {
    describe('getAccess', () => {
      beforeEach(() => {
        mockLogFn.mockClear();
      });

      it('should log an error ', async () => {
        let result;
        try {
          const accessToken: AccessToken = {
            companyHash: null,
            username: 'error',
            rawToken: 'mock',
            issuer: 'mock',
            audience: 'mock',
          };
          result = await accessService.getAccess(accessToken, null, null, null, null, null);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "Authorization get access error",
                },
                Object {
                  "functionName": "AccessService.getAccess",
                },
                [Error: mocked UserDataStoreMock.getUser exception],
              ],
            ]
          `);
        }
      });
    });
  });

  describe('Resources endpoint', () => {
    test('error retrieving user', async () => {
      let result;
      try {
        const accessToken: AccessToken = {
          companyHash: 'hash',
          username: 'error',
          rawToken: 'mock',
          issuer: 'mock',
          audience: 'mock',
        };
        result = await accessService.getAccess(accessToken, ['1'], null, null, null, null);
      } catch {
        expect(result).toMatchInlineSnapshot(`undefined`);
      }
    });

    test('unknown user should return 403', async () => {
      let result;
      try {
        const accessToken: AccessToken = {
          companyHash: 'hash',
          username: 'notfound',
          rawToken: 'mock',
          issuer: 'mock',
          audience: 'mock',
        };
        result = await accessService.getAccess(accessToken, ['1'], null, null, null, null);
      } catch {
        expect(result).toMatchInlineSnapshot(`undefined`);
      }
    });

    test('user has access to company 1 but not 2', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, ['1', '2'], null, null, null, null);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mockCompany",
            "resourceType": "company",
          },
          Object {
            "hasAccess": false,
            "resourceId": "2",
            "resourceName": null,
            "resourceType": "company",
          },
        ]
      `);
    });

    test('user badsite trips an error retrieving sites', async () => {
      let result;
      try {
        const accessToken: AccessToken = {
          companyHash: 'hash',
          username: 'badsite',
          rawToken: 'mock',
          issuer: 'mock',
          audience: 'mock',
        };
        result = await accessService.getAccess(accessToken, [], ['1'], null, null, null);
      } catch {
        expect(result).toMatchInlineSnapshot(`undefined`);
      }
    });

    test('vehicle 666 is an error', async () => {
      let result;
      try {
        const accessToken: AccessToken = {
          companyHash: 'hash',
          username: 'testuser',
          rawToken: 'mock',
          issuer: 'mock',
          audience: 'mock',
        };
        result = await accessService.getAccess(accessToken, [], [], ['666'], null, null);
      } catch {
        expect(result).toMatchInlineSnapshot(`undefined`);
      }
    });

    test('sr 666 is an error', async () => {
      let result;
      try {
        const accessToken: AccessToken = {
          companyHash: 'hash',
          username: 'testuser',
          rawToken: 'mock',
          issuer: 'mock',
          audience: 'mock',
        };
        result = await accessService.getAccess(accessToken, [], [], [], ['666'], null);
      } catch {
        expect(result).toMatchInlineSnapshot(`undefined`);
      }
    });

    test('user has access to sites 1 and 2. does not have access to 6. 999 does not exist', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, [], ['1', '2', '6', '999'], null, null, null);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "one",
            "resourceType": "site",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "two",
            "resourceType": "site",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "site",
          },
          Object {
            "hasAccess": false,
            "resourceId": "999",
            "resourceName": null,
            "resourceType": "site",
          },
        ]
      `);
    });

    test('user has access to vehicles 1 and 2. does not have access to 6. 999 does not exist', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, [], [], ['1', '2', '6', '999'], null, null);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mocked vehicle 1 SN",
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "mocked vehicle 2 SN",
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": false,
            "resourceId": "999",
            "resourceName": null,
            "resourceType": "vehicle",
          },
        ]
      `);
    });

    test('user has access to SRs 1 and 2. does not have access to 6. 999 does not exist', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, [], [], [], ['1', '2', '6', '999'], null);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mocked SR 1 SN",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "mocked SR 2 SN",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": "999",
            "resourceName": null,
            "resourceType": "smartRecorder",
          },
        ]
      `);
    });

    test('user has access to SR SNs 1 and 2. does not have access to 6. 999 does not exist', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, [], [], [], null, ['1', '2', '6', '999']);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "mocked SR 1 ID",
            "resourceName": "1",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": true,
            "resourceId": "mocked SR 2 ID",
            "resourceName": "2",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": null,
            "resourceName": "6",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": null,
            "resourceName": "999",
            "resourceType": "smartRecorder",
          },
        ]
      `);
    });

    test('superuser has access to company 1 and 2', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, ['1', '2'], null, null, null, null);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mockCompany",
            "resourceType": "company",
          },
          Object {
            "hasAccess": false,
            "resourceId": "2",
            "resourceName": null,
            "resourceType": "company",
          },
        ]
      `);
    });

    test('superuser has access to sites 1, 2, and 6.', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, [], ['1', '2', '6'], null, null, null);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "one",
            "resourceType": "site",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "two",
            "resourceType": "site",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "site",
          },
        ]
      `);
    });

    test('superuser has access to vehicles 1, 2, and 6. 999 does not exist', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, [], [], ['1', '2', '6', '999'], null, null);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mocked vehicle 1 SN",
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "mocked vehicle 2 SN",
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": false,
            "resourceId": "999",
            "resourceName": null,
            "resourceType": "vehicle",
          },
        ]
      `);
    });

    test('superuser has access to SRs 1, 2, and 6. 999 does not exist', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, [], [], [], ['1', '2', '6', '999'], null);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mocked SR 1 SN",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "mocked SR 2 SN",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": "999",
            "resourceName": null,
            "resourceType": "smartRecorder",
          },
        ]
      `);
    });

    test('superuser has access to SR SNs 1, 2, and 6. 999 does not exist', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(accessToken, [], [], [], [], ['1', '2', '6', '999']);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "mocked SR 1 ID",
            "resourceName": "1",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": true,
            "resourceId": "mocked SR 2 ID",
            "resourceName": "2",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": null,
            "resourceName": "6",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": null,
            "resourceName": "999",
            "resourceType": "smartRecorder",
          },
        ]
      `);
    });

    test('combining multiple resource types into one query', async () => {
      const accessToken: AccessToken = {
        companyHash: 'hash',
        username: 'testuser',
        rawToken: 'mock',
        issuer: 'mock',
        audience: 'mock',
      };
      const result = await accessService.getAccess(
        accessToken,
        ['1'],
        ['1', '2', '6', '999'],
        ['1', '2', '6', '999'],
        ['1', '2', '6', '999'],
        null
      );
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mockCompany",
            "resourceType": "company",
          },
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "one",
            "resourceType": "site",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "two",
            "resourceType": "site",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "site",
          },
          Object {
            "hasAccess": false,
            "resourceId": "999",
            "resourceName": null,
            "resourceType": "site",
          },
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mocked vehicle 1 SN",
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "mocked vehicle 2 SN",
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": false,
            "resourceId": "999",
            "resourceName": null,
            "resourceType": "vehicle",
          },
          Object {
            "hasAccess": true,
            "resourceId": "1",
            "resourceName": "mocked SR 1 SN",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": true,
            "resourceId": "2",
            "resourceName": "mocked SR 2 SN",
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": "6",
            "resourceName": null,
            "resourceType": "smartRecorder",
          },
          Object {
            "hasAccess": false,
            "resourceId": "999",
            "resourceName": null,
            "resourceType": "smartRecorder",
          },
        ]
      `);
    });
  });
});
