import { AuthorizationDataStoreSqlImpl } from '../../src/dataStores/AuthorizationDataStoreSqlImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Authorization data store SQL', () => {
  describe('getPermissions metrics', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.histogram,
        AuthorizationDataStoreSqlImpl.prototype,
        'getPermissions'
      );

      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "AuthorizationDataStoreSqlImpl.getPermissions",
          "data_store": "Authorization Data Store SQL",
          "location": "core_oltp",
        }
      `);
    });

    it('should be using the error counter decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.errorCounter,
        AuthorizationDataStoreSqlImpl.prototype,
        'getPermissions'
      );

      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "AuthorizationDataStoreSqlImpl.getPermissions",
          "data_store": "Authorization Data Store SQL",
          "location": "core_oltp",
        }
      `);
    });
  });

  describe('getLoginEnabled metrics', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.histogram,
        AuthorizationDataStoreSqlImpl.prototype,
        'getLoginEnabled'
      );

      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "AuthorizationDataStoreSqlImpl.getLoginEnabled",
          "data_store": "Authorization Data Store SQL",
          "location": "core_oltp",
        }
      `);
    });

    it('should be using the error counter decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.errorCounter,
        AuthorizationDataStoreSqlImpl.prototype,
        'getLoginEnabled'
      );

      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "AuthorizationDataStoreSqlImpl.getLoginEnabled",
          "data_store": "Authorization Data Store SQL",
          "location": "core_oltp",
        }
      `);
    });
  });
});
