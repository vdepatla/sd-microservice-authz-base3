import 'reflect-metadata';
import { AccessDataStoreImpl } from '../../src/dataStores/AccessDataStoreImpl';
import {
  getDecoratorMetadata,
  metadataKeys,
  ExternalResourceActions,
  ExternalResources,
} from '@sd-microservice-base/instrumentation';
import { DataStore } from '../../src/models/DataStore';

describe('Acessdata store', () => {
  describe('canUserAccessCompanies', () => {
    describe('Check histogram decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(
          metadataKeys.histogram,
          AccessDataStoreImpl.prototype,
          'canUserAccessCompanies'
        );
      });

      it('should be using the histogram decorator', () => {
        const expectResult = metadataKeys.histogram.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the histogram decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessCompanies');
      });
    });

    describe('Check errorCounter decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(
          metadataKeys.errorCounter,
          AccessDataStoreImpl.prototype,
          'canUserAccessCompanies'
        );
      });

      it('should be using the errorCounter decorator', () => {
        const expectResult = metadataKeys.errorCounter.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the errorCounter decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessCompanies');
      });
    });
  });

  describe('canUserAccessSites', () => {
    describe('Check histogram decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(metadataKeys.histogram, AccessDataStoreImpl.prototype, 'canUserAccessSites');
      });

      it('should be using the histogram decorator', () => {
        const expectResult = metadataKeys.histogram.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the histogram decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessSites');
      });
    });

    describe('Check errorCounter decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(metadataKeys.errorCounter, AccessDataStoreImpl.prototype, 'canUserAccessSites');
      });

      it('should be using the errorCounter decorator', () => {
        const expectResult = metadataKeys.errorCounter.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the errorCounter decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessSites');
      });
    });
  });

  describe('canUserAccessCompanies', () => {
    describe('Check histogram decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(
          metadataKeys.histogram,
          AccessDataStoreImpl.prototype,
          'canUserAccessCompanies'
        );
      });

      it('should be using the histogram decorator', () => {
        const expectResult = metadataKeys.histogram.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the histogram decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessCompanies');
      });
    });

    describe('Check errorCounter decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(
          metadataKeys.errorCounter,
          AccessDataStoreImpl.prototype,
          'canUserAccessCompanies'
        );
      });

      it('should be using the histogram decorator', () => {
        const expectResult = metadataKeys.errorCounter.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the histogram decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessCompanies');
      });
    });
  });

  describe('canUserAccessVehicles', () => {
    describe('Check histogram decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(metadataKeys.histogram, AccessDataStoreImpl.prototype, 'canUserAccessVehicles');
      });

      it('should be using the histogram decorator', () => {
        const expectResult = metadataKeys.histogram.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the histogram decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessVehicles');
      });
    });

    describe('Check errorCounter decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(
          metadataKeys.errorCounter,
          AccessDataStoreImpl.prototype,
          'canUserAccessVehicles'
        );
      });

      it('should be using the errorCounter decorator', () => {
        const expectResult = metadataKeys.errorCounter.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the errorCounter decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessVehicles');
      });
    });
  });

  describe('canUserAccessSmartRecorders', () => {
    describe('Check histogram decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(
          metadataKeys.histogram,
          AccessDataStoreImpl.prototype,
          'canUserAccessSmartRecorders'
        );
      });

      it('should be using the histogram decorator', () => {
        const expectResult = metadataKeys.histogram.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the histogram decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessSmartRecorders');
      });
    });

    describe('Check errorCounter decorator usage', () => {
      let metadata: any;

      beforeAll(() => {
        metadata = getDecoratorMetadata(
          metadataKeys.errorCounter,
          AccessDataStoreImpl.prototype,
          'canUserAccessSmartRecorders'
        );
      });

      it('should be using the errorCounter decorator', () => {
        const expectResult = metadataKeys.errorCounter.decoratorName;
        expect(metadata.decoratorName).toBe(expectResult);
      });

      it('should be using the correct labels for the errorCounter decorator', () => {
        expect(metadata.labelValues.action).toBe(ExternalResourceActions.Read);
        expect(metadata.labelValues.location).toBe(ExternalResources.CoreOLTP);
        expect(metadata.labelValues.data_store).toBe(DataStore.AccessDataStore);
        expect(metadata.labelValues.context).toBe('AccessDataStoreImpl.canUserAccessSmartRecorders');
      });
    });
  });
});
