import { AuthorizationDataStoreImpl } from '../../src/dataStores/AuthorizationDataStoreImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Authorization data store', () => {
  describe('getPermissionsElastic', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.histogram,
        AuthorizationDataStoreImpl.prototype,
        'getPermissionsElastic'
      );
      expect(metadata.labelValues).toMatchInlineSnapshot(`
          Object {
            "action": "read",
            "context": "AuthorizationDataStoreImpl.getPermissionsElastic",
            "data_store": "Authorization Data Store ES",
            "location": "elasticsearch",
          }
        `);
    });

    it('should be using the error decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.errorCounter,
        AuthorizationDataStoreImpl.prototype,
        'getPermissionsElastic'
      );
      expect(metadata.labelValues).toMatchInlineSnapshot(`
          Object {
            "action": "read",
            "context": "AuthorizationDataStoreImpl.getPermissionsElastic",
            "data_store": "Authorization Data Store ES",
            "location": "elasticsearch",
          }
        `);
    });
  });

  describe('getLoginEnabledElastic', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.histogram,
        AuthorizationDataStoreImpl.prototype,
        'getLoginEnabledElastic'
      );
      expect(metadata.labelValues).toMatchInlineSnapshot(`
          Object {
            "action": "read",
            "context": "AuthorizationDataStoreImpl.getLoginEnabledElastic",
            "data_store": "Authorization Data Store ES",
            "location": "elasticsearch",
          }
        `);
    });

    it('should be using the error decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.errorCounter,
        AuthorizationDataStoreImpl.prototype,
        'getLoginEnabledElastic'
      );
      expect(metadata.labelValues).toMatchInlineSnapshot(`
          Object {
            "action": "read",
            "context": "AuthorizationDataStoreImpl.getLoginEnabledElastic",
            "data_store": "Authorization Data Store ES",
            "location": "elasticsearch",
          }
        `);
    });
  });
});
