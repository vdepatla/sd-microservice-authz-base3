import { SiteDataStoreImpl } from './../../src/dataStores/SiteDataStoreImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Site data store', () => {
  describe('getSiteWithChildren', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.histogram, SiteDataStoreImpl.prototype, 'getSiteWithChildren');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "SiteDataStoreImpl.getSiteWithChildren",
          "data_store": "SiteDataStore",
          "location": "core_oltp",
        }
      `);
    });

    it('should be using the error counter decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.errorCounter,
        SiteDataStoreImpl.prototype,
        'getSiteWithChildren'
      );
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "SiteDataStoreImpl.getSiteWithChildren",
          "data_store": "SiteDataStore",
          "location": "core_oltp",
        }
      `);
    });

    it('should be using the log execution decorator', () => {
      const metadata = Reflect.getMetadata('logExecution', SiteDataStoreImpl.prototype, 'getSiteWithChildren');
      expect(metadata).toMatchInlineSnapshot(`"logExecution"`);
    });
  });

  describe('getSites', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.histogram, SiteDataStoreImpl.prototype, 'getSites');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
      Object {
        "action": "read",
        "context": "SiteDataStoreImpl.getSites",
        "data_store": "SiteDataStore",
        "location": "core_oltp",
      }
      `);
    });

    it('should be using the error counter decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.errorCounter, SiteDataStoreImpl.prototype, 'getSites');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
      Object {
        "action": "read",
        "context": "SiteDataStoreImpl.getSites",
        "data_store": "SiteDataStore",
        "location": "core_oltp",
      }
      `);
    });

    it('should be using the log execution decorator', () => {
      const metadata = Reflect.getMetadata('logExecution', SiteDataStoreImpl.prototype, 'getSites');
      expect(metadata).toMatchInlineSnapshot(`"logExecution"`);
    });
  });
});
