import { VehicleDataStoreImpl } from '../../src/dataStores/VehicleDataStoreImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Vehicle data store', () => {
  describe('getVehicles', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.histogram, VehicleDataStoreImpl.prototype, 'getVehicles');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "VehicleDataStoreImpl.getVehicles",
          "data_store": "Vehicle Data Store",
          "location": "core_oltp",
        }
      `);
    });

    it('should be using the error decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.errorCounter, VehicleDataStoreImpl.prototype, 'getVehicles');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "VehicleDataStoreImpl.getVehicles",
          "data_store": "Vehicle Data Store",
          "location": "core_oltp",
        }
      `);
    });
  });
});
