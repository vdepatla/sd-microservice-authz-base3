import { TestConstants } from './../TestConstants';
import { AccessDataStoreImpl } from './../../src/dataStores/AccessDataStoreImpl';
import { AccessDataStore } from './../../src/dataStores/AccessDataStore';
import { CompanyDataStoreMock } from './../mocks/CompanyDataStoreMock';
import { CompanyDataStore } from './../../src/dataStores/CompanyDataStore';
import { SmartRecorderDataStoreMock } from './../mocks/SmartRecorderDataStoreMock';
import { SmartRecorderDataStore } from './../../src/dataStores/SmartRecorderDataStore';
import { VehicleDataStoreMock } from './../mocks/VehicleDataStoreMock';
import { VehicleDataStore } from './../../src/dataStores/VehicleDataStore';
import { Container } from 'inversify';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import 'reflect-metadata';

describe('Access data store', () => {
  let container: Container;
  let accessDataStore: AccessDataStore;
  let loggingService: LoggingService;

  const mockLogFn = jest.fn();

  beforeAll(() => {
    container = new Container();
    container.bind<VehicleDataStore>(InversifyTypes.VehicleDataStore).to(VehicleDataStoreMock);
    container.bind<SmartRecorderDataStore>(InversifyTypes.SmartRecorderDataStore).to(SmartRecorderDataStoreMock);
    container.bind<CompanyDataStore>(InversifyTypes.CompanyDataStore).to(CompanyDataStoreMock);
    container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
      logs: {},
      log: mockLogFn,
      addLogs: () => {},
      replaceLogs: () => {},
      getLogs: (): any => {},
    });
  });

  beforeEach(() => {
    accessDataStore = container.resolve(AccessDataStoreImpl);
    loggingService = container.get(InversifyTypes.LoggingService);
  });

  describe('Logging', () => {
    beforeEach(() => {
      mockLogFn.mockClear();
    });

    describe('canUserAccessCompanies', () => {
      it('should log when an error occurs', async () => {
        try {
          await accessDataStore.canUserAccessCompanies([TestConstants.FailingCompanyId], null, null);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "An error occurred determining company access",
                },
                Object {
                  "functionName": "AccessDataStoreImpl.canUserAccessCompanies",
                },
                [TypeError: Cannot read property 'companyId' of null],
              ],
            ]
          `);
        }
      });
    });

    describe('canUserAccessSites', () => {
      /**
       * This method makes no external calls, so to cause an error we pass in bad data.
       */
      it('Should log if bad parameters are passed in', async () => {
        try {
          await accessDataStore.canUserAccessSites(('this is not an array' as unknown) as string[], null, null);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "An error occurred determining site access",
                },
                Object {
                  "functionName": "AccessDataStoreImpl.canUserAccessSites",
                },
                [TypeError: siteIds.map is not a function],
              ],
            ]
          `);
        }
      });
    });

    describe('canUserAccessVehicles', () => {
      it('Should log when an error occurs', async () => {
        try {
          await accessDataStore.canUserAccessVehicles([TestConstants.FailingVehicleId], null, null);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "An error occurred determining vehicle access",
                },
                Object {
                  "functionName": "AccessDataStoreImpl.canUserAccessVehicles",
                },
                [Error: Vehicle data store was passed failure vehicle ID],
              ],
            ]
          `);
        }
      });
    });

    describe('canUserAccessSmartRecorders', () => {
      it('Should log when an error occurs', async () => {
        try {
          await accessDataStore.canUserAccessSmartRecorders([TestConstants.FailingSmartRecorderId], [], [], null);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "An error occurred determining SR access",
                },
                Object {
                  "functionName": "AccessDataStoreImpl.canUserAccessSmartRecorders",
                },
                [Error: Smart recorder data store was passed failure SR ID],
              ],
            ]
          `);
        }
      });
    });
  });
});
