import { ExtractRefreshTimeCacheImpl } from '../../src/dataStores/ExtractRefreshTimeCacheImpl';
import { ExtractRefreshTime } from '../../src/models/ExtractRefreshTime';
import { Container } from 'inversify';
import { LoggingService, BaseMetricsService } from '@sd-microservice-base/instrumentation';
import { MockConfig } from '../mocks/Config.mock';
import { RedisDataStore, BaseRedisDataStore } from '@sd-microservice-base/redis';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { Config } from '../../src/Config';

const mockGetFn = jest.fn();
const mockSetFn = jest.fn();

let container: Container;
let loggingService: LoggingService;
let extractRefreshTimeCache: ExtractRefreshTimeCacheImpl;

const mockLogFn = jest.fn();
jest.mock('ioredis', () => {
  return {
    Cluster: function() {
      return {
        get: mockGetFn,
        set: mockSetFn,
        on: jest.fn(),
      };
    },
  };
});

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container
    .bind<RedisDataStore>(InversifyTypes.RedisDataStore)
    .toConstantValue(new BaseRedisDataStore(config, new BaseMetricsService()));
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  loggingService = container.get(InversifyTypes.LoggingService);
  extractRefreshTimeCache = container.resolve(ExtractRefreshTimeCacheImpl);
});

beforeEach(() => {
  mockGetFn.mockReset();
  mockSetFn.mockReset();
});

describe('Extract Refresh Time Cache Impl', () => {
  beforeEach(() => {
    mockGetFn.mockReset();
    mockSetFn.mockReset();
  });

  const mockRefreshTimes: ExtractRefreshTime[] = [
    {
      index: 'index_1',
      refreshTimeUnix: 300,
    },
    {
      index: 'index_2',
      refreshTimeUnix: 600,
    },
  ];

  describe('getExtractRefreshTimes', () => {
    it('returns refresh times by index from cache', async () => {
      mockGetFn.mockResolvedValueOnce('300');
      mockGetFn.mockResolvedValueOnce('600');

      const refreshTimes = await extractRefreshTimeCache.getExtractRefreshTimes(['index_1', 'index_2']);
      expect(refreshTimes).toEqual(mockRefreshTimes);
    });

    it('returns an empty array of times when nothing is in cache', async () => {
      mockGetFn.mockResolvedValue(undefined);

      const refreshTimes = await extractRefreshTimeCache.getExtractRefreshTimes(['index_1', 'index_2']);
      expect(refreshTimes).toEqual([]);
    });

    it('returns an empty array of times accessing the cache errors out', async () => {
      mockGetFn.mockRejectedValue(new Error('fake test error'));

      const refreshTimes = await extractRefreshTimeCache.getExtractRefreshTimes(['index_1', 'index_2']);
      expect(refreshTimes).toEqual([]);
    });
  });

  describe('setExtractRefreshTimes', () => {
    it('set new value in cache and returns true', async () => {
      const result = await extractRefreshTimeCache.setExtractRefreshTimes(mockRefreshTimes);
      expect(mockSetFn).toHaveBeenCalled();
      expect(result).toEqual(true);
    });

    it('returns false if an exception is encountered while setting new cache value', async () => {
      mockSetFn.mockRejectedValueOnce(new Error('fake test error'));

      const result = await extractRefreshTimeCache.setExtractRefreshTimes(mockRefreshTimes);
      expect(result).toEqual(false);
    });
  });

  describe('logging', () => {
    beforeEach(() => {
      mockLogFn.mockClear();
    });

    describe('getExtractRefreshTimes', () => {
      it('logs verbose when we get a cache hit', async () => {
        mockGetFn.mockResolvedValueOnce('300');

        await extractRefreshTimeCache.getExtractRefreshTimes(['index_1']);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "verbose",
                "message": "Found refresh time in redis for given key",
              },
              Object {
                "functionName": "ExtractRefreshTimeCacheImpl.getExtractRefreshTimes",
                "key": "index_1",
              },
            ],
          ]
        `);
      });

      it('logs verbose when we get get a cache miss', async () => {
        mockGetFn.mockResolvedValueOnce(undefined);

        await extractRefreshTimeCache.getExtractRefreshTimes(['index_1']);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "verbose",
                "message": "Refresh time not found in redis for given key",
              },
              Object {
                "functionName": "ExtractRefreshTimeCacheImpl.getExtractRefreshTimes",
                "key": "index_1",
              },
            ],
          ]
        `);
      });

      it('logs an error when accessing the cache errors out', async () => {
        mockGetFn.mockRejectedValue(new Error('fake test error'));

        await extractRefreshTimeCache.getExtractRefreshTimes(['index_1']);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "error",
                "message": "Error occurred reading refresh times from redis",
              },
              Object {
                "functionName": "ExtractRefreshTimeCacheImpl.getExtractRefreshTimes",
                "keys": Array [
                  "index_1",
                ],
              },
              [Error: fake test error],
            ],
          ]
        `);
      });
    });

    describe('setExtractRefreshTimes', () => {
      it('set new value in cache and returns true', async () => {
        const index1 = mockRefreshTimes[0];
        await extractRefreshTimeCache.setExtractRefreshTimes([index1]);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "verbose",
                "message": "Writing refresh time for given key to redis",
              },
              Object {
                "functionName": "ExtractRefreshTimeCacheImpl.setExtractRefreshTimes",
                "key": "index_1",
              },
            ],
          ]
        `);
      });

      it('returns false if an exception is encountered while setting new cache value', async () => {
        mockSetFn.mockRejectedValueOnce(new Error('fake test error'));

        const index2 = mockRefreshTimes[1];
        await extractRefreshTimeCache.setExtractRefreshTimes([index2]);

        expect((loggingService.log as jest.Mock).mock.calls[1]).toMatchInlineSnapshot(`
          Array [
            Object {
              "level": "error",
              "message": "Error occurred writing refresh times to redis",
            },
            Object {
              "functionName": "ExtractRefreshTimeCacheImpl.setExtractRefreshTimes",
              "refreshTimes": Array [
                Object {
                  "index": "index_2",
                  "refreshTimeUnix": 600,
                },
              ],
            },
            [Error: fake test error],
          ]
        `);
      });
    });
  });
});
