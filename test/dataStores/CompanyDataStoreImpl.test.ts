import { CompanyDataStoreImpl } from '../../src/dataStores/CompanyDataStoreImpl';
import { Container } from 'inversify';
import { ElasticsearchClient } from '../../src/models/ElasticSearch/ElasticsearchClient';
import { AuthorizationExtractDataStore } from '../../src/dataStores/AuthorizationExtractDataStore';
import { Auth0DataStore } from '../../src/dataStores/Auth0DataStore';
import { AccessTokenCacheMock } from '../mocks/AccessTokenCacheMock';
import { AccessTokenCache } from '../../src/dataStores/AccessTokenCache';
import { Auth0DataStoreMock } from '../mocks/Auth0DataStoreMock';
import { AuthorizationExtractDataStoreMock } from '../mocks/AuthorizationExtractDataStoreMock';
import { CompanyDataStore } from '../../src/dataStores/CompanyDataStore';
import { CompanyDataStoreSql } from '../../src/dataStores/CompanyDataStoreSql';
import { CompanyDataStoreSqlImplMock } from '../mocks/CompanyDataStoreSqlImplMock';
import { ExtractRefreshTimeCache } from '../../src/dataStores/ExtractRefreshTimeCache';
import { ExtractRefreshTimeCacheMock } from '../mocks/ExtractRefreshTimeCacheMock';
import { InducedTestError } from '../TestConstants';
import { ElasticsearchClientImpl } from '../../src/models/ElasticSearch/ElasticsearchClientImpl';
import { RedisDataStore, BaseRedisDataStore } from '@sd-microservice-base/redis';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { BaseMetricsService, LoggingService } from '@sd-microservice-base/instrumentation';
import * as client from 'prom-client';
import { MockConfig } from '../mocks/Config.mock';
import { Config } from '../../src/Config';
import { AuthorizationExtractDataUtil } from '../../src/dataStores/AuthorizationExtractDataUtil';

const searchMethodMock = jest.fn();
const isLatestSpy = jest.spyOn(AuthorizationExtractDataUtil, 'isExtractDataLatest');
const mockLogFn = jest.fn();
let loggingService: LoggingService;

jest.mock('@elastic/elasticsearch', () => ({
  // tslint:disable-next-line: object-literal-shorthand
  Client: function() {
    return {
      search: searchMethodMock,
    };
  },
}));

let container: Container;
let companyDataStore: CompanyDataStore;

beforeAll(() => {
  const config = new MockConfig();
  container = new Container();
  container
    .bind<RedisDataStore>(InversifyTypes.RedisDataStore)
    .toConstantValue(new BaseRedisDataStore(config, new BaseMetricsService(client.register)));
  container.bind<ElasticsearchClient>(InversifyTypes.ElasticsearchClient).to(ElasticsearchClientImpl);
  container
    .bind<AuthorizationExtractDataStore>(InversifyTypes.AuthorizationExtractDataStore)
    .to(AuthorizationExtractDataStoreMock);
  container.bind<AccessTokenCache>(InversifyTypes.AccessTokenCache).to(AccessTokenCacheMock);
  container.bind<Auth0DataStore>(InversifyTypes.Auth0DataStore).to(Auth0DataStoreMock);
  container.bind<CompanyDataStoreSql>(InversifyTypes.CompanyDataStoreSql).to(CompanyDataStoreSqlImplMock);
  container.bind<ExtractRefreshTimeCache>(InversifyTypes.ExtractRefreshTimeCache).to(ExtractRefreshTimeCacheMock);
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });

  loggingService = container.get(InversifyTypes.LoggingService);
  companyDataStore = container.resolve(CompanyDataStoreImpl);
});

afterEach(() => {
  searchMethodMock.mockReset();
  const config = new MockConfig();
  container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
  companyDataStore = container.resolve(CompanyDataStoreImpl);
});

describe('CompanyDataStoreImpl DataStore', () => {
  test('getCompanies - sql fallback occurs when config is turned on', async () => {
    const config = new MockConfig(true);
    container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
    companyDataStore = container.resolve(CompanyDataStoreImpl);
    const result = await companyDataStore.getCompanies(['1']);
    expect(result).toMatchInlineSnapshot(`
      Array [
        Object {
          "CompanyName": "mockCompanySQL",
          "EntityIdentityKey": 1,
        },
      ]
    `);
  });
});

test('getCompanies - sql fallback occurs when error happens', async () => {
  isLatestSpy.mockImplementation(() => {
    return true;
  });
  searchMethodMock.mockRejectedValueOnce(new InducedTestError());
  let result;
  try {
    result = await companyDataStore.getCompanies(['1']);
  } catch {
    expect(result).toMatchInlineSnapshot(`
    Array [
      Object {
        "CompanyName": "mockCompanySQL",
        "EntityIdentityKey": 1,
      },
    ]
  `);
  }
});

test('getCompanies - sql fallback occurs when data is stale', async () => {
  isLatestSpy.mockImplementation(() => {
    return false;
  });
  const result = await companyDataStore.getCompanies(['1']);
  expect(result).toMatchInlineSnapshot(`
    Array [
      Object {
        "CompanyName": "mockCompanySQL",
        "EntityIdentityKey": 1,
      },
    ]
  `);
});

test('getCompanies - elastic search call returns results', async () => {
  isLatestSpy.mockImplementation(() => {
    return true;
  });
  searchMethodMock.mockReturnValueOnce({
    body: {
      hits: {
        total: 1,
        hits: [
          {
            _source: {
              CompanyName: 'mockCompanyES',
              EntityIdentityKey: 1234,
            },
          },
        ],
      },
    },
  });
  const result = await companyDataStore.getCompanies(['1']);
  expect(result).toHaveLength(1);
  expect(result[0].CompanyName).toStrictEqual('mockCompanyES');
  expect(result[0].EntityIdentityKey).toStrictEqual(1234);
});

describe('Logging', () => {
  beforeEach(() => {
    mockLogFn.mockClear();
  });

  describe('getCompanies', () => {
    it('Should log when SQL configuration turned on', async () => {
      const config = new MockConfig(true);
      container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
      companyDataStore = container.resolve(CompanyDataStoreImpl);
      await companyDataStore.getCompanies(['1']);
      expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
    });

    it('Should log when last extract information is stale', async () => {
      isLatestSpy.mockImplementation(() => {
        return false;
      });
      await companyDataStore.getCompanies(['1']);
      expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
    });

    it('Should log verbose when elastic search call is made', async () => {
      isLatestSpy.mockImplementation(() => {
        return true;
      });
      searchMethodMock.mockReturnValueOnce({
        body: {
          hits: {
            total: 1,
            hits: [
              {
                _source: {
                  CompanyName: 'mockCompanyES',
                  EntityIdentityKey: 1234,
                },
              },
            ],
          },
        },
      });
      const result = await companyDataStore.getCompanies([]);
      expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
    });

    it('Should log when an error occurs', async () => {
      isLatestSpy.mockImplementation(() => {
        return true;
      });
      searchMethodMock.mockImplementationOnce(async () => {
        throw new InducedTestError();
      });
      try {
        await companyDataStore.getCompanies([]);
      } catch {
        expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
      }
    });
  });
});
