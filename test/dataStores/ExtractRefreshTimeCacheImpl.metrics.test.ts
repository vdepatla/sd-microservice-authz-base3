import { ExtractRefreshTimeCacheImpl } from './../../src/dataStores/ExtractRefreshTimeCacheImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Extract refresh time cache', () => {
  describe('getExtractRefreshTimes', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.histogram,
        ExtractRefreshTimeCacheImpl.prototype,
        'getExtractRefreshTimes'
      );
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "ExtractRefreshTimeCacheImpl.getExtractRefreshTimes",
          "data_store": "Extract Refresh Time Cache",
          "location": "redis",
        }
      `);
    });
  });

  describe('setExtractRefreshTimes', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.histogram,
        ExtractRefreshTimeCacheImpl.prototype,
        'setExtractRefreshTimes'
      );
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "create",
          "context": "ExtractRefreshTimeCacheImpl.setExtractRefreshTimes",
          "data_store": "Extract Refresh Time Cache",
          "location": "redis",
        }
      `);
    });
  });
});
