import { SmartRecorderDataStoreImpl } from '../../src/dataStores/SmartRecorderDataStoreImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Smart Recorder Data Store', () => {
  describe('getSmartRecorders metrics', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.histogram,
        SmartRecorderDataStoreImpl.prototype,
        'getSmartRecorders'
      );

      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "SmartRecorderDataStoreImpl.getSmartRecorders",
          "data_store": "Smart Recorder Data Store",
          "location": "core_oltp",
        }
      `);
    });

    it('should be using the error counter decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.errorCounter,
        SmartRecorderDataStoreImpl.prototype,
        'getSmartRecorders'
      );

      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "SmartRecorderDataStoreImpl.getSmartRecorders",
          "data_store": "Smart Recorder Data Store",
          "location": "core_oltp",
        }
      `);
    });
  });
});
