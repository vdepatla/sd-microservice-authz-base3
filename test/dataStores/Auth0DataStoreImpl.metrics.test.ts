import 'reflect-metadata';
import { Auth0DataStoreImpl } from './../../src/dataStores/Auth0DataStoreImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Auth0 data store', () => {
  describe('getM2MToken', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.histogram, Auth0DataStoreImpl.prototype, 'getM2MToken');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "Auth0DataStoreImpl.getM2MToken",
          "data_store": "Auth0 Data Store",
          "location": "Auth0",
        }
      `);
    });

    it('should be using the error counter decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.errorCounter, Auth0DataStoreImpl.prototype, 'getM2MToken');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "Auth0DataStoreImpl.getM2MToken",
          "data_store": "Auth0 Data Store",
          "location": "Auth0",
        }
      `);
    });
  });
});
