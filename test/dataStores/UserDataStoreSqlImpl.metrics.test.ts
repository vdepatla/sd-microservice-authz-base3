import { UserDataStoreSqlImpl } from '../../src/dataStores/UserDataStoreSqlImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('User data store (SQL)', () => {
  describe('getUser', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.histogram, UserDataStoreSqlImpl.prototype, 'getUser');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "UserDataStoreSqlImpl.getUser",
          "data_store": "UserDataStoreSqlImpl",
          "location": "core_oltp",
        }
      `);
    });

    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.errorCounter, UserDataStoreSqlImpl.prototype, 'getUser');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "UserDataStoreSqlImpl.getUser",
          "data_store": "UserDataStoreSqlImpl",
          "location": "core_oltp",
        }
      `);
    });

    it('should be using the log execution decorator', () => {
      const metadata = Reflect.getMetadata('logExecution', UserDataStoreSqlImpl.prototype, 'getUser');
      expect(metadata).toMatchInlineSnapshot(`"logExecution"`);
    });
  });

  describe('gatherUserRoles', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.histogram, UserDataStoreSqlImpl.prototype, 'gatherUserRoles');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
      Object {
        "action": "read",
        "context": "UserDataStoreSqlImpl.gatherUserRoles",
        "data_store": "UserDataStoreSqlImpl",
        "location": "core_oltp",
      }
      `);
    });

    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(
        metadataKeys.errorCounter,
        UserDataStoreSqlImpl.prototype,
        'gatherUserRoles'
      );
      expect(metadata.labelValues).toMatchInlineSnapshot(`
      Object {
        "action": "read",
        "context": "UserDataStoreSqlImpl.gatherUserRoles",
        "data_store": "UserDataStoreSqlImpl",
        "location": "core_oltp",
      }
      `);
    });

    it('should be using the log execution decorator', () => {
      const metadata = Reflect.getMetadata('logExecution', UserDataStoreSqlImpl.prototype, 'gatherUserRoles');
      expect(metadata).toMatchInlineSnapshot(`"logExecution"`);
    });
  });
});
