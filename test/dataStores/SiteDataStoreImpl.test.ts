import { InducedTestError } from './../TestConstants';
import { SiteDataStoreImpl } from './../../src/dataStores/SiteDataStoreImpl';
import { Container } from 'inversify';
import { MockConfig } from '../mocks/Config.mock';
import { UserDataStore } from '../../src/dataStores/UserDataStore';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { UserDataStoreMock } from '../mocks/UserDataStoreMock';
import { Config } from '../../src/Config';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { ConnectionPoolManager } from '../../src/sql/ConnectionPoolManager';

const searchMethodMock = jest.fn();

let container: Container;
let siteDataStore: SiteDataStoreImpl;
let requestQueryFn = jest.fn();
let loggingService: LoggingService;
const mockLogFn = jest.fn();

jest.mock('mssql', () => {
  return {
    ConnectionPool: function() {
      return {
        connect: jest.fn().mockResolvedValue(true),
      };
    },
    Request: function() {
      return {
        query: requestQueryFn,
        input: jest.fn(),
      };
    },
  };
});

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  container
    .bind<ConnectionPoolManager>(InversifyTypes.ConnectionPoolManager)
    .toConstantValue(new ConnectionPoolManager(config));

  siteDataStore = container.resolve(SiteDataStoreImpl);
  loggingService = container.get(InversifyTypes.LoggingService);
});

afterEach(() => {
  searchMethodMock.mockReset();
  const config = new MockConfig();
  container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
  siteDataStore = container.resolve(SiteDataStoreImpl);
});

describe('Site data store', () => {
  describe('Logging', () => {
    beforeEach(() => {
      mockLogFn.mockClear();
      requestQueryFn.mockClear();
    });

    describe('getSiteWithChildren', () => {
      it('Should log when an error occurs', async () => {
        try {
          await siteDataStore.getSiteWithChildren(null, null);
        } catch {}

        requestQueryFn.mockRejectedValueOnce(new InducedTestError());

        try {
          await siteDataStore.getSiteWithChildren(null, null);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "Error when trying to retrieve site and site children from database",
                },
                Object {
                  "db": Object {
                    "host": "devci_coreoltp.dev.smartdrivesystems.com",
                    "name": "devci_CoreOLTP",
                  },
                  "functionName": "SiteDataStoreImpl.getSiteWithChildren",
                },
                [TypeError: Cannot read property 'recordset' of undefined],
              ],
              Array [
                Object {
                  "level": "error",
                  "message": "Error when trying to retrieve site and site children from database",
                },
                Object {
                  "db": Object {
                    "host": "devci_coreoltp.dev.smartdrivesystems.com",
                    "name": "devci_CoreOLTP",
                  },
                  "functionName": "SiteDataStoreImpl.getSiteWithChildren",
                },
                [Error: Induced test error],
              ],
            ]
          `);
        }
      });
    });

    describe('getSites', () => {
      it('Should log when an error occurs', async () => {
        try {
          await siteDataStore.getSites(null);
        } catch {}

        requestQueryFn.mockRejectedValueOnce(new InducedTestError());

        try {
          await siteDataStore.getSites([]);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "Error when trying to retrieve sites from database",
                },
                Object {
                  "db": Object {
                    "host": "devci_coreoltp.dev.smartdrivesystems.com",
                    "name": "devci_CoreOLTP",
                  },
                  "functionName": "SiteDataStoreImpl.getSites",
                },
                [TypeError: Cannot read property 'reduce' of null],
              ],
              Array [
                Object {
                  "level": "error",
                  "message": "Error when trying to retrieve sites from database",
                },
                Object {
                  "db": Object {
                    "host": "devci_coreoltp.dev.smartdrivesystems.com",
                    "name": "devci_CoreOLTP",
                  },
                  "functionName": "SiteDataStoreImpl.getSites",
                },
                [Error: Induced test error],
              ],
            ]
          `);
        }
      });
    });
  });
});
