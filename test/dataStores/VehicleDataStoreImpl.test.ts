import { InducedTestError } from './../TestConstants';
import { VehicleDataStoreImpl } from './../../src/dataStores/VehicleDataStoreImpl';
import { Container } from 'inversify';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { MockConfig } from '../mocks/Config.mock';
import { Config } from '../../src/Config';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { ConnectionPoolManager } from '../../src/sql/ConnectionPoolManager';

const requestQueryFn = jest.fn();

jest.mock('mssql', () => {
  return {
    ConnectionPool: function() {
      return {
        connect: jest.fn().mockResolvedValue(true),
      };
    },
    Request: function() {
      return {
        query: requestQueryFn,
        input: jest.fn(),
      };
    },
  };
});

let container: Container;
let vehicleDataStore: VehicleDataStoreImpl;
let loggingService: LoggingService;
const mockLogFn = jest.fn();

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  container
    .bind<ConnectionPoolManager>(InversifyTypes.ConnectionPoolManager)
    .toConstantValue(new ConnectionPoolManager(config));

  vehicleDataStore = container.resolve(VehicleDataStoreImpl);
  loggingService = container.get(InversifyTypes.LoggingService);
});

describe('Vehicle data store', () => {
  describe('Logging', () => {
    beforeEach(() => {
      mockLogFn.mockClear();
      requestQueryFn.mockClear();
    });

    describe('getVehicles', () => {
      it('Should log when an error occurs', async () => {
        requestQueryFn.mockRejectedValueOnce(new InducedTestError());

        try {
          await vehicleDataStore.getVehicles([]);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "Error when trying to retrieve vehicle info from database",
                },
                Object {
                  "db": Object {
                    "host": "devci_coreoltp.dev.smartdrivesystems.com",
                    "name": "devci_CoreOLTP",
                  },
                  "functionName": "VehicleDataStoreImpl.getVehicles",
                },
                [Error: Induced test error],
              ],
            ]
          `);
        }
      });
    });
  });
});
