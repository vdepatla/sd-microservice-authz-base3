import { CompanyDataStoreSqlImpl } from './../../src/dataStores/CompanyDataStoreSqlImpl';
import { Container } from 'inversify';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { Config } from '../../src/Config';
import { ConnectionPoolManager } from '../../src/sql/ConnectionPoolManager';
import { MockConfig } from '../mocks/Config.mock';
import { InducedTestError } from '../../test/TestConstants';

let container: Container;
let companyDataStoreSql: CompanyDataStoreSqlImpl;
let loggingService: LoggingService;
const mockLogFn = jest.fn();

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  container
    .bind<ConnectionPoolManager>(InversifyTypes.ConnectionPoolManager)
    .toConstantValue(new ConnectionPoolManager(config));

  companyDataStoreSql = container.resolve(CompanyDataStoreSqlImpl);
  loggingService = container.get(InversifyTypes.LoggingService);
});

let requestQueryFn = jest.fn();

jest.mock('mssql', () => {
  return {
    ConnectionPool: function() {
      return {
        connect: jest.fn().mockResolvedValue(true),
      };
    },
    Request: function() {
      return {
        query: requestQueryFn,
        input: jest.fn(),
      };
    },
  };
});

describe('Company data store (SQL)', () => {
  describe('Logging', () => {
    beforeEach(() => {
      mockLogFn.mockClear();
      requestQueryFn.mockClear();
    });

    describe('getCompanies', () => {
      it('Should log when an error occurs', async () => {
        requestQueryFn.mockRejectedValueOnce(new InducedTestError());

        try {
          await companyDataStoreSql.getCompanies(['1']);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "An error occurred fetching companies from database",
                },
                Object {
                  "companyIds": Array [
                    "1",
                  ],
                  "functionName": "CompanyDataStoreSqlImpl.getCompanyName",
                  "query": "SELECT CompanyId, CompanyName
                                 FROM Company (NOLOCK) c 
                                 WHERE c.CompanyId in (@companyIds0)
                                 ",
                },
                [Error: Induced test error],
              ],
            ]
          `);
        }
      });
    });
  });
});
