import 'reflect-metadata';
import { MockConfig } from '../mocks/Config.mock';
import { AuthorizationExtractDataUtil } from '../../src/dataStores/AuthorizationExtractDataUtil';

describe('AuthorizationExtractDataUtil', () => {
  test('returns false when data is not latest', async () => {
    const config = new MockConfig();
    const result = AuthorizationExtractDataUtil.isExtractDataLatest(
      [config.ElasticIndexPeopleLoginCredential],
      [],
      config
    );
    expect(result).toBe(false);
  });

  test('returns true when data is latest', async () => {
    const config = new MockConfig();
    const result = AuthorizationExtractDataUtil.isExtractDataLatest([], [], config);
    expect(result).toBe(true);
  });
});
