import { UserDataStoreImpl } from '../../src/dataStores/UserDataStoreImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('User data store', () => {
  describe('getPeopleLoginCredential - metric decorators', () => {
    it('should be using the histogram decorator', () => {
      const histogramMetadata = getDecoratorMetadata(metadataKeys.histogram, UserDataStoreImpl.prototype, 'getPeopleLoginCredential');
      expect(histogramMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "UserDataStoreImpl.getPeopleLoginCredential",
          "data_store": "User Data Store",
          "location": "elasticsearch",
        }
      `);
    });

    it('should be using the errorCounter decorator', () => {
      const errorCounterMetadata = getDecoratorMetadata(metadataKeys.errorCounter, UserDataStoreImpl.prototype, 'getPeopleLoginCredential');
      expect(errorCounterMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "UserDataStoreImpl.getPeopleLoginCredential",
          "data_store": "User Data Store",
          "location": "elasticsearch",
        }
      `);
    });
  });

  describe('gatherRoles - metric decorators', () => {
    it('should be using the histogram decorator', () => {
      const histogramMetadata = getDecoratorMetadata(metadataKeys.histogram, UserDataStoreImpl.prototype, 'gatherRoles');
      expect(histogramMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "UserDataStoreImpl.gatherRoles",
          "data_store": "User Data Store",
          "location": "elasticsearch",
        }
      `);
    });

    it('should be using the errorCounter decorator', () => {
      const errorCounterMetadata = getDecoratorMetadata(metadataKeys.errorCounter, UserDataStoreImpl.prototype, 'gatherRoles');
      expect(errorCounterMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "UserDataStoreImpl.gatherRoles",
          "data_store": "User Data Store",
          "location": "elasticsearch",
        }
      `);
    });
  });

  describe('getUserElastic - metric decorators', () => {
    it('should be using the histogram decorator', () => {
      const histogramMetadata = getDecoratorMetadata(metadataKeys.histogram, UserDataStoreImpl.prototype, 'getUserElastic');
      expect(histogramMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "UserDataStoreImpl.getUserElastic",
          "data_store": "User Data Store",
          "location": "elasticsearch",
        }
      `);
    });

    it('should be using the errorCounter decorator', () => {
      const errorCounterMetadata = getDecoratorMetadata(metadataKeys.errorCounter, UserDataStoreImpl.prototype, 'getUserElastic');
      expect(errorCounterMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "UserDataStoreImpl.getUserElastic",
          "data_store": "User Data Store",
          "location": "elasticsearch",
        }
      `);
    });
  });
});
