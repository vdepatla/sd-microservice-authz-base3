import { UserDataStoreSqlImpl } from '../../src/dataStores/UserDataStoreSqlImpl';
import { Container } from 'inversify';
import { MockConfig } from '../mocks/Config.mock';
import { Config } from '../../src/Config';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { ConnectionPoolManager } from '../../src/sql/ConnectionPoolManager';
import { InducedTestError } from '../TestConstants';

let requestQueryFn = jest.fn();

jest.mock('mssql', () => {
  return {
    ConnectionPool: function() {
      return {
        connect: jest.fn().mockResolvedValue(true),
      };
    },
    Request: function() {
      return {
        query: requestQueryFn,
        input: jest.fn(),
      };
    },
  };
});

let container: Container;
let userDataStoreSql: UserDataStoreSqlImpl;
let loggingService: LoggingService;
const mockLogFn = jest.fn();

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  container
    .bind<ConnectionPoolManager>(InversifyTypes.ConnectionPoolManager)
    .toConstantValue(new ConnectionPoolManager(config));

  userDataStoreSql = container.resolve(UserDataStoreSqlImpl);
  loggingService = container.get(InversifyTypes.LoggingService);
});

describe('User data store (SQL)', () => {
  describe('Logging', () => {
    describe('getUser', () => {
      beforeEach(() => {
        mockLogFn.mockClear();
        requestQueryFn.mockClear();
      });

      it('Should log when an error occurs in the getUser method', async () => {
        requestQueryFn.mockRejectedValueOnce(new InducedTestError());

        try {
          await userDataStoreSql.getUser('hash', 'username');
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "An error occurred when trying to retrieve the User Data from the database",
                },
                Object {
                  "db": Object {
                    "host": "devci_coreoltp.dev.smartdrivesystems.com",
                    "name": "devci_CoreOLTP",
                  },
                  "functionName": "UserDataStoreSqlImpl.getUser",
                  "username": "username",
                },
                [Error: Induced test error],
              ],
            ]
          `);
        }
      });

      it('should log when an error occurs in the private method gatherUserRoles', async () => {
        requestQueryFn.mockRejectedValueOnce(new InducedTestError());

        try {
          await userDataStoreSql.getUser('hash', 'username');
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "An error occurred when trying to retrieve the User Data from the database",
                },
                Object {
                  "db": Object {
                    "host": "devci_coreoltp.dev.smartdrivesystems.com",
                    "name": "devci_CoreOLTP",
                  },
                  "functionName": "UserDataStoreSqlImpl.getUser",
                  "username": "username",
                },
                [Error: Induced test error],
              ],
            ]
          `);
        }
      });
    });
  });
});
