import { Auth0DataStore } from './../../src/dataStores/Auth0DataStore';
import * as rp from 'request-promise-native';
import { Container } from 'inversify';
import { Auth0DataStoreImpl } from './../../src/dataStores/Auth0DataStoreImpl';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { MockConfig } from '../mocks/Config.mock';
import { Config } from '../../src/Config';

jest.mock('request-promise-native');
let auth0DataStore: Auth0DataStore;
let container: Container;
let loggingService: LoggingService;
const mockLogFn = jest.fn();

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container.bind<Auth0DataStore>(InversifyTypes.Auth0DataStore).to(Auth0DataStoreImpl);
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  loggingService = container.get(InversifyTypes.LoggingService);
});

beforeEach(() => {
  auth0DataStore = container.get(InversifyTypes.Auth0DataStore);
});

describe('Auth0 data store', () => {
  describe('Logging', () => {
    beforeEach(() => {
      mockLogFn.mockClear();
    });

    describe('getM2MToken', () => {
      it('should log when an error occurs', async () => {
        const testError = new Error('test message');
        (rp.post as jest.Mock).mockImplementationOnce(() => {
          throw testError;
        });
        try {
          await auth0DataStore.getM2MToken();
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "An error occurred in getting machine to machine token from auth0",
                },
                Object {
                  "ClientID": "bjV4rV95Hzhkyq4zjtwpwCUQzKl86BkD",
                  "functionName": "AccessToken.getM2MToken",
                },
                [Error: test message],
              ],
            ]
          `);
        }
      });

      it('should log as info when a call is made to get machine to machine token', async () => {
        (rp.post as jest.Mock).mockResolvedValueOnce({
          accessToken: 'testToken',
          expiryTimeSeconds: 0,
        });

        await auth0DataStore.getM2MToken();
        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "info",
                "message": "Machine-to-machine token is created for service to service calls",
              },
              Object {
                "functionName": "AccessToken.getM2MToken",
                "response": Object {
                  "accessToken": "testToken",
                  "expiryTimeSeconds": 0,
                },
              },
            ],
          ]
        `);
      });
    });
  });
});
