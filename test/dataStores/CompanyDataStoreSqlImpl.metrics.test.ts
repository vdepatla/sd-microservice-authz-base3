import { CompanyDataStoreSqlImpl } from '../../src/dataStores/CompanyDataStoreSqlImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Company data store', () => {
  describe('getCompanies', () => {
    const histogramMetadata = getDecoratorMetadata(
      metadataKeys.histogram,
      CompanyDataStoreSqlImpl.prototype,
      'getCompanies'
    );

    it('should be using the histogram decorator', () => {
      expect(histogramMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "CompanyDataStoreSqlImpl.getCompanies",
          "data_store": "Company Data Store SQL",
          "location": "core_oltp",
        }
      `);
    });

    const errorCounterMetadata = getDecoratorMetadata(
      metadataKeys.errorCounter,
      CompanyDataStoreSqlImpl.prototype,
      'getCompanies'
    );

    it('should be using the histogram decorator', () => {
      expect(errorCounterMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "CompanyDataStoreSqlImpl.getCompanies",
          "data_store": "Company Data Store SQL",
          "location": "core_oltp",
        }
      `);
    });
  });
});
