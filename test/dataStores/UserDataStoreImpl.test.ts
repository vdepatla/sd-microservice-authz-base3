import { UserDataStoreImpl } from '../../src/dataStores/UserDataStoreImpl';
import { Container } from 'inversify';
import { ElasticsearchClient } from '../../src/models/ElasticSearch/ElasticsearchClient';
import { AuthorizationExtractDataStore } from '../../src/dataStores/AuthorizationExtractDataStore';
import { ElasticsearchClientMock } from '../mocks/ElasticsearchClientMock';
import { AuthorizationExtractDataStoreMock } from '../mocks/AuthorizationExtractDataStoreMock';
import { AuthorizationExtractDataUtil } from '../../src/dataStores/AuthorizationExtractDataUtil';
import { UserDataStoreSql } from '../../src/dataStores/UserDataStoreSql';
import { UserDataStoreSqlImplMock } from '../mocks/UserDataStoreSqlImplMock';
import { ExtractRefreshTimeCache } from '../../src/dataStores/ExtractRefreshTimeCache';
import { ExtractRefreshTimeCacheMock } from '../mocks/ExtractRefreshTimeCacheMock';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { MockConfig } from '../mocks/Config.mock';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { Config } from '../../src/Config';
import { InducedTestError } from '../TestConstants';

const searchMethodMock = jest.fn();
const islatestSpy = jest.spyOn(AuthorizationExtractDataUtil, 'isExtractDataLatest');

const mockLogFn = jest.fn();

jest.mock('@elastic/elasticsearch', () => ({
  // tslint:disable-next-line: object-literal-shorthand
  Client: function() {
    return {
      search: searchMethodMock,
    };
  },
}));

let container: Container;
let userDataStore: UserDataStoreImpl;
let loggingService: LoggingService;

beforeAll(() => {
  const config = new MockConfig();
  container = new Container();
  container.bind<ElasticsearchClient>(InversifyTypes.ElasticsearchClient).to(ElasticsearchClientMock);
  container.bind<UserDataStoreSql>(InversifyTypes.UserDataStoreSql).to(UserDataStoreSqlImplMock);
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<ExtractRefreshTimeCache>(InversifyTypes.ExtractRefreshTimeCache).to(ExtractRefreshTimeCacheMock);
  container
    .bind<AuthorizationExtractDataStore>(InversifyTypes.AuthorizationExtractDataStore)
    .to(AuthorizationExtractDataStoreMock);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });

  loggingService = container.get(InversifyTypes.LoggingService);
  userDataStore = container.resolve(UserDataStoreImpl);
});

afterEach(() => {
  searchMethodMock.mockReset();
  islatestSpy.mockClear();
  const config = new MockConfig();
  container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
  userDataStore = container.resolve(UserDataStoreImpl);
});

describe('User Data Store Impl', () => {
  describe('getUser', () => {
    test('getUser - sql fallback occurs when config is turned on', async () => {
      const config = new MockConfig(true);
      container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
      userDataStore = container.resolve(UserDataStoreImpl);
      const result = await userDataStore.getUser('mock', 'mocksqluser');
      expect(result).toMatchInlineSnapshot(`
        User {
          "companyId": "1",
          "roles": Array [],
          "siteId": "666",
          "siteIsActive": 1,
          "userId": "1",
          "username": "mocksqluser",
        }
      `);
    });

    test('getUser - sql fallback occurs when error happens', async () => {
      islatestSpy.mockImplementation(() => {
        return true;
      });
      searchMethodMock.mockRejectedValueOnce(new InducedTestError());
      const result = await userDataStore.getUser('mock', 'mock');
      expect(result).toMatchInlineSnapshot(`
        User {
          "companyId": "1",
          "roles": Array [],
          "siteId": "666",
          "siteIsActive": 1,
          "userId": "1",
          "username": "mock",
        }
      `);
    });

    test('getUser - sql fallback occurs when data is stale', async () => {
      islatestSpy.mockImplementation(() => {
        return false;
      });
      const result = await userDataStore.getUser('mock', 'mock');
      expect(result).toMatchInlineSnapshot(`
        User {
          "companyId": "1",
          "roles": Array [],
          "siteId": "666",
          "siteIsActive": 1,
          "userId": "1",
          "username": "mock",
        }
      `);
    });

    describe('logging', () => {
      beforeEach(() => {
        mockLogFn.mockClear();
      });

      describe('getPeopleLoginCredential', () => {
        it('logs verbose when getting people login credentials', async () => {
          searchMethodMock.mockResolvedValueOnce({
            body: {
              hits: {
                total: 1,
                hits: [
                  {
                    _source: {
                      userName: 'bob',
                      companyHash: '123ABC',
                      companySeed: '0BJ2',
                    },
                  },
                ],
              },
            },
          });
          await userDataStore.getPeopleLoginCredential('0BJ2', 'bob');
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "verbose",
                  "message": "Initiating Elasticsearch call for get people login credentials",
                },
                Object {
                  "companyHash": "0BJ2",
                  "functionName": "UserDataStoreImpl.getPeopleLoginCredential",
                  "username": "bob",
                },
              ],
            ]
          `);
        });

        it('logs a warning when credentials are not found for the user', async () => {
          searchMethodMock.mockResolvedValueOnce({
            body: {
              hits: {
                total: 0,
                hits: [],
              },
            },
          });
          await userDataStore.getPeopleLoginCredential('0BJ2', 'bob');
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "verbose",
                  "message": "Initiating Elasticsearch call for get people login credentials",
                },
                Object {
                  "companyHash": "0BJ2",
                  "functionName": "UserDataStoreImpl.getPeopleLoginCredential",
                  "username": "bob",
                },
              ],
              Array [
                Object {
                  "level": "warn",
                  "message": "People login credential information not found for the user",
                },
                Object {
                  "companyHash": "0BJ2",
                  "functionName": "UserDataStoreImpl.getPeopleLoginCredential",
                  "username": "bob",
                },
              ],
            ]
          `);
        });

        it('logs an error when an exception occurs during the search attempt', async () => {
          searchMethodMock.mockRejectedValueOnce(new Error('fake test error'));

          try {
            await userDataStore.getPeopleLoginCredential('0BJ2', 'bob');
          } catch {
            expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
          }
        });
      });

      describe('getUser', () => {
        beforeEach(() => {
          mockLogFn.mockClear();
        });

        it('logs info message if USE_SQL_ONLY and deffers immediately to SQL', async () => {
          const config = new MockConfig(true);
          container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
          userDataStore = container.resolve(UserDataStoreImpl);

          expect(await userDataStore.getUser('0BJ2', 'bob')).toMatchInlineSnapshot(`
            User {
              "companyId": "1",
              "roles": Array [],
              "siteId": "666",
              "siteIsActive": 1,
              "userId": "1",
              "username": "bob",
            }
          `);
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "info",
                  "message": "Configuration is turned on to fall back to SQL",
                },
                Object {
                  "functionName": "UserDataStoreImpl.getUserElastic",
                },
              ],
            ]
          `);
        });

        it('logs info message if it has been too long since the last refresh', async () => {
          islatestSpy.mockImplementation(() => {
            return false;
          });

          expect(await userDataStore.getUser('0BJ2', 'bob')).toMatchInlineSnapshot(`
            User {
              "companyId": "1",
              "roles": Array [],
              "siteId": "666",
              "siteIsActive": 1,
              "userId": "1",
              "username": "bob",
            }
          `);
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "info",
                  "message": "last refresh information not found or refresh time is greater than configured minutes. fall back to SQL will be attempted",
                },
                Object {
                  "deltaRefreshTolerance": 240.2,
                  "fullRefreshTolerance": 240.5,
                  "functionName": "UserDataStoreImpl.getUserElastic",
                  "processExtractResponse": Array [
                    Object {
                      "index": "people_login_credentials_devci",
                      "refreshTimeUnix": 949363200,
                    },
                    Object {
                      "index": "role_names_devci",
                      "refreshTimeUnix": 949363200,
                    },
                  ],
                },
              ],
            ]
          `);
        });

        it('logs an error if one of the sub-calls throws an exception', async () => {
          islatestSpy.mockImplementation(() => {
            return true;
          });
          searchMethodMock.mockRejectedValueOnce(new Error('fake test error'));

          try {
            await userDataStore.getUser('0BJ2', 'bob');
            expect(true).toBe(false);
          } catch {
            expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
          }
        });
      });

      describe('gatherRoles', () => {
        it('logs a verbose message about gathering roles for a user', async () => {
          searchMethodMock.mockResolvedValueOnce({
            body: {
              hits: {
                total: 0,
              },
            },
          });
          await userDataStore.gatherRoles([]);
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "verbose",
                  "message": "Initiating Elasticsearch call to get role names",
                },
                Object {
                  "functionName": "UserDataStoreImpl.gatherRoles",
                  "roleIdList": Array [],
                },
              ],
            ]
          `);
        });

        it('logs a verbose message reporting that a block of roles was obtained from ES', async () => {
          searchMethodMock.mockResolvedValueOnce({
            body: {
              hits: {
                total: 2,
                hits: [
                  {
                    _source: { roleName: 'FLT' },
                  },
                  {
                    _source: { roleName: 'COU' },
                  },
                ],
              },
            },
          });
          await userDataStore.gatherRoles([]);
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "verbose",
                  "message": "Initiating Elasticsearch call to get role names",
                },
                Object {
                  "functionName": "UserDataStoreImpl.gatherRoles",
                  "roleIdList": Array [],
                },
              ],
              Array [
                Object {
                  "level": "verbose",
                  "message": "Received 2 of 2 role names.",
                },
                Object {
                  "functionName": "UserDataStoreImpl.gatherRoles",
                  "roleIdList": Array [],
                },
              ],
            ]
          `);
        });

        it('logs an error if it fails to retreive roles', async () => {
          searchMethodMock.mockRejectedValueOnce(new Error('fake test error'));

          try {
            await userDataStore.gatherRoles([1, 2]);
          } catch {
            expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
              Array [
                Array [
                  Object {
                    "level": "verbose",
                    "message": "Initiating Elasticsearch call to get role names",
                  },
                  Object {
                    "functionName": "UserDataStoreImpl.gatherRoles",
                    "roleIdList": Array [
                      1,
                      2,
                    ],
                  },
                ],
                Array [
                  Object {
                    "level": "error",
                    "message": "An error occurred in fetching role names",
                  },
                  Object {
                    "functionName": "UserDataStoreImpl.gatherRoles",
                    "maxRetries": 3,
                  },
                  [Error: fake test error],
                ],
              ]
            `);
          }
        });
      });
    });
  });
});
