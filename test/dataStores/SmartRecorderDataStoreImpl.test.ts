import { TestConstants, InducedTestError } from '../TestConstants';
import { SmartRecorderDataStoreImpl } from '../../src/dataStores/SmartRecorderDataStoreImpl';
import { Container } from 'inversify';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { MockConfig } from '../mocks/Config.mock';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { Config } from '../../src/Config';
import { ConnectionPoolManager } from '../../src/sql/ConnectionPoolManager';
import { SmartRecorderDataStore } from '../../src/dataStores/SmartRecorderDataStore';

const searchMethodMock = jest.fn();

let container: Container;
let smartRecorderDataStore: SmartRecorderDataStore;
let requestQueryFn = jest.fn();
let loggingService: LoggingService;
const mockLogFn = jest.fn();

jest.mock('mssql', () => {
  return {
    ConnectionPool: function() {
      return {
        connect: jest.fn().mockResolvedValue(true),
      };
    },
    Request: function() {
      return {
        query: requestQueryFn,
        input: jest.fn(),
      };
    },
  };
});

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  container
    .bind<ConnectionPoolManager>(InversifyTypes.ConnectionPoolManager)
    .toConstantValue(new ConnectionPoolManager(config));

  smartRecorderDataStore = container.resolve(SmartRecorderDataStoreImpl);
  loggingService = container.get(InversifyTypes.LoggingService);
});

afterEach(() => {
  searchMethodMock.mockReset();
  const config = new MockConfig();
  container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
  smartRecorderDataStore = container.resolve(SmartRecorderDataStoreImpl);
});

describe('Smart Recorder Data Store', () => {
  describe('Logging', () => {
    describe('getSmartRecorders', () => {
      it('should log when an error occurs', async () => {
        try {
          requestQueryFn.mockRejectedValue(new InducedTestError());
          await smartRecorderDataStore.getSmartRecorders(['2'], ['sd']);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "level": "error",
                  "message": "Error when trying to retrieve smart recorder info from database",
                },
                Object {
                  "db": Object {
                    "host": "devci_coreoltp.dev.smartdrivesystems.com",
                    "name": "devci_CoreOLTP",
                  },
                  "functionName": "SmartRecorderDataStoreImpl.getSmartRecorders",
                },
                [Error: Induced test error],
              ],
            ]
          `);
        }
      });
    });
  });
});
