import { AuthorizationDataStoreImpl } from '../../src/dataStores/AuthorizationDataStoreImpl';
import { AuthorizationDataStore } from '../../src/dataStores/AuthorizationDataStore';
import { Container } from 'inversify';
import { UserDataStore } from '../../src/dataStores/UserDataStore';
import { ElasticsearchClient } from '../../src/models/ElasticSearch/ElasticsearchClient';
import { AuthorizationExtractDataStore } from '../../src/dataStores/AuthorizationExtractDataStore';
import { Auth0DataStore } from '../../src/dataStores/Auth0DataStore';
import { AccessTokenCacheMock } from '../mocks/AccessTokenCacheMock';
import { UserDataStoreMock } from '../mocks/UserDataStoreMock';
import { AccessTokenCache } from '../../src/dataStores/AccessTokenCache';
import { Auth0DataStoreMock } from '../mocks/Auth0DataStoreMock';
import { AuthorizationExtractDataStoreMock } from '../mocks/AuthorizationExtractDataStoreMock';
import { AuthorizationDataStoreSqlImplMock } from '../mocks/AuthorizationDataStoreSqlImplMock';
import { UserDataStoreSqlImplMock } from '../mocks/UserDataStoreSqlImplMock';
import { AuthorizationDataStoreSql } from '../../src/dataStores/AuthorizationDataStoreSql';
import { UserDataStoreSql } from '../../src/dataStores/UserDataStoreSql';
import { ExtractRefreshTimeCache } from '../../src/dataStores/ExtractRefreshTimeCache';
import { ExtractRefreshTimeCacheMock } from '../mocks/ExtractRefreshTimeCacheMock';
import { InducedTestError } from '../TestConstants';
import { ElasticsearchClientImpl } from '../../src/models/ElasticSearch/ElasticsearchClientImpl';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { LoggingService, BaseMetricsService } from '@sd-microservice-base/instrumentation';
import { Config } from '../../src/Config';
import { MockConfig } from '../mocks/Config.mock';
import { RedisDataStore, BaseRedisDataStore } from '@sd-microservice-base/redis';
import * as client from 'prom-client';
import { AuthorizationExtractDataUtil } from '../../src/dataStores/AuthorizationExtractDataUtil';
import { AuthorizationResult } from '../../src/models/AuthorizationResult';
import { LoginEnabled } from '../../src/models/LoginEnabled';

const searchMethodMock = jest.fn();
const mockLogFn = jest.fn();
const islatestSpy = jest.spyOn(AuthorizationExtractDataUtil, 'isExtractDataLatest');

jest.mock('@elastic/elasticsearch', () => ({
  // tslint:disable-next-line: object-literal-shorthand
  Client: function() {
    return {
      search: searchMethodMock,
    };
  },
}));

let container: Container;
let authorizationDataStore: AuthorizationDataStore;
let loggingService: LoggingService;

const mockResource = 'mockResourceES';
const mockRole = 'mockRoleES';
const mockPermission = 'mockPermissionES';
const mockPermissionScope = 'mockPermissionScopeES';

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container.bind<UserDataStore>(InversifyTypes.UserDataStore).to(UserDataStoreMock);
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  container.bind<ElasticsearchClient>(InversifyTypes.ElasticsearchClient).to(ElasticsearchClientImpl);
  container
    .bind<AuthorizationExtractDataStore>(InversifyTypes.AuthorizationExtractDataStore)
    .to(AuthorizationExtractDataStoreMock);
  container.bind<AccessTokenCache>(InversifyTypes.AccessTokenCache).to(AccessTokenCacheMock);
  container.bind<ExtractRefreshTimeCache>(InversifyTypes.ExtractRefreshTimeCache).to(ExtractRefreshTimeCacheMock);
  container.bind<Auth0DataStore>(InversifyTypes.Auth0DataStore).to(Auth0DataStoreMock);
  container.bind<UserDataStoreSql>(InversifyTypes.UserDataStoreSql).to(UserDataStoreSqlImplMock);
  container
    .bind<AuthorizationExtractDataUtil>(InversifyTypes.AuthorizationExtractDataUtil)
    .to(AuthorizationExtractDataUtil);
  container
    .bind<AuthorizationDataStoreSql>(InversifyTypes.AuthorizationDataStoreSql)
    .to(AuthorizationDataStoreSqlImplMock);
  loggingService = container.get(InversifyTypes.LoggingService);
  authorizationDataStore = container.resolve(AuthorizationDataStoreImpl);
});

afterEach(() => {
  searchMethodMock.mockReset();
  islatestSpy.mockClear();
  const config = new MockConfig();
  container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
  authorizationDataStore = container.resolve(AuthorizationDataStoreImpl);
});

describe('AuthorizationDataStoreImpl DataStore', () => {
  describe('getPermissions', () => {
    test('sql fallback occurs when config is turned on', async () => {
      const config = new MockConfig(true);
      container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
      authorizationDataStore = container.resolve(AuthorizationDataStoreImpl);
      const result = await authorizationDataStore.getPermissions('mock', 'mock', ['mock']);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "permission": "mockPermissionSQL",
            "permissionScope": "mockPermissionScopeSQL",
            "resource": "mockSqlResourceSQL",
            "role": "mockRoleSQL",
            "userScope": "mockUserScopeSQL",
          },
        ]
      `);
    });

    test('sql fallback occurs when error happens', async () => {
      islatestSpy.mockImplementation(() => {
        return true;
      });
      searchMethodMock.mockRejectedValueOnce(new Error('fake test error'));
      let result: AuthorizationResult[];
      try {
        result = await authorizationDataStore.getPermissions('mock', 'mock', ['mock']);
      } catch {
        expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "permission": "mockPermissionSQL",
            "permissionScope": "mockPermissionScopeSQL",
            "resource": "mockSqlResourceSQL",
            "role": "mockRoleSQL",
            "userScope": "mockUserScopeSQL",
          },
        ]
      `);
      }
    });

    test('sql fallback occurs when data is stale', async () => {
      islatestSpy.mockImplementation(() => {
        return false;
      });
      const result = await authorizationDataStore.getPermissions('mock', 'mock', ['mock']);
      expect(result).toMatchInlineSnapshot(`
        Array [
          Object {
            "permission": "mockPermissionSQL",
            "permissionScope": "mockPermissionScopeSQL",
            "resource": "mockSqlResourceSQL",
            "role": "mockRoleSQL",
            "userScope": "mockUserScopeSQL",
          },
        ]
      `);
    });

    test('getPermissions - elastic search returns results', async () => {
      islatestSpy.mockImplementation(() => {
        return true;
      });
      searchMethodMock.mockReturnValueOnce({
        body: {
          hits: {
            total: 1,
            hits: [
              {
                _source: {
                  resource: mockResource,
                  name: mockRole,
                  permission: mockPermission,
                  permissionScope: mockPermissionScope,
                },
              },
            ],
          },
        },
      });
      const result = await authorizationDataStore.getPermissions('mockHash', 'mockUsername', []);
      expect(result).toHaveLength(1);
      expect(result[0].resource).toStrictEqual(mockResource);
      expect(result[0].role).toStrictEqual(mockRole);
      expect(result[0].permission).toStrictEqual(mockPermission);
      expect(result[0].permissionScope).toStrictEqual(mockPermissionScope);
    });
  });

  describe('getLoginEnabled', () => {
    test('sql fallback occurs when config is turned on', async () => {
      const config = new MockConfig(true);
      container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
      authorizationDataStore = container.resolve(AuthorizationDataStoreImpl);
      const result = await authorizationDataStore.getLoginEnabled('mock', 'mock');
      expect(result).toMatchInlineSnapshot(`undefined`);
    });

    test('sql fallback occurs when error happens', async () => {
      islatestSpy.mockImplementationOnce(() => {
        return true;
      });
      searchMethodMock.mockReturnValueOnce(new InducedTestError());
      let result: LoginEnabled;
      try {
        result = await authorizationDataStore.getLoginEnabled('mock', 'mock');
      } catch {
        expect(result).toMatchInlineSnapshot(`undefined`);
      }
    });

    test('sql fallback occurs when data is stale', async () => {
      islatestSpy.mockImplementationOnce(() => {
        return false;
      });
      const result = await authorizationDataStore.getLoginEnabled('mock', 'mock');
      expect(result).toMatchInlineSnapshot(`undefined`);
    });

    test('getLoginEnabled - elastic search returns results', async () => {
      islatestSpy.mockImplementationOnce(() => {
        return true;
      });
      searchMethodMock.mockReturnValueOnce({
        body: {
          hits: {
            total: 1,
            hits: [
              {
                _source: {
                  isLocked: 'false',
                },
              },
            ],
          },
        },
      });
      const result = await authorizationDataStore.getLoginEnabled('mockHash', 'mockUsername');
      expect(result).toMatchInlineSnapshot(`
        Object {
          "isLocked": false,
        }
      `);
    });
  });

  describe('Logging', () => {
    beforeEach(() => {
      mockLogFn.mockClear();
    });

    afterEach(() => {
      searchMethodMock.mockReset();
      const config = new MockConfig(false);
      container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
      authorizationDataStore = container.resolve(AuthorizationDataStoreImpl);
    });

    describe('getPermissions', () => {
      it('Should log when SQL configuration turned on', async () => {
        const config = new MockConfig(true);
        container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
        authorizationDataStore = container.resolve(AuthorizationDataStoreImpl);
        await authorizationDataStore.getPermissions('mockHash', 'mockUsername', []);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
      });

      it('Should log verbose when elastic search call is made', async () => {
        islatestSpy.mockImplementationOnce(() => {
          return true;
        });
        searchMethodMock.mockReturnValueOnce({
          body: {
            hits: {
              total: 1,
              hits: [
                {
                  _source: {
                    resource: mockResource,
                    name: mockRole,
                    permission: mockPermission,
                    permissionScope: mockPermissionScope,
                  },
                },
              ],
            },
          },
        });
        const result = await authorizationDataStore.getPermissions('mockHash', 'mockUsername', []);
        expect(result).toHaveLength(1);
        expect(result[0].resource).toStrictEqual(mockResource);
        expect(result[0].role).toStrictEqual(mockRole);
        expect(result[0].permission).toStrictEqual(mockPermission);
        expect(result[0].permissionScope).toStrictEqual(mockPermissionScope);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
      });

      it('Should log when last extract information is stale', async () => {
        islatestSpy.mockImplementation(() => {
          return false;
        });
        await authorizationDataStore.getPermissions('mockHash', 'mockUsername', []);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
      });

      it('Should log when an error occurs', async () => {
        islatestSpy.mockImplementation(() => {
          return true;
        });
        searchMethodMock.mockImplementationOnce(async () => {
          throw new InducedTestError();
        });
        try {
          await authorizationDataStore.getPermissions('mockHash', 'mockUsername', []);
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
        }
      });
    });

    describe('getLoginEnabled', () => {
      it('Should log when SQL configuration turned on', async () => {
        const config = new MockConfig(true);
        container.rebind<Config>(InversifyTypes.Config).toConstantValue(config);
        authorizationDataStore = container.resolve(AuthorizationDataStoreImpl);
        await authorizationDataStore.getLoginEnabled('mockHash', 'mockUsername');
        expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
      });

      it('Should log verbose when elastic search call is made', async () => {
        islatestSpy.mockImplementation(() => {
          return true;
        });
        searchMethodMock.mockReturnValueOnce({
          body: {
            hits: {
              total: 1,
              hits: [
                {
                  _source: {
                    isLocked: 'false',
                  },
                },
              ],
            },
          },
        });
        await authorizationDataStore.getLoginEnabled('mockHash', 'mockUsername');
        expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
      });

      it('Should log when last extract information is stale', async () => {
        islatestSpy.mockImplementation(() => {
          return false;
        });
        await authorizationDataStore.getLoginEnabled('mockHash', 'mockUsername');
        expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
      });

      it('Should log when an error occurs', async () => {
        islatestSpy.mockImplementation(() => {
          return true;
        });
        searchMethodMock.mockImplementationOnce(async () => {
          throw new InducedTestError();
        });
        try {
          await authorizationDataStore.getLoginEnabled('mockHash', 'mockUsername');
        } catch {
          expect((loggingService.log as jest.Mock).mock.calls).toMatchSnapshot();
        }
      });
    });
  });
});
