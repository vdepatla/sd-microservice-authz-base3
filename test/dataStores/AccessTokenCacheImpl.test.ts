import 'reflect-metadata';
import { AccessTokenCacheImpl } from '../../src/dataStores/AccessTokenCacheImpl';
import { AccessTokenResponse } from '../../src/models/AccessTokenResponse';
import { Container } from 'inversify';
import { LoggingService, BaseMetricsService } from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { AccessTokenCache } from '../../src/dataStores/AccessTokenCache';
import { RedisDataStore, BaseRedisDataStore } from '@sd-microservice-base/redis';
import { MockConfig } from './../mocks/Config.mock';
import * as client from 'prom-client';

const mockGetFn = jest.fn();
const mockSetFn = jest.fn();

let container: Container;
let loggingService: LoggingService;
let accessTokenCache: AccessTokenCache;

const mockLogFn = jest.fn();
jest.mock('ioredis', () => {
  return {
    Cluster: function() {
      return {
        get: mockGetFn,
        set: mockSetFn,
        on: jest.fn(),
      };
    },
  };
});

beforeAll(() => {
  container = new Container();
  const config = new MockConfig();
  container
    .bind<RedisDataStore>(InversifyTypes.RedisDataStore)
    .toConstantValue(new BaseRedisDataStore(config, new BaseMetricsService(client.register)));
  container.bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue({
    logs: {},
    log: mockLogFn,
    addLogs: () => {},
    replaceLogs: () => {},
    getLogs: (): any => {},
  });
  loggingService = container.get(InversifyTypes.LoggingService);
  accessTokenCache = container.resolve(AccessTokenCacheImpl);
});

beforeEach(() => {
  mockGetFn.mockReset();
  mockSetFn.mockReset();
});

describe('Access Token Cache Impl', () => {
  const mockTokenResponse: AccessTokenResponse = {
    accessToken: 'mock token value',
    expiryTimeSeconds: 60,
  };

  describe('getAccessToken', () => {
    it('returns the cache hit if our key is in the cache', async () => {
      const expected = 'cacheHit';
      mockGetFn.mockResolvedValueOnce(expected);

      const result = await accessTokenCache.getAccessToken('testKey', async () => {
        return mockTokenResponse;
      });
      expect(result).toEqual(expected);
    });

    it('returns a fallback value if not in the cache', async () => {
      mockGetFn.mockResolvedValueOnce(undefined);

      const result = await accessTokenCache.getAccessToken('testKey', async () => {
        return mockTokenResponse;
      });
      expect(mockSetFn).toBeCalled();
      expect(result).toEqual(mockTokenResponse.accessToken);
    });
  });

  describe('logging', () => {
    beforeEach(() => {
      mockLogFn.mockClear();
    });

    describe('getAccessToken', () => {
      it('logs verbose entry when we have a cache hit', async () => {
        mockGetFn.mockResolvedValueOnce('mockToken');
        await accessTokenCache.getAccessToken('testKey', async () => {
          return mockTokenResponse;
        });

        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "verbose",
                "message": "Found the value in redis for the given key",
              },
              Object {
                "functionName": "AccessTokenCacheImpl.readCacheValue",
                "key": "testKey",
              },
            ],
          ]
        `);
      });

      it('logs verbose that key is not in cache before moving on to using the fallback function', async () => {
        mockGetFn.mockResolvedValueOnce(undefined);

        await accessTokenCache.getAccessToken('testKey', async () => {
          return mockTokenResponse;
        });

        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "verbose",
                "message": "Value not found in redis for the given key",
              },
              Object {
                "functionName": "AccessTokenCacheImpl.readCacheValue",
                "key": "testKey",
              },
            ],
          ]
        `);
      });

      it('logs an error if an exception occurs when trying to get the token from redis - goes to fallback', async () => {
        mockGetFn.mockRejectedValueOnce(new Error('fake test error'));

        const result = await accessTokenCache.getAccessToken('testKey', async () => {
          return mockTokenResponse;
        });

        expect(result).toEqual(mockTokenResponse.accessToken);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "error",
                "message": "Error occurred reading value from redis",
              },
              Object {
                "functionName": "AccessTokenCacheImpl.readCacheValue",
                "key": "testKey",
              },
              [Error: fake test error],
            ],
          ]
        `);
      });

      it('logs an error if an exception occurs when trying to set the new token into redis - still returns fallback', async () => {
        mockGetFn.mockResolvedValueOnce(undefined);
        mockSetFn.mockRejectedValueOnce(new Error('fake test error'));

        const result = await accessTokenCache.getAccessToken('testKey', async () => {
          return mockTokenResponse;
        });

        expect(result).toEqual(mockTokenResponse.accessToken);
        expect((loggingService.log as jest.Mock).mock.calls).toMatchInlineSnapshot(`
          Array [
            Array [
              Object {
                "level": "verbose",
                "message": "Value not found in redis for the given key",
              },
              Object {
                "functionName": "AccessTokenCacheImpl.readCacheValue",
                "key": "testKey",
              },
            ],
            Array [
              Object {
                "level": "error",
                "message": "Error occurred writing value to redis",
              },
              Object {
                "functionName": "AccessTokenCacheImpl.writeCacheValue",
                "key": "testKey",
              },
              [Error: fake test error],
            ],
          ]
        `);
      });
    });
  });
});
