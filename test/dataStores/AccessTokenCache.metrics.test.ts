import { AccessTokenCacheImpl } from '../../src/dataStores/AccessTokenCacheImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';
import 'reflect-metadata';

describe('Access token cache', () => {
  describe('readCacheValue', () => {
    it('should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.histogram, AccessTokenCacheImpl.prototype, 'readCacheValue');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "AccessTokenCacheImpl.readCacheValue",
          "data_store": "Access Token Cache",
          "location": "redis",
        }
      `);
    });
  });

  describe('writeCacheValue', () => {
    it('Should be using the histogram decorator', () => {
      const metadata = getDecoratorMetadata(metadataKeys.histogram, AccessTokenCacheImpl.prototype, 'writeCacheValue');
      expect(metadata.labelValues).toMatchInlineSnapshot(`
      Object {
        "action": "create",
        "context": "AccessTokenCacheImpl.writeCacheValue",
        "data_store": "Access Token Cache",
        "location": "redis",
      }
      `);
    });
  });
});
