import { CompanyDataStoreImpl } from '../../src/dataStores/CompanyDataStoreImpl';
import { getDecoratorMetadata, metadataKeys } from '@sd-microservice-base/instrumentation';

describe('Company data store', () => {
  describe('getCompanies', () => {
    it('should be using the histogram decorator', () => {
      const histogramMetadata = getDecoratorMetadata(
        metadataKeys.histogram,
        CompanyDataStoreImpl.prototype,
        'getCompanies'
      );
      expect(histogramMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "CompanyDataStoreImpl.getCompanies",
          "data_store": "Company Data Store ES",
          "location": "elasticsearch",
        }
      `);
    });

    it('should be using the histogram decorator', () => {
      const errorCounterMetadata = getDecoratorMetadata(
        metadataKeys.errorCounter,
        CompanyDataStoreImpl.prototype,
        'getCompanies'
      );
      expect(errorCounterMetadata.labelValues).toMatchInlineSnapshot(`
        Object {
          "action": "read",
          "context": "CompanyDataStoreImpl.getCompanies",
          "data_store": "Company Data Store ES",
          "location": "elasticsearch",
        }
      `);
    });
  });
});
