import 'reflect-metadata';
import {
  BaseLoggingService,
  BaseMetricsService,
  LoggingService,
  MetricsService,
} from '@sd-microservice-base/instrumentation';
// tslint:disable-next-line:no-import-side-effect
import { BaseGqlExpressMicroservice } from '@sd-microservice-base/express';
import { Config } from '../../src/Config';
// import the service's IoC container modules and required interfaces to be resolved
import { Container } from 'inversify';
import { InversifyTypes } from '../../src/models/InversifyTypes';
import { GQLSchemaFactory } from '../../src/gql/GQLSchemaFactory';
import { ApplicationLogs } from '../../src/models/ApplicationLogs';
import { TokenValidator } from '@sd-microservice-base/security';
import { AuthorizationDataStoreMock } from './../mocks/AuthorizationDataStoreMock';
import { VehicleDataStoreMock } from './../mocks/VehicleDataStoreMock';
import { ElasticsearchClientMock } from './../mocks/ElasticsearchClientMock';
import { UserDataStoreMock } from './../mocks/UserDataStoreMock';
import { SmartRecorderDataStoreMock } from './../mocks/SmartRecorderDataStoreMock';
import { SiteDataStoreMock } from './../mocks/SiteDataStoreMock';
import { CompanyDataStoreMock } from './../mocks/CompanyDataStoreMock';
import { Auth0DataStoreMock } from './../mocks/Auth0DataStoreMock';
import { AccessTokenCacheMock } from './../mocks/AccessTokenCacheMock';
import { AuthorizationDataStoreSqlImplMock } from './../mocks/AuthorizationDataStoreSqlImplMock';
import { CompanyDataStoreSqlImplMock } from './../mocks/CompanyDataStoreSqlImplMock';
import { AuthorizationExtractDataStoreMock } from './../mocks/AuthorizationExtractDataStoreMock';
import { ExtractRefreshTimeCacheMock } from './../mocks/ExtractRefreshTimeCacheMock';
import { MockTokenValidator } from './../mocks/MockTokenValidator';
import { UserDataStoreSqlImplMock } from './../mocks/UserDataStoreSqlImplMock';

import { AuthorizationDataStore } from '../../src/dataStores/AuthorizationDataStore';
import { UserDataStore } from '../../src/dataStores/UserDataStore';
import { SiteDataStore } from '../../src/dataStores/SiteDataStore';
import { VehicleDataStore } from '../../src/dataStores/VehicleDataStore';
import { SmartRecorderDataStore } from '../../src/dataStores/SmartRecorderDataStore';
import { CompanyDataStore } from '../../src/dataStores/CompanyDataStore';
import { Auth0DataStore } from '../../src/dataStores/Auth0DataStore';
import { ElasticsearchClient } from '../../src/models/ElasticSearch/ElasticsearchClient';
import { AccessTokenCache } from '../../src/dataStores/AccessTokenCache';
import { AuthorizationDataStoreSql } from '../../src/dataStores/AuthorizationDataStoreSql';
import { UserDataStoreSql } from '../../src/dataStores/UserDataStoreSql';
import { CompanyDataStoreSql } from '../../src/dataStores/CompanyDataStoreSql';
import { AuthorizationExtractDataStore } from '../../src/dataStores/AuthorizationExtractDataStore';
import { ExtractRefreshTimeCache } from '../../src/dataStores/ExtractRefreshTimeCache';
import { AccessDataStoreMock } from './../mocks/AccessDataStoreMock';
import { AccessDataStore } from '../../src/dataStores/AccessDataStore';
import * as client from 'prom-client';
import { RedisDataStore, BaseRedisDataStore } from '@sd-microservice-base/redis';

export const createExpressService = (config: Config): BaseGqlExpressMicroservice => {
  const container = new Container();
  container.bind<Config>(InversifyTypes.Config).toConstantValue(config);
  container
    .bind<LoggingService>(InversifyTypes.LoggingService)
    .toConstantValue(
      new BaseLoggingService(config, ApplicationLogs, [{ replace: 'componentName', with: config.ServiceName }])
    );
  container
    .bind<AuthorizationDataStore>(InversifyTypes.AuthorizationDataStore)
    .to(AuthorizationDataStoreMock)
    .inSingletonScope();
  container
    .bind<MetricsService>(InversifyTypes.MetricsService)
    .toConstantValue(new BaseMetricsService(client.register));
  container
    .bind<UserDataStore>(InversifyTypes.UserDataStore)
    .to(UserDataStoreMock)
    .inSingletonScope();
  container
    .bind<SiteDataStore>(InversifyTypes.SiteDataStore)
    .to(SiteDataStoreMock)
    .inSingletonScope();
  container
    .bind<VehicleDataStore>(InversifyTypes.VehicleDataStore)
    .to(VehicleDataStoreMock)
    .inSingletonScope();
  container
    .bind<AccessDataStore>(InversifyTypes.AccessDataStore)
    .to(AccessDataStoreMock)
    .inSingletonScope();
  container
    .bind<SmartRecorderDataStore>(InversifyTypes.SmartRecorderDataStore)
    .to(SmartRecorderDataStoreMock)
    .inSingletonScope();
  container
    .bind<CompanyDataStore>(InversifyTypes.CompanyDataStore)
    .to(CompanyDataStoreMock)
    .inSingletonScope();
  container
    .bind<Auth0DataStore>(InversifyTypes.Auth0DataStore)
    .to(Auth0DataStoreMock)
    .inSingletonScope();
  container
    .bind<ElasticsearchClient>(InversifyTypes.ElasticsearchClient)
    .to(ElasticsearchClientMock)
    .inSingletonScope();
  container
    .bind<AccessTokenCache>(InversifyTypes.AccessTokenCache)
    .to(AccessTokenCacheMock)
    .inSingletonScope();
  container
    .bind<AuthorizationDataStoreSql>(InversifyTypes.AuthorizationDataStoreSql)
    .to(AuthorizationDataStoreSqlImplMock)
    .inSingletonScope();
  container
    .bind<CompanyDataStoreSql>(InversifyTypes.CompanyDataStoreSql)
    .to(CompanyDataStoreSqlImplMock)
    .inSingletonScope();
  container
    .bind<UserDataStoreSql>(InversifyTypes.UserDataStoreSql)
    .to(UserDataStoreSqlImplMock)
    .inSingletonScope();
  container
    .bind<AuthorizationExtractDataStore>(InversifyTypes.AuthorizationExtractDataStore)
    .to(AuthorizationExtractDataStoreMock)
    .inSingletonScope();
  container
    .bind<ExtractRefreshTimeCache>(InversifyTypes.ExtractRefreshTimeCache)
    .to(ExtractRefreshTimeCacheMock)
    .inSingletonScope();
  container.bind<TokenValidator>(InversifyTypes.TokenValidator).toConstantValue(new MockTokenValidator(config));
  container
    .bind<GQLSchemaFactory>(InversifyTypes.GQLSchemaFactory)
    .to(GQLSchemaFactory)
    .inSingletonScope();

  const loggingService: LoggingService = container.get(InversifyTypes.LoggingService);
  const metricsService: MetricsService = container.get(InversifyTypes.MetricsService);
  const gqlSchemaFactory: GQLSchemaFactory = container.get(InversifyTypes.GQLSchemaFactory);

  const expressService = new BaseGqlExpressMicroservice(config, { loggingService, metricsService });
  expressService.configure(gqlSchemaFactory.createGQLSchema(container));

  return expressService;
};
