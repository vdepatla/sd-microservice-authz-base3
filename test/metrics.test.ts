// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { MockConfig } from './mocks/Config.mock';
import { BaseGqlExpressMicroservice } from '@sd-microservice-base/express';
import { createExpressService } from './helpers/create-express-service';
import { logger } from '@sd-microservice-base/instrumentation';
import * as client from 'prom-client';
import * as request from 'supertest';

function getValueByName(name: string, values: any[]) {
  return (
    values.length > 0 &&
    values.reduce((acc, val) => {
      if (val.metricName === name) {
        acc = val;
      }
      return acc;
    })
  );
}

const getHistogramCount = (metricName: string, registry = client.register) => {
  return getValueByName(`${metricName}_count`, (registry.getSingleMetric(metricName) as any).get().values).value;
};

beforeAll(() => {
  logger.transports.console.silent = true;
});

describe('Metrics', () => {
  let expressService: BaseGqlExpressMicroservice;

  beforeEach(() => {
    client.register.clear();
    expressService = createExpressService(new MockConfig());
  });

  it('Should expose a /metrics endpoint', async () => {
    await request(expressService.expressApp)
      .get('/metrics')
      .expect(200);
  });

  describe('Request metrics', () => {
    beforeEach(() => {
      client.register.resetMetrics();
    });

    const makeGqlRequest = async () => {
      await request(expressService.expressApp)
        .post('/gql')
        .send({
          query: `
            query getAuthorizations {
                authorizations {
                    resource
                    role
                    permission
                    userScope
                    permissionScope
                }
            }
          `,
        });
    };

    it('Should track metrics around requests made to the API', async () => {
      expect(getHistogramCount('http_request_duration_seconds')).toBe(undefined);

      await makeGqlRequest();
      expect(getHistogramCount('http_request_duration_seconds')).toBe(1);

      await makeGqlRequest();
      expect(getHistogramCount('http_request_duration_seconds')).toBe(2);
    });
  });
});
