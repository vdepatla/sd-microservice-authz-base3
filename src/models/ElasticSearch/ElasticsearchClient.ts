import { Client } from '@elastic/elasticsearch';

export interface ElasticsearchClient {
  getClient(): Promise<Client>;
}
