/**
 * Application Membership details
 */
export interface ApplicationMembershipDataResponse {
  siteHash: string;
  applicationMembershipId: number;
  siteId: number;
  applicationId: number;
  isActive: boolean;
}
