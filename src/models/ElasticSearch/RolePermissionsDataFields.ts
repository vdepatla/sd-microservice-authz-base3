/**
 * Enum mapping local constants to elasticsearch fields for the 'role_permissions' ES mapping.
 */
export enum RolePermissionsDataFields {
  PermissionScope = "permissionScope",
  PrivelegeEntityKey = "privilegeEntityKey",
  PrivelegeEntityHash = "privilegeEntityHash",
  Company = "company",
  Site = "site",
  User = "user",
  RoleId = "roleId",
  Resource = "resource",
  Global = "global",
  AuthPermissionId = "auth_permissionId"
}
