/**
 * Enum mapping local constants to elasticsearch fields for the 'role_names' ES mapping.
 */
export enum RoleNamesDataFields {
  RoleId = "roleId",
  RoleName = "roleName"
}
