import { TimeoutError, ResponseError, ConnectionError } from '@elastic/elasticsearch/lib/errors';

export class ElasticsearchErrors {
  public static getErrorResponse(error: Error): Error {
    if (error instanceof ResponseError) {
      return {
        name: error.name,
        message: error.meta
          ? `message: ${error.meta.body.message}, statuscode: ${error.meta.statusCode}, warnings: ${error.meta.warnings}`
          : '',
        stack: error.stack,
      };
    }
    return error;
  }
}
