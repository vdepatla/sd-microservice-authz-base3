export enum ProcessExtractMetaDataFields {
  IndexName = 'doc.index_name',
  LastUpdatedTimestamp = 'doc.last_updated_timestamp',
}
