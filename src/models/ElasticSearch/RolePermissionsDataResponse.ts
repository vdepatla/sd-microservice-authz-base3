// Info about role permission data
export interface RolePermissionsDataResponse {
  resource: string;
  name: string;
  permission: string;
  roleId: Number;
  userScope: string;
  privilegeEntityHash: string;
  auth_permissionId: number;
  permissionScope: string;
  privilegeEntityId: number;
  auth_privilegeId: number;
}
