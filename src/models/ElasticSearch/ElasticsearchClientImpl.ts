import {
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  histogram,
  errorCounter,
  LoggingService,
} from '@sd-microservice-base/instrumentation';
import { injectable, inject } from 'inversify';
import { Client } from '@elastic/elasticsearch';
import { Auth0DataStore } from '../../dataStores/Auth0DataStore';
import { Config } from '../../Config';
import { AccessTokenCache } from '../../dataStores/AccessTokenCache';
import { ElasticsearchClient } from './ElasticsearchClient';
import { DataStore } from '../DataStore';
import { InversifyTypes } from '../InversifyTypes';
import { ApplicationLogs } from '../ApplicationLogs';

@injectable()
export class ElasticsearchClientImpl implements ElasticsearchClient {
  public static metricLabels = {
    [MetricLabels.DataStore]: DataStore.ElasticsearchClient,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Elasticsearch,
  };

  @inject(InversifyTypes.AccessTokenCache)
  private accessTokenCache: AccessTokenCache;

  @inject(InversifyTypes.Auth0DataStore)
  private auth0DataStore: Auth0DataStore;

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  private esClient: Client;
  private accessToken: string;

  @histogram(ElasticsearchClientImpl.metricLabels)
  @errorCounter(ElasticsearchClientImpl.metricLabels)
  public async getClient(): Promise<Client> {
    const functionName = 'ElasticsearchClientImpl.getClient()';

    const accessToken = await this.accessTokenCache.getAccessToken(this.config.Auth0ClientId, async () => {
      return await this.auth0DataStore.getM2MToken();
    });

    if (!this.esClient || accessToken !== this.accessToken) {
      this.esClient = new Client({
        node: this.config.ElasticServer,
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
        maxRetries: this.config.ElasticMaxRetries,
        requestTimeout: this.config.ElasticRequestTimeout,
      });

      // logEvent(logLevels.info, 'New elastic search client is created.', globalStrings.componentName, {
      //   functionName,
      //   oldAccessToken: this.accessToken ? this.accessToken : null,
      //   newAccessToken: accessToken,
      //   maxRetries: Config.ELASTIC_MAX_RETRIES,
      //   requestTimeout: Config.ELASTIC_REQUEST_TIMEOUT,
      //   elasticServer: Config.ELASTIC_SERVER,
      // });
      this.loggingService.log(ApplicationLogs.CreatedElasticsearchClient, {
        functionName,
        oldAccessToken: this.accessToken ? this.accessToken : null,
        newAccessToken: accessToken,
        maxRetries: this.config.ElasticMaxRetries,
        requestTimeout: this.config.ElasticRequestTimeout,
        elasticServer: this.config.ElasticServer,
      });

      this.accessToken = accessToken;
    }

    return this.esClient;
  }
}
