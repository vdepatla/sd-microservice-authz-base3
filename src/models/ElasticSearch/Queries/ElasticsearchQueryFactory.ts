import { PeopleLoginCredentialDataFields } from '../PeopleLoginCredentialDataFields';
import { RolePermissionDataFilterFactory } from '../Filters/RolePermissionDataFilterFactory';
import { PeopleLoginCredentialDataResponse } from '../PeopleLoginCredentialDataResponse';
import { SortOrder } from '../SortOrder';
import { GetLoginEnabledDataFields } from '../GetLoginEnabledDataFields';
import { ProcessExtractMetaDataFields } from '../ProcessExtractMetaDataFields';

export class ElasticsearchQueryFactory {
  public static getPeopleLoginCredentials(companyHash: string, username: string): object {
    // elastic search will have username and email-adress in lowercase, so convert it before using in the query
    const usernameLower = username.toLowerCase();

    return {
      size: 1,
      sort: {
        [PeopleLoginCredentialDataFields.CreatedDate]: SortOrder.Descending,
      },
      query: {
        bool: {
          must: [
            {
              term: {
                [`${PeopleLoginCredentialDataFields.CompanySeed}.keyword`]: companyHash,
              },
            },
            {
              term: { [PeopleLoginCredentialDataFields.PeopleIsActive]: true },
            },
            { term: { [PeopleLoginCredentialDataFields.IsLocked]: false } },
          ],
          should: [
            {
              term: {
                [`${PeopleLoginCredentialDataFields.Username}.keyword`]: usernameLower,
              },
            },
            {
              term: {
                [`${PeopleLoginCredentialDataFields.EmailAddress}.keyword`]: usernameLower,
              },
            },
          ],
          minimum_should_match: 1,
        },
      },
    };
  }

  public static getRolePermissions(
    resources: string[],
    roleIds: number[],
    peopleLoginCredential: PeopleLoginCredentialDataResponse
  ): object {
    return {
      bool: {
        filter: {
          bool: {
            must: [
              RolePermissionDataFilterFactory.resourceFilter(resources),
              RolePermissionDataFilterFactory.roleFilter(roleIds),
            ],
          },
        },
        should: [
          RolePermissionDataFilterFactory.globalPermissionFilter(),
          RolePermissionDataFilterFactory.sitePermissionFilter(peopleLoginCredential.siteHash),
          RolePermissionDataFilterFactory.companyPermissionFilter(peopleLoginCredential.companySeed),
          RolePermissionDataFilterFactory.userPermissionFilter(peopleLoginCredential.peopleHash),
        ],
        minimum_should_match: 1,
      },
    };
  }

  public static getLoginEnabledApplicationMembership(siteHash: string, applicationId: number): object {
    return {
      size: 0,
      query: {
        bool: {
          filter: {
            bool: {
              must: [
                {
                  term: {
                    [`${GetLoginEnabledDataFields.SiteHash}.keyword`]: siteHash,
                  },
                },
                {
                  term: {
                    [GetLoginEnabledDataFields.ApplicationId]: applicationId,
                  },
                },
                {
                  term: {
                    [GetLoginEnabledDataFields.IsActive]: true,
                  },
                },
              ],
            },
          },
        },
      },
    };
  }

  public static getProcessExtractRefreshTime(indices: string[]): object {
    return {
      size: indices.length,
      query: {
        terms: {
          [ProcessExtractMetaDataFields.IndexName]: indices,
        },
      },
    };
  }
}
