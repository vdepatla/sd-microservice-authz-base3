export enum GetLoginEnabledDataFields {
  SiteHash = "siteHash",
  ApplicationId = "applicationId",
  IsActive = "isActive"
}
