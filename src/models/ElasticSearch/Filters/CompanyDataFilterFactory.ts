import { CompanyMetaDataFields } from "../CompanyMetaDataFields";
export class CompanyDataFilterFactory {
  public static companyFilter(companyIds: Number[]): object {
    return {
      bool: {
        filter: {
          bool: {
            must: [
              {
                terms: {
                  [CompanyMetaDataFields.EntityIdentityKey]: companyIds
                }
              }
            ]
          }
        }
      }
    };
  }
}
