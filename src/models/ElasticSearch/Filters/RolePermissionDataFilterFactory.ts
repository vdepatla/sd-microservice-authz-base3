import { RolePermissionsDataFields } from "../RolePermissionsDataFields";

export class RolePermissionDataFilterFactory {
  public static roleFilter(roleIds: Number[]): object {
    return {
      terms: {
        [RolePermissionsDataFields.RoleId]: roleIds
      }
    };
  }

  public static resourceFilter(resources: string[]): object {
    if (resources && resources.length > 0) {
      return {
        terms: {
          [`${RolePermissionsDataFields.Resource}.keyword`]: resources
        }
      };
    }
  }

  public static globalPermissionFilter(): object {
    return {
      term: {
        [`${RolePermissionsDataFields.PermissionScope}.keyword`]: RolePermissionsDataFields.Global
      }
    };
  }

  public static sitePermissionFilter(siteHash: string): object {
    return {
      bool: {
        must: [
          {
            term: {
              [`${RolePermissionsDataFields.PermissionScope}.keyword`]: RolePermissionsDataFields.Site
            }
          },
          {
            term: {
              [`${RolePermissionsDataFields.PrivelegeEntityHash}.keyword`]: siteHash
            }
          }
        ]
      }
    };
  }

  public static companyPermissionFilter(companyHash: string): object {
    return {
      bool: {
        must: [
          {
            term: {
              [`${RolePermissionsDataFields.PermissionScope}.keyword`]: RolePermissionsDataFields.Company
            }
          },
          {
            term: {
              [`${RolePermissionsDataFields.PrivelegeEntityHash}.keyword`]: companyHash
            }
          }
        ]
      }
    };
  }

  public static userPermissionFilter(peopleHash: string): object {
    return {
      bool: {
        must: [
          {
            term: {
              [RolePermissionsDataFields.PermissionScope]:
                RolePermissionsDataFields.User
            }
          },
          {
            term: {
              [`${RolePermissionsDataFields.PrivelegeEntityHash}.keyword`]: peopleHash
            }
          }
        ]
      }
    };
  }
}
