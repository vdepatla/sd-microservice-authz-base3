export enum CompanyMetaDataFields {
  CompanyName = "CompanyName",
  EntityIdentityKey = "EntityIdentityKey"
}
