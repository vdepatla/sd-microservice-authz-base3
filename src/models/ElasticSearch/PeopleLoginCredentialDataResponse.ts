/**
 * Information about people login credential
 */
export interface PeopleLoginCredentialDataResponse {
  userName: string;
  companyHash: string;
  companySeed: string;
  companyId: number;
  peopleId: number;
  site_isActive: boolean;
  siteHash: string;
  isLocked: boolean;
  siteId: number;
  people_isActive: boolean;
  createdDate: Date;
  emailAddress: string;
  role_list: string;
  peopleHash: string;
  company_isActive: string;
}
