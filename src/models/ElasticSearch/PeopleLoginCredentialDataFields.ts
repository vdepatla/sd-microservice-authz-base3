/**
 * Enum mapping local constants to elasticsearch fields for the 'people_login_credential' ES mapping.
 */
export enum PeopleLoginCredentialDataFields {
  CreatedDate = "createdDate",
  CompanyHash = "companyHash",
  SeedHash = "seedHash",
  Username = "userName",
  PeopleIsActive = "people_isActive",
  IsLocked = "isLocked",
  EmailAddress = "emailAddress",
  CompanySeed = "companySeed"
}
