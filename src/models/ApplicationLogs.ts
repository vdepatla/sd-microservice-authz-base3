import { LogLevel } from '@sd-microservice-base/instrumentation';

/**
 * Customize the logs below for your microservice.
 * These logs are passed to the logging service in the inversify.config.ts file.
 */
export const ApplicationLogs = {
  ApplicationStarted: { level: LogLevel.Verbose, message: 'Microservice started: {{componentName}}' },
  ApplicationStartFailed: { level: LogLevel.Error, message: 'Microservice failed to start: {{componentName}}' },
  DataStoreGetStatusError: { level: LogLevel.Error, message: 'Error Processing Status: {{componentName}}' },
  DataStoreBackendRequestError: { level: LogLevel.Error, message: 'Error communicating with CoreOLTP' },

  /** DataStore logs */
  CouldNotGetCompanyAccess: { level: LogLevel.Error, message: 'An error occurred determining company access' },
  CouldNotGetSiteAccess: { level: LogLevel.Error, message: 'An error occurred determining site access' },
  CouldNotGetVehicleAccess: { level: LogLevel.Error, message: 'An error occurred determining vehicle access' },
  CouldNotGetSmartRecorderAccess: { level: LogLevel.Error, message: 'An error occurred determining SR access' },
  FoundValueInRedis: { level: LogLevel.Verbose, message: 'Found the value in redis for the given key' },
  CouldNotFindValueInRedis: { level: LogLevel.Verbose, message: 'Value not found in redis for the given key' },
  RedisReadError: { level: LogLevel.Error, message: 'Error occurred reading value from redis' },
  RedisWriteError: { level: LogLevel.Error, message: 'Error occurred writing value to redis' },
  EstablishingConnectionPoolError: {
    level: LogLevel.Error,
    message: 'The read-only connection pool could not be established. ',
  },
  CouldNotGetUserDataFromTokens: {
    level: LogLevel.Error,
    message: 'Could not get user data because the required information was not found in the tokens.',
  },
  CreatedMachineToMachine: {
    level: LogLevel.Info,
    message: 'Machine-to-machine token is created for service to service calls',
  },
  CouldNotCreateMachineToMachineToken: {
    level: LogLevel.Error,
    message: 'An error occurred in getting machine to machine token from auth0',
  },
  SqlFallBackTurnedOn: {
    level: LogLevel.Info,
    message: 'Configuration is turned on to fall back to SQL',
  },
  ExtractInformationIsNotLatest: {
    level: LogLevel.Info,
    message:
      'last refresh information not found or refresh time is greater than configured minutes. fall back to SQL will be attempted',
  },
  MakingElasticSearchCallToGetApplicationMembership: {
    level: LogLevel.Verbose,
    message: 'Initiating Elasticsearch call to get application membership details',
  },
  UserSiteNotEnabled: {
    level: LogLevel.Warn,
    message: 'User site is not login enabled',
  },
  CouldNotGetApplicationMembershipFromElasticsearch: {
    level: LogLevel.Error,
    message:
      'An error occurred querying elastic search for whether the login is enabled. Fallback to SQL will be attempted',
  },
  MakingElasticSearchCallToGetPermissions: {
    level: LogLevel.Verbose,
    message: 'Initiating Elasticsearch call to get permissions',
  },
  CouldNotGetPermissionsFromElasticsearch: {
    level: LogLevel.Error,
    message: 'An error occurred querying the elastic search for permissions. Fallback to SQL will be attempted',
  },
  CouldNotGetPermissionsFromSql: {
    level: LogLevel.Error,
    message: 'An error occurred querying the database for permissions',
  },
  CouldNotGetApplicationMembershipFromSql: {
    level: LogLevel.Error,
    message: 'An error occurred querying the database for whether the login is enabled',
  },
  MakingElasticSearchCallToGetExtractRefreshTime: {
    level: LogLevel.Verbose,
    message: 'Initiating Elasticsearch call to get application membership details',
  },
  CouldNotGetExtractRefreshTimeFromSql: {
    level: LogLevel.Error,
    message: 'An error occurred fetching process extract information from elastic search',
  },
  MakingElasticSearchCallToGetCompanyHash: {
    level: LogLevel.Verbose,
    message: 'Initiating Elasticsearch call for company hash lookup',
  },
  CouldNotGetCompanyHashFromElasticsearch: {
    level: LogLevel.Error,
    message: 'An error occurred fetching companies from elastic search. Fallback to SQL will be attempted',
  },
  CouldNotGetCompaniesFromSql: {
    level: LogLevel.Error,
    message: 'An error occurred fetching companies from database',
  },
  FoundRefreshTimeInRedis: {
    level: LogLevel.Verbose,
    message: 'Found refresh time in redis for given key',
  },
  NotFoundRefreshTimeInRedis: {
    level: LogLevel.Verbose,
    message: 'Refresh time not found in redis for given key',
  },
  CouldNotGetRefreshTimeFromRedis: {
    level: LogLevel.Error,
    message: 'Error occurred reading refresh times from redis',
  },
  WriteRefreshTimeInRedis: {
    level: LogLevel.Verbose,
    message: 'Writing refresh time for given key to redis',
  },
  CouldNotWriteRefreshTimeToRedis: {
    level: LogLevel.Error,
    message: 'Error occurred writing refresh times to redis',
  },
  SuccessfulRedisConnection: {
    level: LogLevel.Verbose,
    message: 'Connection to Redis cluster established successfully',
  },
  RedisConnectionError: {
    level: LogLevel.Error,
    message: 'Redis encountered an error',
  },
  RedisConnectionEnded: {
    level: LogLevel.Error,
    message: 'Connection to Redis cluster ended',
  },
  CouldNotDeleteCacheEntries: {
    level: LogLevel.Error,
    message: 'Could not delete cache entries',
  },
  CouldNotGetSiteChildren: {
    level: LogLevel.Error,
    message: 'Error when trying to retrieve site and site children from database',
  },
  CouldNotGetSites: {
    level: LogLevel.Error,
    message: 'Error when trying to retrieve sites from database',
  },
  CouldNotGetSmartRecorders: {
    level: LogLevel.Error,
    message: 'Error when trying to retrieve smart recorder info from database',
  },
  CouldNotGetVehicles: {
    level: LogLevel.Error,
    message: 'Error when trying to retrieve vehicle info from database',
  },
  CouldNotGetUserFromSql: {
    level: LogLevel.Error,
    message: 'An error occurred when trying to retrieve the User Data from the database',
  },
  MakingElasticSearchCallToGetPeopleLoginCredential: {
    level: LogLevel.Verbose,
    message: 'Initiating Elasticsearch call for get people login credentials',
  },
  PeopleLoginCredentialNotFound: {
    level: LogLevel.Warn,
    message: 'People login credential information not found for the user',
  },
  CouldNotGetPeopleLoginCredential: {
    level: LogLevel.Error,
    message: 'An error occurred in fetching people login credentials',
  },
  MakingElasticSearchCallToGetRoleNames: {
    level: LogLevel.Verbose,
    message: 'Initiating Elasticsearch call to get role names',
  },
  CouldNotGetRoleNames: {
    level: LogLevel.Error,
    message: 'An error occurred in fetching role names',
  },
  CouldNotGetUser: {
    level: LogLevel.Error,
    message: 'An error occurred fetching user data from elastic search. Fallback to SQL will be attempted',
  },
  CreatedElasticsearchClient: {
    level: LogLevel.Info,
    message: 'New elastic search client is created',
  },

  /** Service logs */
  CouldNotGetAccessData: {
    level: LogLevel.Error,
    message: 'Authorization get access error',
  },
  GetPermissionsStarted: {
    level: LogLevel.Verbose,
    message: 'Authorization get permissions started',
  },
  GotPermissionsSuccessfully: {
    level: LogLevel.Verbose,
    message: 'Successfully retrieved permissions',
  },
  CouldNotGetPermissions: {
    level: LogLevel.Error,
    message: 'An error occurred while fetching permissions',
  },
  GetLoginEnabledStarted: {
    level: LogLevel.Verbose,
    message: 'Authorization get login enabled started',
  },
  GotLoginEnabledSuccessfully: {
    level: LogLevel.Verbose,
    message: 'Successfully retrieved login enabled information',
  },
  CouldNotGetLoginEnabled: {
    level: LogLevel.Error,
    message: 'An error occurred while fetching login enabled information',
  },
};
