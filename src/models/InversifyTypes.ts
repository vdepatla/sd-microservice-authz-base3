/**
 * These symbols allow injection via inversify.
 * There should be one entry below for each injectable item in the microservice.
 */
export const InversifyTypes = {
  /**
   * Common
   */
  Config: Symbol('Config'),
  LoggingService: Symbol('LoggingService'),
  MetricsService: Symbol('MetricsService'),
  GQLSchemaFactory: Symbol('GQLSchemaFactory'),

  /**
   * Service specific
   */
  AccessDataStore: Symbol('AccessDataStore'),
  AuthorizationDataStore: Symbol('AuthorizationDataStore'),
  UserDataStore: Symbol('UserDataStore'),
  SiteDataStore: Symbol('SiteDataStore'),
  VehicleDataStore: Symbol('VehicleDataStore'),
  SmartRecorderDataStore: Symbol('SmartRecorderDataStore'),
  CompanyDataStore: Symbol('CompanyDataStore'),
  Auth0DataStore: Symbol('Auth0DataStore'),
  ElasticsearchClient: Symbol('ElasticsearchClient'),
  RedisDataStore: Symbol('RedisDataStore'),
  AccessTokenCache: Symbol('AccessTokenCache'),
  AuthorizationDataStoreSql: Symbol('AuthorizationDataStoreSql'),
  UserDataStoreSql: Symbol('UserDataStoreSql'),
  CompanyDataStoreSql: Symbol('CompanyDataStoreSql'),
  AuthorizationExtractDataStore: Symbol('AuthorizationExtractDataStore'),
  ExtractRefreshTimeCache: Symbol('ExtractRefreshTimeCache'),
  ConnectionPoolManager: Symbol('ConnectionPoolManager'),
  TokenValidator: Symbol('TokenValidator'),
  AuthorizationExtractDataUtil: Symbol('AuthorizationExtractDataUtil'),
};
