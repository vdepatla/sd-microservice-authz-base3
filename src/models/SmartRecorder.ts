export interface SmartRecorder {
  // SR ID
  srId: string;
  // SR SN
  srSn: string;
  // SR site ID
  siteId: string;
}
