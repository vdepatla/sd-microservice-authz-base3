export enum Timezone {
  Pacific = 'America/Los_Angeles',
  UTC = 'UTC',
}
