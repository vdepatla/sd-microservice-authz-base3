export interface Role {
  roleName: string;
  roleId: number;
  isActive: boolean;
  isDefault: string;
}
