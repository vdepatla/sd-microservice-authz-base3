export interface AccessResult {
  resourceType: string;
  resourceId: string;
  resourceName: string;
  hasAccess: boolean;
}
