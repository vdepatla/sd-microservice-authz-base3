export class User {
  // username
  public username: string;
  // user id
  public userId: string;
  // the user's company id
  public companyId: string;
  // the user's site id
  public siteId: string;
  // whether the user's site is active
  public siteIsActive: number;
  // user roles
  public roles: string[];

  public constructor(
    username: string,
    userId: string,
    companyId: string,
    siteId: string,
    siteIsActive: number
  ) {
    this.username = username;
    this.userId = userId;
    this.companyId = companyId;
    this.siteId = siteId;
    this.siteIsActive = siteIsActive;
    this.roles = [];
  }

  public isSuperUser(): boolean {
    // Current super user roles are "SDA", "SDCSM", "SDPM", and "SDTS"
    const superusers: string[] = ["SDA", "SDCSM", "SDPM", "SDTS"];
    const suRoles: string[] = this.roles.filter(
      role => superusers.indexOf(role) > -1
    );
    return suRoles.length > 0;
  }
}
