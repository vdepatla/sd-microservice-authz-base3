export interface AccessTokenResponse {
  accessToken: string;
  expiryTimeSeconds: number;
}
