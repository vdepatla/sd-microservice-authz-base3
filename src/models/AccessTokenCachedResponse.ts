export interface AccessTokenCachedResponse {
    accessToken: string;
    fromCache: boolean;
  }