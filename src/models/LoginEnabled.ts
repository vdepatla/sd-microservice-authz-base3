export interface LoginEnabled {
  // If the user is locked from login
  isLocked: boolean;
}
