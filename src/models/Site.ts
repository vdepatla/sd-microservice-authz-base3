export interface Site {
  // the user's site id
  siteId: string;
  // the name of the user's site
  siteName: string;
}
