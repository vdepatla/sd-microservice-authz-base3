export interface Vehicle {
  // the vehicle's id
  vehicleId: string;
  // the vehicle's SN
  vehicleSn: string;
  // the vehicle's VIN
  vehicleVin: string;
  // the vehicle's site id
  siteId: string;
}
