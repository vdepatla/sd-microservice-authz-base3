export interface Status {
  // service name
  name: string;
  // pod name
  pod: string;
  // current environment: dev, qa, ...
  env: string;
  // service version
  ver: string;
  // status code (1=OK)
  code: number;
  // status message (optional)
  msg: string;
  // service host address
  host: string;
  // current server time (ISO8601)
  ts: string;
  // service uptime (ISO8601)
  up: string;
}
