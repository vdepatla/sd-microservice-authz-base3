export interface ExtractRefreshTime {
  index: string;
  refreshTimeUnix: number;
}
