export interface AuthorizationResult {
  // the type of resource (such as service-console)
  resource: string;
  // a smartdrive role
  role: string;
  // a permission
  permission: string;
  // what the user is allowed to do with the permission scope (if permission scope is site, and their user scope is site-children,
  // they can access data relating to their site and any of its children)
  userScope: string;
  // what entity the permission is tied to (global, user, company, site)
  permissionScope: string;
}
