export interface Company {
  EntityIdentityKey: number;
  CompanyName: string;
}
