import { injectable, inject } from 'inversify';
import { AuthorizationDataStore } from '../dataStores/AuthorizationDataStore';
import { InversifyTypes } from '../models/InversifyTypes';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { AccessToken } from '@sd-microservice-base/security';

@injectable()
export class AuthorizationService {
  @inject(InversifyTypes.AuthorizationDataStore)
  private authorizationDataStore: AuthorizationDataStore;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  /**
   * getLoginEnabled returns login enabled information correponding to the
   * user and company from access token
   */
  public getLoginEnabled = async (accessToken: AccessToken) => {
    const functionName = 'AuthorizationService.getLoginEnabled';
    try {
      this.loggingService.log(ApplicationLogs.GetLoginEnabledStarted, {
        functionName,
        username: accessToken.username,
        companyHash: accessToken.companyHash,
      });

      const loginEnabled = await this.authorizationDataStore.getLoginEnabled(
        accessToken.companyHash,
        accessToken.username
      );

      this.loggingService.log(ApplicationLogs.GotLoginEnabledSuccessfully, {
        functionName,
        username: accessToken.username,
        companyHash: accessToken.companyHash,
      });

      return loginEnabled;
    } catch (err) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetLoginEnabled,
        {
          functionName,
          username: accessToken.username,
          companyHash: accessToken.companyHash,
        },
        err
      );
      throw err;
    }
  };

  /**
   * getLoginEnabled returns login enabled information correponding to the
   * user and company from access token
   */
  public getPermissions = async (accessToken: AccessToken, resources: string[]) => {
    const functionName = 'AuthorizationService.getPermissions';
    try {
      this.loggingService.log(ApplicationLogs.GetPermissionsStarted, {
        functionName,
        username: accessToken.username,
        companyHash: accessToken.companyHash,
        resources,
      });

      const authorizationResult = await this.authorizationDataStore.getPermissions(
        accessToken.companyHash,
        accessToken.username,
        resources
      );

      this.loggingService.log(ApplicationLogs.GotPermissionsSuccessfully, {
        functionName,
        username: accessToken.username,
        companyHash: accessToken.companyHash,
        resources,
      });

      return authorizationResult;
    } catch (err) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetPermissions,
        {
          functionName,
          username: accessToken.username,
          companyHash: accessToken.companyHash,
          resources,
        },
        err
      );
      throw err;
    }
  };
}
