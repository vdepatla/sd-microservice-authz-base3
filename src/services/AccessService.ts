import { injectable, inject } from 'inversify';
import { AccessDataStore } from '../dataStores/AccessDataStore';
import { UserDataStore } from '../dataStores/UserDataStore';
import { SiteDataStore } from '../dataStores/SiteDataStore';
import { AccessResult } from '../models/AccessResult';
import { Site } from '../models/Site';
import { InversifyTypes } from '../models/InversifyTypes';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { AccessToken, ForbiddenError } from '@sd-microservice-base/security';

@injectable()
export class AccessService {
  @inject(InversifyTypes.AccessDataStore)
  private accessDataStore: AccessDataStore;

  @inject(InversifyTypes.UserDataStore)
  private userDataStore: UserDataStore;

  @inject(InversifyTypes.SiteDataStore)
  private siteDataStore: SiteDataStore;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  /**
   * getAccess returns login enabled information corresponding to the requested companyIds,
   * siteIds, vehicleIds, srIds and srSns
   */
  public getAccess = async (
    accessToken: AccessToken,
    companyIds: string[],
    siteIds: string[],
    vehicleIds: string[],
    srIds: string[],
    srSns: string[]
  ): Promise<AccessResult[]> => {
    const functionName = 'AccessService.getAccess';
    try {
      const accessResults: AccessResult[] = [];
      const userInfo = await this.userDataStore.getUser(accessToken.companyHash, accessToken.username);
      if (!userInfo) {
        throw new ForbiddenError();
      }
      const userIsSuperUser = userInfo.isSuperUser();
      let childSites: Site[];

      // Company access
      if (Array.isArray(companyIds) && companyIds.length > 0) {
        const companyAccess = await this.accessDataStore.canUserAccessCompanies(companyIds, userInfo, userIsSuperUser);
        accessResults.push(...companyAccess);
      }

      // Site access
      if (Array.isArray(siteIds) && siteIds.length > 0) {
        if (childSites === undefined) {
          if (userIsSuperUser) {
            childSites = await this.siteDataStore.getSites(siteIds);
          } else {
            childSites = await this.siteDataStore.getSiteWithChildren(userInfo.companyId, userInfo.siteId);
          }
        }
        const siteAccess = await this.accessDataStore.canUserAccessSites(siteIds, childSites, userIsSuperUser);
        accessResults.push(...siteAccess);
      }

      // Vehicle access
      if (Array.isArray(vehicleIds) && vehicleIds.length > 0) {
        if (childSites === undefined) {
          childSites = await this.siteDataStore.getSiteWithChildren(userInfo.companyId, userInfo.siteId);
        }
        const vehicleAccess = await this.accessDataStore.canUserAccessVehicles(vehicleIds, childSites, userIsSuperUser);
        accessResults.push(...vehicleAccess);
      }

      // SR access
      if ((Array.isArray(srIds) && srIds.length > 0) || (Array.isArray(srSns) && srSns.length > 0)) {
        if (childSites === undefined) {
          childSites = await this.siteDataStore.getSiteWithChildren(userInfo.companyId, userInfo.siteId);
        }
        const srAccess = await this.accessDataStore.canUserAccessSmartRecorders(
          srIds,
          srSns,
          childSites,
          userIsSuperUser
        );
        accessResults.push(...srAccess);
      }

      return accessResults;
    } catch (error) {
      this.loggingService.log(ApplicationLogs.CouldNotGetAccessData, { functionName }, error);
      throw error;
    }
  };
}
