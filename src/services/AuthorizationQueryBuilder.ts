// tslint:disable-next-line:max-func-body-length
export const getPermissionsQuery = (resources: string[]): string => {
  let querySuffix: string = "";

  if (resources && resources.length > 0) {
    const resourcesForQuery =
      resources instanceof Array
        ? resources.map(x => `'${x}'`).join(",")
        : `'${resources}'`;
    querySuffix = ` AND ap.Resource in (${resourcesForQuery})`;
  }

  // First part of query gets global permissions. Second part (after UNION) returns back any company specific permissions.
  // tslint:disable-next-line:no-multiline-string
  return `-- get single person
    declare @peopleId INT = (
        SELECT TOP 1
            activePeople.PeopleId
        FROM (
                SELECT
                    p.PeopleId,
                    p.CreatedDate
                FROM People (NOLOCK) p
                JOIN Site (NOLOCK) s ON s.SiteId = p.SiteId
                JOIN LoginCredential (NOLOCK) lc ON lc.PeopleId = p.PeopleId
                JOIN seed sd (nolock) ON sd.CompanyID = s.CompanyId
                WHERE sd.CompanyHash = @companyHash
                AND lc.UserName = @username
                AND p.IsActive = 1
                AND lc.IsLocked = 0
                UNION ALL
                SELECT
                    p.PeopleId,
                    p.CreatedDate
                FROM People (NOLOCK) p
                JOIN Site (NOLOCK) s ON s.SiteId = p.SiteId
                JOIN LoginCredential (NOLOCK) lc ON lc.PeopleId = p.PeopleId
                JOIN Email (NOLOCK) e ON e.EmailId = p.EmailId
                JOIN seed sd (nolock) ON sd.CompanyID = s.CompanyId
                WHERE sd.CompanyHash = @companyHash
                AND e.EmailAddress = @username
                AND p.IsActive = 1
                AND lc.IsLocked = 0
        ) activePeople
        ORDER BY activePeople.CreatedDate DESC
    )
    --EXPLAIN
    ;with permissionsTemp as (
    select
        ap.resource,
        r.name as role,
        ap.permission,
        ap.userScope,
        ap.permissionScope,
        c.companyid,
        ap.auth_permissionid,
        s.siteId,
        s.siteName,
        p.peopleId
    from
        auth_permission ap (NOLOCK)
        join role r (NOLOCK) on r.RoleId = ap.RoleId
        join peopleroles pr (NOLOCK) on pr.RoleId = r.RoleId
        join people p (NOLOCK) on p.PeopleId = pr.PeopleId
        join site s (NOLOCK) on s.siteId = p.SiteId
        join company c (NOLOCK) on c.companyid = s.CompanyId
        join seed sd (NOLOCK) on sd.CompanyID = c.CompanyId
    where
        p.PeopleId = @peopleId
        and sd.CompanyHash = @companyHash
        ${querySuffix})
    -- get global permissions
    select
        up.resource,
        up.role,
        up.permission,
        up.userScope,
        up.permissionScope
    from
        permissionsTemp up
    where
        up.permissionscope = 'global'
    union all
    -- get company level permissions for user's company
    select distinct
        up.resource,
        up.role,
        up.permission,
        up.userScope,
        up.permissionScope
    from
        permissionsTemp up
        join auth_privilege apr (NOLOCK) on apr.auth_permissionid = up.auth_permissionid
    where
        up.permissionscope = 'company'
        and apr.PrivilegeEntityKey = up.companyId
    union all
    -- get site level permissions for user's site
    select distinct
        up.resource,
        up.role,
        up.permission,
        up.userScope,
        up.permissionScope
    from
        permissionsTemp up
        join auth_privilege apr (NOLOCK) on apr.auth_permissionid = up.auth_permissionid
    where
        up.permissionscope = 'site'
        and apr.PrivilegeEntityKey = up.siteId
    union all
    -- get user level permissions for user
    select distinct
        up.resource,
        up.role,
        up.permission,
        up.userScope,
        up.permissionScope
    from
        permissionsTemp up
        join auth_privilege apr (NOLOCK) on apr.auth_permissionid = up.auth_permissionid
    where
        up.permissionscope = 'user'
        and apr.PrivilegeEntityKey = up.PeopleId;
    `;
};

// tslint:disable-next-line:export-name
export const getLoginEnabledQuery = (): string => {
  // First part of query gets global permissions. Second part (after UNION) returns back any company specific permissions.
  // tslint:disable-next-line:no-multiline-string
  return `declare @peopleId INT = (
        SELECT TOP 1
            activePeople.PeopleId
        FROM (
                SELECT
                    p.PeopleId,
                    p.CreatedDate
                FROM People (NOLOCK) p
                JOIN Site (NOLOCK) s ON s.SiteId = p.SiteId
                JOIN LoginCredential (NOLOCK) lc ON lc.PeopleId = p.PeopleId
                JOIN seed sd (nolock) ON sd.CompanyID = s.CompanyId
                WHERE sd.CompanyHash = @companyHash
                AND lc.UserName = @username
                UNION ALL
                SELECT
                    p.PeopleId,
                    p.CreatedDate
                FROM People (NOLOCK) p
                JOIN Site (NOLOCK) s ON s.SiteId = p.SiteId
                JOIN LoginCredential (NOLOCK) lc ON lc.PeopleId = p.PeopleId
                JOIN Email (NOLOCK) e ON e.EmailId = p.EmailId
                JOIN seed sd (nolock) ON sd.CompanyID = s.CompanyId
                WHERE sd.CompanyHash = @companyHash
                AND e.EmailAddress = @username
        ) activePeople
        ORDER BY activePeople.CreatedDate DESC
    )

    SELECT DISTINCT
        lc.IsLocked
    FROM
        People p WITH (NOLOCK)
        JOIN Site s WITH (NOLOCK) ON p.SiteId = s.SiteId
        JOIN Company c WITH (NOLOCK) ON s.CompanyId = c.CompanyId
        LEFT JOIN CompanySetting cs WITH (NOLOCK) ON cs.CompanyId = c.CompanyId
        JOIN Seed sd WITH (NOLOCK) ON sd.CompanyId = c.CompanyId
        JOIN ApplicationMembership am WITH (NOLOCK) ON am.SiteId = S.SiteId
        LEFT JOIN LoginCredential lc WITH (NOLOCK) ON lc.PeopleId = p.PeopleId
        LEFT JOIN Email e WITH (NOLOCK) ON e.EmailId = p.EmailId
    WHERE
        p.PeopleId = @peopleId
        AND p.IsActive = 1
        AND s.IsActive = 1
        AND am.ApplicationID = 6 -- Enable User Login
        AND am.IsActive = 1
        AND c.IsActive = 1
        AND (sd.CompanyHash = @companyHash)`;
};
