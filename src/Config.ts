import { ApplicationLogs } from './models/ApplicationLogs';
import { MicroserviceConfig } from '@sd-microservice-base/configuration';
import { ExpressAppConfig } from '@sd-microservice-base/express';
import { RedisConfig } from '@sd-microservice-base/redis';
import { Log, LogLevel, getLogLevelFromString, InstrumentationConfig } from '@sd-microservice-base/instrumentation';
import { SecurityConfig, ResourceAccessRule } from '@sd-microservice-base/security';

// tslint:disable-next-line:no-unnecessary-class
export class Config
  implements MicroserviceConfig, RedisConfig, SecurityConfig, ExpressAppConfig, InstrumentationConfig {
  protected logLevel: LogLevel;

  protected toBoolean = (target: string, fallback = false) =>
    !target ? fallback : target.trim().toLowerCase() === 'true';

  /** Microservice Config */

  /**
   * The environment to run as (this will map to k8s namespace).
   */
  public get ServiceEnvironment(): string {
    return process.env.SERVICE_ENVIRONMENT;
  }

  /**
   * The name of the service.
   */
  public get ServiceName(): string {
    return process.env.SERVICE_NAME;
  }

  /**
   * The service version.
   */
  public get ServiceVersion(): string {
    return process.env.SERVICE_VERSION;
  }

  /** SQL Config */
  public get SQLUser(): string {
    return process.env.DB_USERNAME;
  }

  /** SQL Password */
  public get SQLPassword(): string {
    return process.env.DB_PASSWORD;
  }

  /** SQL Database */
  public get SQLDatabase(): string {
    return process.env.DB_NAME;
  }

  /** SQL User Domain */
  public get SQLUserDomain(): string {
    return process.env.DB_USER_DOMAIN;
  }

  /** SQL Server */
  public get SQLServer(): string {
    return process.env.DB_SERVER;
  }

  /** Redis Config */

  /**
   * The redis host address
   */
  public get RedisHost(): string {
    return process.env.REDIS_HOST;
  }

  /**
   * The redis port number
   */
  public get RedisPort(): number {
    return parseInt(process.env.REDIS_PORT, 10);
  }

  /**
   * The redis cache TTL in seconds
   */
  public get RedisCacheTTLSeconds(): number {
    return parseInt(process.env.REDIS_CACHE_TTL_SECONDS, 10);
  }

  /**
   * The redis namespace
   */
  public get RedisNamespace(): string {
    return process.env.REDIS_NAMESPACE;
  }

  /**
   * The max retries for redis
   */
  public get MaxRetries(): number {
    return parseInt(process.env.REDIS_MAX_RETRIES, 10);
  }

  /** Security Config */

  /**
   * Signing secret of the TAP API family
   */
  public get JwksUri(): string {
    return process.env.JWKS_URI;
  }

  /**
   * Native API Audience - uniquely identifies the API
   */
  public get AuthNAudience(): string {
    return process.env.AUTHN_AUDIENCE;
  }

  /**
   * Auth0 API Audience - uniquely identifies the API
   */
  public get Auth0Audience(): string {
    return process.env.AUTH0_AUDIENCE;
  }

  /**
   * Native Authentication issuer
   */
  public get AuthNIssuer(): string {
    return process.env.AUTHN_ISSUER;
  }

  /**
   * Auth0 Authentication issuer
   */
  public get Auth0Issuer(): string {
    return process.env.AUTH0_ISSUER;
  }

  /**
   * Native Authentication signing key
   */
  public get AuthNSigningKey(): string {
    return process.env.AUTHN_SIGNING_KEY;
  }

  /**
   * Auth0 Client Id
   */
  public get Auth0ClientId(): string {
    return process.env.AUTH0_CLIENT_ID;
  }

  /**
   * Auth0 Request Timeout
   */
  public get Auth0RequestTimeout(): number {
    return Number(process.env.AUTH0_REQUEST_TIMEOUT);
  }

  /**
   * Auth0 Client Secret
   */
  public get Auth0SigningKey(): string {
    return process.env.AUTH0_SIGNING_KEY;
  }

  /**
   * Native Authorization service GQL URL
   */
  public get AuthorizationServiceGqlUrl(): string {
    return process.env.AUTHORIZATION_SERVICE_GQL_URL;
  }

  /** Elastic Search Config */

  /**
   * configuration to control disable elastic search
   */
  public get UseSqlOnly(): boolean {
    return JSON.parse(process.env.USE_SQL_ONLY);
  }

  /**
   * duration minutes for elastic search delta refresh job run
   */
  public get ElasticDeltaRefreshDuartionMinutes(): number {
    return Number(process.env.ELASTIC_DELTA_REFRESH_DURATION_MINUTES);
  }

  /**
   * duration minutes for elastic search full refresh job run
   */
  public get ElasticFullRefreshDurationMinutes(): number {
    return Number(process.env.ELASTIC_FULL_REFRESH_DURATION_MINUTES);
  }

  /**
   *  process_extract_refresh elastic index name
   */
  public get ElasticIndexProcessExtractRefresh(): string {
    return process.env.ELASTIC_INDEX_PROCESS_EXTRACT_REFRESH;
  }

  /**
   * tolerance percentage for elastic search delta refresh job
   */
  public get ElasticDeltaRefreshTolerancePercentage(): number {
    return Number(process.env.ELASTIC_DELTA_REFRESH_TOLERANCE_PERCENTAGE);
  }

  /**
   * tolerance percentage for elastic search full refresh job
   */
  public get ElasticFullRefreshTolerancePercentage(): number {
    return Number(process.env.ELASTIC_FULL_REFRESH_TOLERANCE_PERCENTAGE);
  }

  /**
   *  redis cache ttl for extract timestamp
   */
  public get ExtractTimestampCacheTtlSeconds(): number {
    return Number(process.env.EXTRACT_TIMESTAMP_CACHE_TTL_SECONDS);
  }

  /**
   *  elastic search delta refresh indices
   */
  public get ElasticDeltaRefreshIndices(): string[] {
    return process.env.ELASTIC_DELTA_REFRESH_INDICES.split(',');
  }

  /**
   *  elastic search full refresh indices
   */
  public get ElasticFullRefreshIndices(): string[] {
    return process.env.ELASTIC_FULL_REFRESH_INDICES.split(',');
  }

  public get ElasticDeltaRefreshMinutesWithTolerance(): number {
    const tolerance = this.ElasticDeltaRefreshTolerancePercentage / 100;
    return this.ElasticFullRefreshDurationMinutes + tolerance;
  }

  public get ElasticFullRefreshMinutesWithTolerance(): number {
    const tolerance = this.ElasticFullRefreshTolerancePercentage / 100;
    return this.ElasticFullRefreshDurationMinutes + tolerance;
  }

  /**
   * Elasticsearch server address
   */
  public get ElasticServer(): any {
    return process.env.ELASTIC_SERVER;
  }

  /**
   * Elastic search people_login_credential index
   */
  public get ElasticIndexPeopleLoginCredential(): string {
    return process.env.ELASTIC_INDEX_PEOPLE_LOGIN_CREDENTIAL;
  }

  /**
   * Elastic search role_permission index
   */
  public get ElasticIndexRolePermissions(): string {
    return process.env.ELASTIC_INDEX_ROLE_PERMISSIONS;
  }

  /**
   * Elastic search application_membership index
   */
  public get ElasticIndexApplicationMembership(): string {
    return process.env.ELASTIC_INDEX_APPLICATION_MEMBERSHIP;
  }

  /**
   * Elastic search company_hash index
   */
  public get ElasticIndexCompanyHash(): string {
    return process.env.ELASTIC_INDEX_COMPANY_HASH;
  }

  /**
   * Query limit for role_permission index query
   */
  public get RolePermissionsQueryLimit(): string {
    return process.env.ROLE_PERMISSIONS_QUERY_LIMIT;
  }

  /**
   * Query limit for company index query
   */
  public get CompanyQueryLimit(): string {
    return process.env.COMPANY_QUERY_LIMIT;
  }

  /**
   * Elastic search role_names index
   */
  public get ElasticIndexRoleNames(): string {
    return process.env.ELASTIC_INDEX_ROLE_NAMES;
  }

  /**
   * Query Size for role names index query
   */
  public get RoleNamesQueryLimit(): string {
    return process.env.ROLE_NAMES_QUERY_LIMIT;
  }

  /**
   * Elastic search max retries
   */
  public get ElasticMaxRetries(): number {
    return Number(process.env.ELASTIC_MAX_RETRIES);
  }

  /**
   * Request timeout for elastic search connection
   */
  public get ElasticRequestTimeout(): number {
    return Number(process.env.ELASTIC_REQUEST_TIMEOUT);
  }

  /** Express App Config */

  /**
   * The port the service will listen on.
   */
  public get ServicePort(): number {
    return parseInt(process.env.SERVICE_PORT, 10);
  }

  /**
   * The GQL endpoint.
   */
  public get GqlEndpoint(): string {
    return process.env.GQL_ENDPOINT;
  }

  /**
   * Enables GQL tracing.
   */
  public get EnableGqlTracing(): boolean {
    return this.toBoolean(process.env.ENABLE_GQL_TRACING, false);
  }

  /**
   * A set of rules to limit access to the service based on role, resource, and permission.
   * This is the primary way to configure role-based access control using the Base express application.
   *
   * The service will throw an error on startup of no rules are provided unless `DisableRoleBasedAuthorization` is set to `true`.
   */
  public get ServiceAccessRules(): ResourceAccessRule[] {
    return [];
  }

  /**
   * Disables RBAC. This is useful in development environments.
   * Setting this to `true` will disable startup errors thrown when access rules are missing.
   */
  public get DisableRoleBasedAuthorization(): boolean {
    return this.toBoolean(process.env.DISABLE_ROLE_BASED_AUTHORIZATION, false);
  }

  /**
   * Disable authentication. This is useful in development environments.
   * Setting this to `true` will disable token-based authentication and allow open access to the API.
   */
  public get DisableAuthentication(): boolean {
    return this.toBoolean(process.env.DISABLE_AUTHENTICATION, false);
  }

  /**
   * Optional distinct cache TTL for Apollo caching. Falls back to redis cache TTL in the Base GQL express microservice if not supplied.
   */
  public get ApolloCacheTTLSeconds(): number {
    return parseInt(process.env.APOLLO_CACHE_TTL_SECONDS, 10);
  }

  /**
   * Set to true to disable Apollo caching via Redis.
   */
  public get DisableApolloCaching(): boolean {
    return this.toBoolean(process.env.DISABLE_APOLLO_CACHING, false);
  }

  /**
   * The object which contains application logs. This config item is used to power the /logs rest endpoint.
   */
  public get ApplicationLogs(): { [key: string]: Log } {
    return ApplicationLogs;
  }

  public get MockGqlApi(): boolean {
    return this.toBoolean(process.env.MOCK_GQL_API, false);
  }

  /**
   * The initial logging level to start up with.
   */
  public get LogLevel(): LogLevel {
    if (!this.logLevel) {
      this.logLevel = getLogLevelFromString(process.env.LOG_LEVEL || 'info');
    }

    return this.logLevel;
  }

  /**
   * Updates the current log level.
   */
  public updateLogLevel(newLevel: LogLevel) {
    this.logLevel = newLevel;
  }
}
