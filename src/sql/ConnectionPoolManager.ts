import { ApplicationLogs } from './../models/ApplicationLogs';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { Config } from './../Config';
import { InversifyTypes } from './../models/InversifyTypes';
import * as sql from 'mssql';
import { injectable, inject } from 'inversify';

@injectable()
export class ConnectionPoolManager {
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  private readOnlyConnectionPool: sql.ConnectionPool;
  private readOnlyConnectionPoolConnectionPromise: Promise<sql.ConnectionPool>;

  private get readOnlyConfig(): sql.config & { options: { readOnlyIntent: boolean } } {
    return {
      ...this.baseSqlConfig,
      options: {
        ...this.baseSqlConfig.options,
        ...{
          readOnlyIntent: true,
        },
      },
    };
  }

  private get baseSqlConfig(): sql.config {
    return {
      user: this.config.SQLUser,
      password: this.config.SQLPassword,
      server: this.config.SQLServer,
      database: this.config.SQLDatabase,
      domain: this.config.SQLUserDomain,
      options: {
        encrypt: false,
      },
    };
  }

  private async establishPools(): Promise<void> {
    this.readOnlyConnectionPool = new sql.ConnectionPool(this.readOnlyConfig);
    this.readOnlyConnectionPoolConnectionPromise = this.readOnlyConnectionPool.connect();
  }

  constructor(@inject(InversifyTypes.Config) config: Config) {
    this.config = config;

    this.establishPools();
  }

  public async poolsAreHealthy(): Promise<boolean> {
    if (this.readOnlyConnectionPool.connecting) {
      try {
        console.log('Waiting');
        await this.readOnlyConnectionPoolConnectionPromise;
      } catch {
        return false;
      }
    }

    return this.readOnlyConnectionPool.connected;
  }

  /**
   * If the connection pool is already connected, return it.
   * If the pool is in the process of connection, we can just await the existing promise.
   * Otherwise, call `connect` and return the promise.
   *
   * This method is meant to be idempotent. You should get the same response whether the
   * pool has connected already, is currently connection, or encountered an error connecting.
   */
  public async getReadOnlyConnectionPool(): Promise<sql.ConnectionPool> {
    if (this.readOnlyConnectionPool.connected) {
      return this.readOnlyConnectionPool;
    }

    try {
      return await this.readOnlyConnectionPoolConnectionPromise;
    } catch (error) {
      this.loggingService.log(ApplicationLogs.EstablishingConnectionPoolError, error);
      throw error;
    }
  }
}
