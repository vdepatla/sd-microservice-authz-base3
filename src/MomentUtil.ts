import moment = require('moment');

export class MomentUtil {
  public static durationMinFromCurrent(end: number): number {
    const current = moment().unix(); // moment().unix() gives UTC time
    return (current - end) / 60;
  }
}
