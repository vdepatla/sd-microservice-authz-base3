import 'reflect-metadata';
import { GQLSchemaFactory } from './gql/GQLSchemaFactory';
import { Config } from './Config';
import { ApplicationLogs } from './models/ApplicationLogs';
import { ContainerModule, interfaces } from 'inversify';
import {
  LoggingService,
  BaseLoggingService,
  BaseMetricsService,
  MetricsService,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from './models/InversifyTypes';
import { AccessDataStoreImpl } from './dataStores/AccessDataStoreImpl';
import { AuthorizationDataStoreImpl } from './dataStores/AuthorizationDataStoreImpl';
import { UserDataStoreImpl } from './dataStores/UserDataStoreImpl';
import { SiteDataStoreImpl } from './dataStores/SiteDataStoreImpl';
import { VehicleDataStoreImpl } from './dataStores/VehicleDataStoreImpl';
import { SmartRecorderDataStoreImpl } from './dataStores/SmartRecorderDataStoreImpl';
import { CompanyDataStoreImpl } from './dataStores/CompanyDataStoreImpl';
import { Auth0DataStoreImpl } from './dataStores/Auth0DataStoreImpl';
import { ElasticsearchClientImpl } from './models/ElasticSearch/ElasticsearchClientImpl';
import { AccessTokenCacheImpl } from './dataStores/AccessTokenCacheImpl';
import { AuthorizationDataStoreSqlImpl } from './dataStores/AuthorizationDataStoreSqlImpl';
import { UserDataStoreSqlImpl } from './dataStores/UserDataStoreSqlImpl';
import { AuthorizationExtractDataStoreImpl } from './dataStores/AuthorizationExtractDataStoreImpl';
import { ExtractRefreshTimeCacheImpl } from './dataStores/ExtractRefreshTimeCacheImpl';
import { AuthorizationDataStore } from './dataStores/AuthorizationDataStore';
import { AccessDataStore } from './dataStores/AccessDataStore';
import { UserDataStore } from './dataStores/UserDataStore';
import { SiteDataStore } from './dataStores/SiteDataStore';
import { VehicleDataStore } from './dataStores/VehicleDataStore';
import { SmartRecorderDataStore } from './dataStores/SmartRecorderDataStore';
import { CompanyDataStore } from './dataStores/CompanyDataStore';
import { Auth0DataStore } from './dataStores/Auth0DataStore';
import { ElasticsearchClient } from './models/ElasticSearch/ElasticsearchClient';
import { RedisDataStore, BaseRedisDataStore } from '@sd-microservice-base/redis';
import { AccessTokenCache } from './dataStores/AccessTokenCache';
import { AuthorizationDataStoreSql } from './dataStores/AuthorizationDataStoreSql';
import { UserDataStoreSql } from './dataStores/UserDataStoreSql';
import { AuthorizationExtractDataStore } from './dataStores/AuthorizationExtractDataStore';
import { ExtractRefreshTimeCache } from './dataStores/ExtractRefreshTimeCache';
import { ConnectionPoolManager } from './sql/ConnectionPoolManager';
import * as client from 'prom-client';
import { TokenValidator } from '@sd-microservice-base/security';
import { CompanyDataStoreSql } from './dataStores/CompanyDataStoreSql';
import { CompanyDataStoreSqlImpl } from './dataStores/CompanyDataStoreSqlImpl';

const config = new Config();

export const dependencyContainer = new ContainerModule((bind: interfaces.Bind) => {
  const metricsService = new BaseMetricsService(client.register);

  bind<Config>(InversifyTypes.Config).toConstantValue(config);
  bind<LoggingService>(InversifyTypes.LoggingService).toConstantValue(
    new BaseLoggingService(config, ApplicationLogs, [{ replace: 'componentName', with: config.ServiceName }])
  );
  bind<MetricsService>(InversifyTypes.MetricsService).toConstantValue(metricsService);
  bind<RedisDataStore>(InversifyTypes.RedisDataStore).toConstantValue(new BaseRedisDataStore(config, metricsService));
  bind<TokenValidator>(InversifyTypes.TokenValidator).toConstantValue(new TokenValidator(config));

  bind<GQLSchemaFactory>(InversifyTypes.GQLSchemaFactory)
    .to(GQLSchemaFactory)
    .inSingletonScope();
  bind<AccessDataStore>(InversifyTypes.AccessDataStore)
    .to(AccessDataStoreImpl)
    .inSingletonScope();
  bind<AccessTokenCache>(InversifyTypes.AccessTokenCache)
    .to(AccessTokenCacheImpl)
    .inSingletonScope();
  bind<AuthorizationDataStore>(InversifyTypes.AuthorizationDataStore)
    .to(AuthorizationDataStoreImpl)
    .inSingletonScope();
  bind<UserDataStore>(InversifyTypes.UserDataStore)
    .to(UserDataStoreImpl)
    .inSingletonScope();
  bind<SiteDataStore>(InversifyTypes.SiteDataStore)
    .to(SiteDataStoreImpl)
    .inSingletonScope();
  bind<VehicleDataStore>(InversifyTypes.VehicleDataStore)
    .to(VehicleDataStoreImpl)
    .inSingletonScope();
  bind<SmartRecorderDataStore>(InversifyTypes.SmartRecorderDataStore)
    .to(SmartRecorderDataStoreImpl)
    .inSingletonScope();
  bind<CompanyDataStore>(InversifyTypes.CompanyDataStore)
    .to(CompanyDataStoreImpl)
    .inSingletonScope();
  bind<Auth0DataStore>(InversifyTypes.Auth0DataStore)
    .to(Auth0DataStoreImpl)
    .inSingletonScope();
  bind<ElasticsearchClient>(InversifyTypes.ElasticsearchClient)
    .to(ElasticsearchClientImpl)
    .inSingletonScope();
  bind<AuthorizationDataStoreSql>(InversifyTypes.AuthorizationDataStoreSql)
    .to(AuthorizationDataStoreSqlImpl)
    .inSingletonScope();
  bind<UserDataStoreSql>(InversifyTypes.UserDataStoreSql)
    .to(UserDataStoreSqlImpl)
    .inSingletonScope();
  bind<CompanyDataStoreSql>(InversifyTypes.CompanyDataStoreSql)
    .to(CompanyDataStoreSqlImpl)
    .inSingletonScope();
  bind<AuthorizationExtractDataStore>(InversifyTypes.AuthorizationExtractDataStore)
    .to(AuthorizationExtractDataStoreImpl)
    .inSingletonScope();
  bind<ExtractRefreshTimeCache>(InversifyTypes.ExtractRefreshTimeCache)
    .to(ExtractRefreshTimeCacheImpl)
    .inSingletonScope();
  bind<ConnectionPoolManager>(InversifyTypes.ConnectionPoolManager)
    .to(ConnectionPoolManager)
    .inSingletonScope();
});
