// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { BaseGqlExpressMicroservice } from '@sd-microservice-base/express';
import { Config } from './Config';
// import the service's IoC container modules and required interfaces to be resolved
import { Container } from 'inversify';
import { dependencyContainer } from './inversify.config';
import { InversifyTypes } from './models/InversifyTypes';
import { GQLSchemaFactory } from './gql/GQLSchemaFactory';
import { ApplicationLogs } from './models/ApplicationLogs';
import { LoggingService, MetricsService } from '@sd-microservice-base/instrumentation';

const container = new Container();
container.load(dependencyContainer);

const config: Config = container.get(InversifyTypes.Config);
const loggingService: LoggingService = container.get(InversifyTypes.LoggingService);
const metricsService: MetricsService = container.get(InversifyTypes.MetricsService);
const gqlSchemaFactory: GQLSchemaFactory = container.get(InversifyTypes.GQLSchemaFactory);

// create Express server.
(async () => {
  try {
    const expressService = new BaseGqlExpressMicroservice(config, { loggingService, metricsService });
    expressService.configure(gqlSchemaFactory.createGQLSchema(container));
    const app = await expressService.start();
    loggingService.log(ApplicationLogs.ApplicationStarted, { address: app.address() });
  } catch (error) {
    loggingService.log(ApplicationLogs.ApplicationStartFailed, {}, error);
    process.exit(1);
  }
})();
