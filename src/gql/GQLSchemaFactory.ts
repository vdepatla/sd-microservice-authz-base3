import { buildFederatedSchema } from '@apollo/federation';
import { GraphQLSchema } from 'graphql';
import { injectable, inject, Container } from 'inversify';
import * as schema from './schema.graphql';
import { AccessToken, TokenValidator, UnauthorizedError } from '@sd-microservice-base/security';
import { InversifyTypes } from '../models/InversifyTypes';
import { LoggingService } from '@sd-microservice-base/instrumentation';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { AuthorizationDataStore } from '../dataStores/AuthorizationDataStore';
import { AccessService } from '../services/AccessService';
import { Request } from 'express';
import { AuthorizationService } from '../services/AuthorizationService';

@injectable()
export class GQLSchemaFactory {
  @inject(InversifyTypes.TokenValidator)
  private tokenValidator: TokenValidator;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @inject(InversifyTypes.AuthorizationDataStore)
  private authorizationDataStore: AuthorizationDataStore;

  createGQLSchema(container: Container) {
    const accessService = container.resolve(AccessService);
    const authorizationService = container.resolve(AuthorizationService);

    return buildFederatedSchema({
      typeDefs: schema,
      resolvers: {
        Query: {
          authorizations: async (_: any, { resources }: { resources: [string] }, { req }: { req: Request }) => {
            let accessToken: AccessToken;

            try {
              accessToken = this.tokenValidator.getAccessToken(req);
            } catch (error) {
              this.loggingService.log(ApplicationLogs.CouldNotGetUserDataFromTokens);
              throw new UnauthorizedError();
            }

            return await authorizationService.getPermissions(accessToken, resources);
          },

          loginEnabled: async (_: any, __: any, { req }: { req: Request }) => {
            let accessToken: AccessToken;

            try {
              accessToken = this.tokenValidator.getAccessToken(req);
            } catch (error) {
              this.loggingService.log(ApplicationLogs.CouldNotGetUserDataFromTokens);
              throw new UnauthorizedError();
            }

            return await authorizationService.getLoginEnabled(accessToken);
          },

          resourceAccess: async (
            _: any,
            {
              companyIds,
              siteIds,
              vehicleIds,
              srIds,
              srSns,
            }: { companyIds: [string]; siteIds: [string]; vehicleIds: [string]; srIds: [string]; srSns: [string] },
            { req }: { req: Request }
          ) => {
            let accessToken: AccessToken;

            try {
              accessToken = this.tokenValidator.getAccessToken(req);
            } catch (error) {
              this.loggingService.log(ApplicationLogs.CouldNotGetUserDataFromTokens);
              throw new UnauthorizedError();
            }

            return await accessService.getAccess(accessToken, companyIds, siteIds, vehicleIds, srIds, srSns);
          },
        },
      },
    }) as GraphQLSchema;
  }
}
