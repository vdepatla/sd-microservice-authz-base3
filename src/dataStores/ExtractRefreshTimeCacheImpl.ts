import 'reflect-metadata';
import { injectable, inject } from 'inversify';
import { DataStore } from '../models/DataStore';
import { ExtractRefreshTimeCache } from './ExtractRefreshTimeCache';
import { ExtractRefreshTime } from '../models/ExtractRefreshTime';
import {
  histogram,
  DALLabelValues,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  LoggingService,
} from '@sd-microservice-base/instrumentation';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { InversifyTypes } from '../models/InversifyTypes';
import { Config } from '../Config';
import { RedisDataStore } from '@sd-microservice-base/redis';

@injectable()
export class ExtractRefreshTimeCacheImpl implements ExtractRefreshTimeCache {
  public static readMetricLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.ExtractRefreshTimeCacheDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Redis,
  };

  public static writeMetricLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.ExtractRefreshTimeCacheDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Create,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Redis,
  };

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @inject(InversifyTypes.Config)
  private appConfig: Config;

  @inject(InversifyTypes.RedisDataStore)
  private redisDataStore: RedisDataStore;

  @histogram(ExtractRefreshTimeCacheImpl.readMetricLabels)
  public async getExtractRefreshTimes(keys: string[]): Promise<ExtractRefreshTime[]> {
    const functionName = 'ExtractRefreshTimeCacheImpl.getExtractRefreshTimes';

    try {
      const extractRefreshTimes: ExtractRefreshTime[] = [];
      for (const key of keys) {
        const cachedValue = await this.redisDataStore.client.get(key);
        if (cachedValue) {
          this.loggingService.log(ApplicationLogs.FoundRefreshTimeInRedis, { functionName, key });
          extractRefreshTimes.push({ index: key, refreshTimeUnix: Number(cachedValue) });
        } else {
          this.loggingService.log(ApplicationLogs.NotFoundRefreshTimeInRedis, { functionName, key });
        }
      }
      return extractRefreshTimes;
    } catch (readError) {
      this.loggingService.log(ApplicationLogs.CouldNotGetRefreshTimeFromRedis, { functionName, keys }, readError);
      return [];
    }
  }

  @histogram(ExtractRefreshTimeCacheImpl.writeMetricLabels)
  public async setExtractRefreshTimes(refreshTimes: ExtractRefreshTime[]): Promise<boolean> {
    const functionName = 'ExtractRefreshTimeCacheImpl.setExtractRefreshTimes';

    try {
      for (const refreshTime of refreshTimes) {
        this.loggingService.log(ApplicationLogs.WriteRefreshTimeInRedis, { functionName, key: refreshTime.index });
        await this.redisDataStore.client.set(
          refreshTime.index,
          refreshTime.refreshTimeUnix.toString(),
          'EX',
          this.appConfig.ExtractTimestampCacheTtlSeconds
        );
      }
      return true;
    } catch (writeError) {
      this.loggingService.log(
        ApplicationLogs.CouldNotWriteRefreshTimeToRedis,
        { functionName, refreshTimes },
        writeError
      );
      return false;
    }
  }
}
