import { ExtractRefreshTime } from '../models/ExtractRefreshTime';

export interface ExtractRefreshTimeCache {
  getExtractRefreshTimes(keys: string[]): Promise<ExtractRefreshTime[]>;
  setExtractRefreshTimes(refreshTimes: ExtractRefreshTime[]): Promise<boolean>;
}
