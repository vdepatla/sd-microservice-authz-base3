import { SmartRecorder } from "../models/SmartRecorder";

/**
 * Interface for the smart recorder data store.
 */
export interface SmartRecorderDataStore {
  getSmartRecorders(srIds: string[], srSns: string[]): Promise<SmartRecorder[]>;
}
