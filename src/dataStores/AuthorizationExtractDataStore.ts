import { ExtractRefreshTime } from '../models/ExtractRefreshTime';

export interface AuthorizationExtractDataStore {
  getLastRefreshTime(indices: string[]): Promise<ExtractRefreshTime[]>;
}
