import { Site } from "../models/Site";

/**
 * Interface for the Site data store.
 */
export interface SiteDataStore {
  getSiteWithChildren(companyId: string, siteId: string): Promise<Site[]>;
  getSites(siteIds: string[]): Promise<Site[]>
}
