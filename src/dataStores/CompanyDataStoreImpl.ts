import 'reflect-metadata';
import { CompanyDataStore } from './CompanyDataStore';
import { injectable, inject } from 'inversify';
import { Config } from './../Config';
import { Company } from '../models/Company';
import { SortOrder } from '../models/ElasticSearch/SortOrder';
import { CompanyDataFilterFactory } from '../models/ElasticSearch/Filters/CompanyDataFilterFactory';
import { CompanyMetaDataFields } from '../models/ElasticSearch/CompanyMetaDataFields';
import { RequestParams, ApiResponse } from '@elastic/elasticsearch';
import { SearchResponse } from '../models/ElasticSearch/SearchResponse';
import { ElasticsearchClient } from '../models/ElasticSearch/ElasticsearchClient';
import { AuthorizationExtractDataStore } from './AuthorizationExtractDataStore';
import { DataStore } from '../models/DataStore';
import { CompanyDataStoreSql } from './CompanyDataStoreSql';
import { ExtractRefreshTimeCache } from './ExtractRefreshTimeCache';
import { AuthorizationExtractDataUtil } from './AuthorizationExtractDataUtil';
import {
  errorCounter,
  histogram,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  LoggingService,
  LogLevel,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';

@injectable()
export class CompanyDataStoreImpl implements CompanyDataStore {
  public static metricLabels = {
    [MetricLabels.DataStore]: DataStore.CompanyDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Elasticsearch,
  };

  @inject(InversifyTypes.CompanyDataStoreSql)
  private companyDataStoreSql: CompanyDataStoreSql;

  @inject(InversifyTypes.AuthorizationExtractDataStore)
  private authorizationExtractDataStore: AuthorizationExtractDataStore;

  @inject(InversifyTypes.ExtractRefreshTimeCache)
  private extractRefreshTimeCache: ExtractRefreshTimeCache;

  @inject(InversifyTypes.ElasticsearchClient)
  private elasticsearchClient: ElasticsearchClient;

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @histogram(CompanyDataStoreImpl.metricLabels)
  @errorCounter(CompanyDataStoreImpl.metricLabels)
  public async getCompanies(companyIds: string[]): Promise<Company[]> {
    const functionName = 'CompanyDataStoreImpl.getCompanyName';

    if (this.config.UseSqlOnly) {
      this.loggingService.log(ApplicationLogs.SqlFallBackTurnedOn, { functionName });
      return this.companyDataStoreSql.getCompanies(companyIds);
    }

    const body = {
      size: this.config.CompanyQueryLimit,
      query: CompanyDataFilterFactory.companyFilter(companyIds.map(Number)),
    };

    let search: RequestParams.Search = {
      body,
      index: this.config.ElasticIndexCompanyHash,
    };

    try {
      const indices = [this.config.ElasticIndexCompanyHash];

      // get index refresh times from cache ..
      const cachedRefreshTimes = await this.extractRefreshTimeCache.getExtractRefreshTimes(indices);

      if (!AuthorizationExtractDataUtil.isExtractDataLatest(indices, cachedRefreshTimes, this.config)) {
        const processExtractResponse = await this.authorizationExtractDataStore.getLastRefreshTime(indices);

        // set refresh times in cache ..
        await this.extractRefreshTimeCache.setExtractRefreshTimes(processExtractResponse);

        // last refresh is greater than configured minutes, fallback to sql
        if (!AuthorizationExtractDataUtil.isExtractDataLatest(indices, processExtractResponse, this.config)) {
          this.loggingService.log(ApplicationLogs.ExtractInformationIsNotLatest, {
            functionName,
            processExtractResponse,
            fullRefreshTolerance: this.config.ElasticFullRefreshMinutesWithTolerance,
          });
          return this.companyDataStoreSql.getCompanies(companyIds);
        }
      }

      const esClient = await this.elasticsearchClient.getClient();

      this.loggingService.log(ApplicationLogs.MakingElasticSearchCallToGetCompanyHash, {
        functionName,
        companyIds,
        search,
      });

      let result: ApiResponse<SearchResponse<Company>> = await esClient.search(search);
      const response = result.body;

      const allCompanies = [];
      while (response && response.hits && response.hits.hits.length) {
        allCompanies.push(...response.hits.hits);

        // logEvent(
        //   logLevels.verbose,
        //   `Received ${allCompanies.length} of ${response.hits.total} companies.`,
        //   globalStrings.componentName,
        //   { functionName, companyIds, search }
        // );

        this.loggingService.log(
          {
            level: LogLevel.Verbose,
            message: `Received ${allCompanies.length} of ${response.hits.total} companies.`,
          },
          {
            functionName,
            companyIds,
            search,
          }
        );

        if (response.hits.total === allCompanies.length) {
          // received all permissions no paging
          break;
        }

        const body = {
          size: this.config.CompanyQueryLimit,
          query: CompanyDataFilterFactory.companyFilter(companyIds.map(Number)),
          search_after: response.hits.hits[response.hits.hits.length - 1].sort,
          sort: {
            [CompanyMetaDataFields.EntityIdentityKey]: SortOrder.Ascending,
          },
        };

        search = {
          body,
          index: this.config.ElasticIndexCompanyHash,
        };

        result = await esClient.search(search);
      }

      return allCompanies.map(rs => {
        return {
          CompanyName: rs._source.CompanyName,
          EntityIdentityKey: rs._source.EntityIdentityKey,
        };
      });
    } catch (error) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetCompanyHashFromElasticsearch,
        {
          functionName,
          companyIds,
          search,
          maxRetries: this.config.ElasticMaxRetries,
        },
        error
      );

      return this.companyDataStoreSql.getCompanies(companyIds);
    }
  }
}
