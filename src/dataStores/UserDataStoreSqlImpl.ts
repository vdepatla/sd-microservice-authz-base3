import 'reflect-metadata';
import * as sql from 'mssql';
import { User } from '../models/User';
import { injectable, inject } from 'inversify';
import { UserDataStoreSql } from './UserDataStoreSql';
import { Config } from '../Config';
import {
  errorCounter,
  histogram,
  logExecution,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  DALLabelValues,
  LoggingService,
  LogLevel,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { ConnectionPoolManager } from '../sql/ConnectionPoolManager';

/**
 * Used to retrieve user information in order to access the Vehicle Management Service.
 */
@injectable()
export class UserDataStoreSqlImpl implements UserDataStoreSql {
  private static metricsLabels: DALLabelValues = {
    [MetricLabels.DataStore]: 'UserDataStoreSqlImpl',
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.CoreOLTP,
  };

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @inject(InversifyTypes.ConnectionPoolManager)
  private connectionPoolManager: ConnectionPoolManager;

  /**
   * Retrieves company and site information for a given username.
   * @param username The username of the user to retrieve information for.
   * @returns User data containing Site and Company information.
   */
  @histogram(UserDataStoreSqlImpl.metricsLabels)
  @errorCounter(UserDataStoreSqlImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async getUser(companyHash: string, username: string): Promise<User> {
    const functionName = 'UserDataStoreSqlImpl.getUser';

    try {
      const pool = await this.connectionPoolManager.getReadOnlyConnectionPool();
      const req = new sql.Request(pool);
      // tslint:disable-next-line:no-multiline-string
      const query = `SELECT TOP 1
                activePeople.PeopleId,
                activePeople.CompanyId,
                activePeople.SiteId,
                activePeople.SiteName,
                activePeople.IsActive
            FROM (
                SELECT
                    p.PeopleId,
                    s.CompanyId,
                    s.SiteId,
                    s.SiteName,
                    s.IsActive,
                    p.CreatedDate
                FROM People (NOLOCK) p
                JOIN Site (NOLOCK) s ON s.SiteId = p.SiteId
                JOIN LoginCredential (NOLOCK) lc ON lc.PeopleId = p.PeopleId
                JOIN Seed sd (NOLOCK) ON sd.CompanyId = s.CompanyId
                WHERE sd.CompanyHash = @companyHash
                AND lc.UserName = @username
                AND p.IsActive = 1
                AND lc.IsLocked = 0
                UNION ALL
                SELECT
                    p.PeopleId,
                    s.CompanyId,
                    s.SiteId,
                    s.SiteName,
                    s.IsActive,
                    p.CreatedDate
                FROM People (NOLOCK) p
                JOIN Site (NOLOCK) s ON s.SiteId = p.SiteId
                JOIN Email (NOLOCK) e ON e.EmailId = p.EmailId
                JOIN LoginCredential (NOLOCK) lc ON lc.PeopleId = p.PeopleId
                JOIN Seed sd (NOLOCK) ON sd.CompanyId = s.CompanyId
                WHERE sd.CompanyHash = @companyHash
                AND e.EmailAddress = @username
                AND p.IsActive = 1
                AND lc.IsLocked = 0
            ) activePeople
            ORDER BY activePeople.CreatedDate DESC`;

      req.input('companyHash', companyHash);
      req.input('username', username);
      const result = await req.query(query);
      const userData: User[] = result.recordset.map(rs => {
        return new User(username, rs.PeopleId, rs.CompanyId.toString(), rs.SiteId.toString(), rs.IsActive);
      });
      if (userData.length > 0) {
        const userDataWithRoles = await this.gatherUserRoles(userData[0], pool);
        return userDataWithRoles;
      }
      return undefined;
    } catch (error) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetUserFromSql,
        {
          functionName,
          username,
          db: {
            host: this.config.SQLServer,
            name: this.config.SQLDatabase,
          },
        },
        error
      );
      throw error;
    }
  }

  @histogram(UserDataStoreSqlImpl.metricsLabels)
  @errorCounter(UserDataStoreSqlImpl.metricsLabels)
  @logExecution()
  private async gatherUserRoles(user: User, pool: sql.ConnectionPool): Promise<User> {
    const req = new sql.Request(pool);
    // tslint:disable-next-line:no-multiline-string
    const query = `SELECT
            r.Name
        FROM PeopleRoles (NOLOCK) pr
        JOIN Role (NOLOCK) r ON pr.roleid = r.roleid
        WHERE pr.peopleid = @userId`;

    req.input('userId', user.userId);
    const result = await req.query(query);
    user.roles.push(...result.recordset.map(rs => rs.Name));

    return user;
  }
}
