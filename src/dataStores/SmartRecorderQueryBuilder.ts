export namespace SmartRecorderQueryBuilder {
  export const buildQuery = (srIds: string[], srSns: string[]): string => {
    let whereClause: string;
    if (srIds !== undefined && srIds.length > 0) {
      // Because we need to add each value individually, we're going to
      // create an object that looks like
      // {@srIds0: srIds[0], @srIds1: srIds[1], ...}
      const keyValueSrIds = srIds.reduce<{ [key: string]: string }>(
        (obj, item, idx) => {
          obj[`@srIds${idx}`] = item;
          return obj;
        },
        {}
      );
      const keyString = Object.keys(keyValueSrIds).join(",");
      whereClause = ` WHERE c.CameraId IN (${keyString})`;
    }
    if (srSns !== undefined && srSns.length > 0) {
      // Because we need to add each value individually, we're going to
      // create an object that looks like
      // {@srSns0: srSns[0], @srSns1: srSns[1], ...}
      const keyValueSrSns = srSns.reduce<{ [key: string]: string }>(
        (obj, item, idx) => {
          obj[`@srSns${idx}`] = item;
          return obj;
        },
        {}
      );
      const keyString = Object.keys(keyValueSrSns).join(",");
      if (whereClause === undefined) {
        whereClause = ` WHERE c.CameraSN IN (${keyString})`;
      } else {
        whereClause += ` OR c.CameraSN IN (${keyString})`;
      }
    }
    return `SELECT CameraId, CameraSN, SiteId FROM dbo.Camera c (NOLOCK)${whereClause}`;
  };
}
