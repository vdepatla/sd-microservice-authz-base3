import 'reflect-metadata';
import * as sql from 'mssql';
import { Site } from '../models/Site';
import { injectable, inject } from 'inversify';
import { SiteDataStore } from './SiteDataStore';
import { Config } from '../Config';
import {
  errorCounter,
  histogram,
  logExecution,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  DALLabelValues,
  LogLevel,
  LoggingService,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { ConnectionPoolManager } from '../sql/ConnectionPoolManager';

/**
 * Used to retrieve user information in order to access the Vehicle Management Service.
 */
@injectable()
export class SiteDataStoreImpl implements SiteDataStore {
  public static metricsLabels: DALLabelValues = {
    [MetricLabels.DataStore]: 'SiteDataStore',
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.CoreOLTP,
  };

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @inject(InversifyTypes.ConnectionPoolManager)
  private connectionPoolManager: ConnectionPoolManager;

  /**
   * Retrieves site hierarchy for a given company id and site id
   * @param companyId The company id of the given site.
   * @param siteId The id of the site to retrieve hierarchy for.
   * @returns Array of SiteDataStoreResult for each site in hierarchy
   */
  @histogram(SiteDataStoreImpl.metricsLabels)
  @errorCounter(SiteDataStoreImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async getSiteWithChildren(companyId: string, siteId: string): Promise<Site[]> {
    const functionName = 'SiteDataStoreImpl.getSiteWithChildren';

    try {
      const conn = await this.connectionPoolManager.getReadOnlyConnectionPool();
      const req = new sql.Request(conn);

      // tslint:disable-next-line:no-multiline-string
      const query = `DECLARE @SiteId BIGINT = @siteIdInput
            DECLARE @CompanyID INT = @companyIdInput

            BEGIN
                -- get users site heirarchy (separate recordset)
                ;WITH SiteHierarchy(SiteId, CompanyId, SiteName, IsActive) AS (
                SELECT
                    s.SiteId,
                    s.CompanyId,
                    s.SiteName,
                    s.IsActive
                FROM
                    dbo.[Site] s (NOLOCK)
                WHERE
                    s.SiteId = @SiteId
                UNION ALL
                SELECT
                    s2.SiteId,
                    s2.CompanyId,
                    s2.SiteName,
                    s2.IsActive
                FROM
                    dbo.[Site] s2 WITH (NOLOCK)
                    JOIN SiteHierarchy p ON s2.ParentId	= p.SiteId and s2.CompanyId = @CompanyID
                )

                SELECT DISTINCT
                    SiteId,
                    CompanyId,
                    SiteName
                FROM SiteHierarchy s
                WHERE s.IsActive = 1
                ORDER BY SiteName
            END`;

      req.input('siteIdInput', siteId);
      req.input('companyIdInput', companyId);
      const result = await req.query(query);
      const sites = result.recordset.map(rs => {
        return {
          siteId: rs.SiteId.toString(),
          siteName: rs.SiteName,
        };
      });
      return sites.length !== 0 ? sites : [];
    } catch (err) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetSiteChildren,
        {
          functionName,
          db: {
            host: this.config.SQLServer,
            name: this.config.SQLDatabase,
          },
        },
        err
      );
      throw err;
    }
  }

  /**
   * Retrieves site hierarchy for a given company id and site id
   * @param companyId The company id of the given site.
   * @param siteId The id of the site to retrieve hierarchy for.
   * @returns Array of SiteDataStoreResult for each site in hierarchy
   */
  @histogram(SiteDataStoreImpl.metricsLabels)
  @errorCounter(SiteDataStoreImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async getSites(siteIds: string[]): Promise<Site[]> {
    const functionName = 'SiteDataStoreImpl.getSites';

    try {
      const conn = await this.connectionPoolManager.getReadOnlyConnectionPool();
      const req = new sql.Request(conn);

      // Because we need to add each value individually, we're going to
      // create an object that looks like
      // {@siteIds: siteIds[0], @siteIds1: siteIds[1], ...}
      const keyValueSiteIds = siteIds.reduce<{ [key: string]: string }>((obj, item, idx) => {
        obj[`@siteIds${idx}`] = item;
        return obj;
      }, {});

      const keyString = Object.keys(keyValueSiteIds).join(',');

      // tslint:disable-next-line:no-multiline-string
      const query = `SELECT
                    s.SiteId,
                    s.SiteName
                FROM
                    dbo.[Site] s (NOLOCK)
                WHERE
                    s.SiteId IN (${keyString})
      `;

      // Now let's add each value as input
      Object.keys(keyValueSiteIds).forEach(key => {
        // The slice is to strip the '@' since that goofs things up
        req.input(key.slice(1), keyValueSiteIds[key]);
      });

      const result = await req.query(query);

      const sites = result.recordset.map(rs => {
        return {
          siteId: rs.SiteId.toString(),
          siteName: rs.SiteName,
        };
      });

      return sites.length !== 0 ? sites : [];
    } catch (err) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetSites,
        {
          functionName,
          db: {
            host: this.config.SQLServer,
            name: this.config.SQLDatabase,
          },
        },
        err
      );

      throw err;
    }
  }
}
