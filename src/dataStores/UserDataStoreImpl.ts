import 'reflect-metadata';
import { User } from '../models/User';
import { injectable, inject } from 'inversify';
import { UserDataStore } from './UserDataStore';
import { Config } from '../Config';
import { PeopleLoginCredentialDataResponse } from '../models/ElasticSearch/PeopleLoginCredentialDataResponse';
import { ElasticsearchQueryFactory } from '../models/ElasticSearch/Queries/ElasticsearchQueryFactory';
import { SortOrder } from '../models/ElasticSearch/SortOrder';
import { RoleNamesDataFields } from '../models/ElasticSearch/RoleNamesDataFields';
import { Role } from '../models/Role';
import { ApiResponse, RequestParams } from '@elastic/elasticsearch';
import { SearchResponse } from '../models/ElasticSearch/SearchResponse';
import { ElasticsearchClient } from '../models/ElasticSearch/ElasticsearchClient';
import { AuthorizationExtractDataStore } from './AuthorizationExtractDataStore';
import { DataStore } from '../models/DataStore';
import { UserDataStoreSql } from './UserDataStoreSql';
import { ExtractRefreshTimeCache } from './ExtractRefreshTimeCache';
import { AuthorizationExtractDataUtil } from './AuthorizationExtractDataUtil';
import {
  errorCounter,
  histogram,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  DALLabelValues,
  logExecution,
  LoggingService,
  logLevels,
  LogLevel,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';

/**
 * Used to retrieve user information in order to access the Vehicle Management Service.
 */
@injectable()
export class UserDataStoreImpl implements UserDataStore {
  public static metricLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.UserDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Elasticsearch,
  };

  @inject(InversifyTypes.ElasticsearchClient)
  private elasticsearchClient: ElasticsearchClient;

  @inject(InversifyTypes.UserDataStoreSql)
  private userDataStoreSql: UserDataStoreSql;

  @inject(InversifyTypes.AuthorizationExtractDataStore)
  private authorizationExtractDataStore: AuthorizationExtractDataStore;

  @inject(InversifyTypes.ExtractRefreshTimeCache)
  private extractRefreshTimeCache: ExtractRefreshTimeCache;

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @errorCounter(UserDataStoreImpl.metricLabels)
  @histogram(UserDataStoreImpl.metricLabels)
  public async getPeopleLoginCredential(
    companyHash: string,
    username: string
  ): Promise<PeopleLoginCredentialDataResponse | undefined> {
    const functionName = 'UserDataStoreImpl.getPeopleLoginCredential';

    const body = ElasticsearchQueryFactory.getPeopleLoginCredentials(companyHash, username);

    const search: RequestParams.Search = {
      body,
      index: this.config.ElasticIndexPeopleLoginCredential,
    };

    try {
      const esClient = await this.elasticsearchClient.getClient();

      this.loggingService.log(ApplicationLogs.MakingElasticSearchCallToGetPeopleLoginCredential, {
        functionName,
        companyHash,
        username,
      });

      const result: ApiResponse<SearchResponse<PeopleLoginCredentialDataResponse>> = await esClient.search(search);
      const response = result.body;

      if (response.hits.total === 0) {
        this.loggingService.log(ApplicationLogs.PeopleLoginCredentialNotFound, {
          functionName,
          companyHash,
          username,
        });
        return undefined;
      }

      return response.hits.hits[0]._source;
    } catch (error) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetPeopleLoginCredential,
        {
          functionName,
          companyHash,
          username,
          search,
          maxRetries: this.config.ElasticMaxRetries,
        },
        error
      );
      throw error;
    }
  }
  /**
   * Retrieves company and site information for a given username.
   * @param username The username of the user to retrieve information for.
   * @returns User data containing Site and Company information.
   */
  public async getUser(companyHash: string, username: string): Promise<User> {
    try {
      return await this.getUserElastic(companyHash, username);
    } catch (error) {
      return this.userDataStoreSql.getUser(companyHash, username);
    }
  }

  @errorCounter(UserDataStoreImpl.metricLabels)
  @histogram(UserDataStoreImpl.metricLabels)
  public async gatherRoles(roleIdList: number[]): Promise<string[]> {
    const functionName = 'UserDataStoreImpl.gatherRoles';
    let search: RequestParams.Search;
    try {
      const esClient = await this.elasticsearchClient.getClient();

      const body = {
        size: this.config.RoleNamesQueryLimit,
        _source: RoleNamesDataFields.RoleName,
        query: {
          terms: {
            roleId: roleIdList,
          },
        },
        sort: {
          [RoleNamesDataFields.RoleId]: SortOrder.Ascending,
        },
      };

      search = {
        body,
        index: this.config.ElasticIndexRoleNames,
      };

      this.loggingService.log(ApplicationLogs.MakingElasticSearchCallToGetRoleNames, {
        functionName,
        roleIdList,
      });

      const roles: string[] = [];
      let result: ApiResponse<SearchResponse<Role>> = await esClient.search(search);
      let response: SearchResponse<Role> = result.body;

      while (response.hits && response.hits.hits) {
        response.hits.hits.map(rs => {
          roles.push(rs._source.roleName);
        });

        this.loggingService.log(
          { level: LogLevel.Verbose, message: `Received ${roles.length} of ${response.hits.total} role names.` },
          {
            functionName,
            roleIdList,
          }
        );

        if (response.hits.total === roles.length) {
          // received all roles no paging
          break;
        }

        const body = {
          size: this.config.RoleNamesQueryLimit,
          _source: RoleNamesDataFields.RoleName,
          query: {
            terms: {
              roleId: roleIdList,
            },
          },
          search_after: response.hits.hits[response.hits.hits.length - 1].sort,
          sort: {
            [RoleNamesDataFields.RoleId]: SortOrder.Ascending,
          },
        };

        search = {
          body,
          index: this.config.ElasticIndexRoleNames,
        };

        result = await esClient.search(search);
        response = result.body;
      }
      return roles;
    } catch (error) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetRoleNames,
        {
          functionName,
          maxRetries: this.config.ElasticMaxRetries,
        },
        error
      );
      throw error;
    }
  }

  @errorCounter(UserDataStoreImpl.metricLabels)
  @histogram(UserDataStoreImpl.metricLabels)
  @logExecution(logLevels.debug, true)
  private async getUserElastic(companyHash: string, username: string): Promise<User> {
    const functionName = 'UserDataStoreImpl.getUserElastic';

    if (this.config.UseSqlOnly) {
      // logEvent(logLevels.info, 'Configuration is turned on to fall back to SQL.', globalStrings.componentName, {
      //   functionName,
      // });
      this.loggingService.log(ApplicationLogs.SqlFallBackTurnedOn, {
        functionName,
      });
      return this.userDataStoreSql.getUser(companyHash, username);
    }

    try {
      const indices = [this.config.ElasticIndexPeopleLoginCredential, this.config.ElasticIndexRoleNames];

      // get index refresh times from cache ..
      const cachedRefreshTimes = await this.extractRefreshTimeCache.getExtractRefreshTimes(indices);

      if (!AuthorizationExtractDataUtil.isExtractDataLatest(indices, cachedRefreshTimes, this.config)) {
        const processExtractResponse = await this.authorizationExtractDataStore.getLastRefreshTime(indices);

        // set refresh times in cache ..
        await this.extractRefreshTimeCache.setExtractRefreshTimes(processExtractResponse);

        // last refresh is greater than configured minutes, fallback to sql
        if (!AuthorizationExtractDataUtil.isExtractDataLatest(indices, processExtractResponse, this.config)) {
          // logEvent(
          //   logLevels.info,
          //   'last refresh information not found or refresh time is greater than configured minutes. fall back to SQL will be attempted.',
          //   globalStrings.componentName,
          //   {
          //     functionName,
          //     processExtractResponse,
          //     deltaRefreshTolerance: this.config.ElasticDeltaRefreshMinutesWithTolerance,
          //     fullRefreshTolerance: this.config.ElasticFullRefreshMinutesWithTolerance,
          //   }
          // );
          this.loggingService.log(ApplicationLogs.ExtractInformationIsNotLatest, {
            functionName,
            processExtractResponse,
            deltaRefreshTolerance: this.config.ElasticDeltaRefreshMinutesWithTolerance,
            fullRefreshTolerance: this.config.ElasticFullRefreshMinutesWithTolerance,
          });
          return this.userDataStoreSql.getUser(companyHash, username);
        }
      }

      const result = await this.getPeopleLoginCredential(companyHash, username);

      if (result) {
        const userData: User = new User(
          result.userName,
          result.peopleId.toString(),
          result.companyId.toString(),
          result.siteId.toString(),
          result.site_isActive ? 1 : 0
        );
        userData.roles = await this.gatherRoles(result.role_list.split(',').map(Number));
        return userData;
      }
      return undefined;
    } catch (error) {
      // logEvent(
      //   logLevels.error,
      //   'An error occurred fetching user data from elastic search. Fallback to SQL will be attempted.',
      //   globalStrings.componentName,
      //   {
      //     functionName,
      //     username,
      //     companyHash,
      //     maxRetries: this.config.ElasticMaxRetries,
      //   },
      //   ElasticsearchErrors.getErrorResponse(error)
      // );
      this.loggingService.log(
        ApplicationLogs.CouldNotGetUser,
        {
          functionName,
          username,
          companyHash,
          maxRetries: this.config.ElasticMaxRetries,
        },
        error
      );
      throw error;
    }
  }
}
