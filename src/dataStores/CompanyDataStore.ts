import { Company } from "../models/Company";

export interface CompanyDataStore {
  getCompanies(companyIds: string[]): Promise<Company[]>;
}
