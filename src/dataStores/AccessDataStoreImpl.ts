import 'reflect-metadata';
import { AccessDataStore } from './AccessDataStore';
import { AccessResult } from '../models/AccessResult';
import { injectable, inject } from 'inversify';
import { DataStore } from '../models/DataStore';
import { User } from '../models/User';
import { Site } from '../models/Site';
import { VehicleDataStore } from '../dataStores/VehicleDataStore';
import { SmartRecorderDataStore } from '../dataStores/SmartRecorderDataStore';
import { Vehicle } from '../models/Vehicle';
import { SmartRecorder } from '../models/SmartRecorder';
import { CompanyDataStore } from './CompanyDataStore';
import { Company } from '../models/Company';
import {
  errorCounter,
  histogram,
  logExecution,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  DALLabelValues,
  LoggingService,
  LogLevel,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';

@injectable()
export class AccessDataStoreImpl implements AccessDataStore {
  @inject(InversifyTypes.VehicleDataStore)
  private vehicleDataStore: VehicleDataStore;

  @inject(InversifyTypes.SmartRecorderDataStore)
  private smartRecorderDataStore: SmartRecorderDataStore;

  @inject(InversifyTypes.CompanyDataStore)
  private companyDataStore: CompanyDataStore;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  public static metricsLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.AccessDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.CoreOLTP,
  };

  @histogram(AccessDataStoreImpl.metricsLabels)
  @errorCounter(AccessDataStoreImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async canUserAccessCompanies(
    companyIds: string[],
    userInfo: User,
    superuser: boolean
  ): Promise<AccessResult[]> {
    const functionName = 'AccessDataStoreImpl.canUserAccessCompanies';

    try {
      const validCompanyIds = companyIds.filter(item => {
        return /^\d+$/.test(item); // return items with valid number strings
      });

      let companies: Company[] = [];
      if (validCompanyIds.length) {
        companies = await this.companyDataStore.getCompanies(validCompanyIds);
      }

      // For each given companyId, declare a new AccessResults.
      // If that companyId is the user's company or they're a superuser,
      // they have access.
      return companyIds.map(companyId => {
        const companyAccess: AccessResult = {
          resourceType: 'company',
          resourceId: companyId,
          resourceName: null,
          hasAccess: false,
        };

        const matchingCompanies = companies.filter(company => {
          return company && company.EntityIdentityKey && company.EntityIdentityKey.toString() === companyId;
        });

        if (companyId === userInfo.companyId || superuser) {
          if (matchingCompanies.length > 0) {
            companyAccess.hasAccess = true;
            companyAccess.resourceName = matchingCompanies[0].CompanyName;
          }
        }
        return companyAccess;
      });
    } catch (error) {
      this.loggingService.log(ApplicationLogs.CouldNotGetCompanyAccess, { functionName }, error);
      throw error;
    }
  }

  @histogram(AccessDataStoreImpl.metricsLabels)
  @errorCounter(AccessDataStoreImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async canUserAccessSites(siteIds: string[], childSites: Site[], superuser: boolean): Promise<AccessResult[]> {
    const functionName = 'AccessDataStoreImpl.canUserAccessSites';

    try {
      // For each given siteId, declare a new AccessResults.
      // If that siteId is found in the user's childSites, they have
      // access.
      return siteIds.map(siteId => {
        const siteAccess: AccessResult = {
          resourceType: 'site',
          resourceId: siteId,
          resourceName: null,
          hasAccess: false,
        };
        const matchingSites = childSites.filter(site => {
          return site.siteId === siteId;
        });
        if (matchingSites.length > 0) {
          siteAccess.resourceName = matchingSites[0].siteName;
          siteAccess.hasAccess = true;
        }
        return siteAccess;
      });
    } catch (error) {
      this.loggingService.log(ApplicationLogs.CouldNotGetSiteAccess, { functionName }, error);
      throw error;
    }
  }

  @histogram(AccessDataStoreImpl.metricsLabels)
  @errorCounter(AccessDataStoreImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async canUserAccessVehicles(
    vehicleIds: string[],
    childSites: Site[],
    superuser: boolean
  ): Promise<AccessResult[]> {
    const functionName = 'AccessDataStoreImpl.canUserAccessVehicles';

    try {
      const vehicles: Vehicle[] = await this.vehicleDataStore.getVehicles(vehicleIds);
      // For each given vehicleId, declare a new AccessResults.
      // Find that vehicle's siteId.
      // If that siteId is found in the user's childSites, they have
      // access.
      return vehicleIds.map(vehicleId => {
        const vehicleAccess: AccessResult = {
          resourceType: 'vehicle',
          resourceId: vehicleId,
          resourceName: null,
          hasAccess: false,
        };
        const matchingVehicles = vehicles.filter(vehicle => {
          return vehicle !== undefined && vehicle.vehicleId === vehicleId;
        });
        if (matchingVehicles.length > 0) {
          const vehicleSiteId = matchingVehicles[0].siteId;
          const matchingSites = childSites.filter(site => {
            return site.siteId === vehicleSiteId;
          });
          if (matchingSites.length > 0 || superuser) {
            vehicleAccess.resourceName = matchingVehicles[0].vehicleSn;
            vehicleAccess.hasAccess = true;
          }
        }
        return vehicleAccess;
      });
    } catch (error) {
      this.loggingService.log(ApplicationLogs.CouldNotGetVehicleAccess, { functionName }, error);
      throw error;
    }
  }

  @histogram(AccessDataStoreImpl.metricsLabels)
  @errorCounter(AccessDataStoreImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async canUserAccessSmartRecorders(
    smartRecorderIds: string[],
    smartRecorderSns: string[],
    childSites: Site[],
    superuser: boolean
  ): Promise<AccessResult[]> {
    const functionName = 'AccessDataStoreImpl.canUserAccessSmartRecorders';

    try {
      let accessById: AccessResult[] = [];
      let accessBySn: AccessResult[] = [];
      const smartRecorders = await this.smartRecorderDataStore.getSmartRecorders(smartRecorderIds, smartRecorderSns);
      if (Array.isArray(smartRecorderIds) && smartRecorderIds.length > 0) {
        accessById = this.getSmartRecorderAccessByIds(smartRecorderIds, smartRecorders, childSites, superuser);
      }
      if (Array.isArray(smartRecorderSns) && smartRecorderSns.length > 0) {
        accessBySn = this.getSmartRecorderAccessBySns(smartRecorderSns, smartRecorders, childSites, superuser);
      }
      return accessById.concat(accessBySn);
    } catch (error) {
      this.loggingService.log(ApplicationLogs.CouldNotGetSmartRecorderAccess, { functionName }, error);
      throw error;
    }
  }

  private getSmartRecorderAccessByIds(
    smartRecorderIds: string[],
    smartRecorders: SmartRecorder[],
    childSites: Site[],
    superuser: boolean
  ): AccessResult[] {
    // For each given srId, declare a new AccessResults.
    // Find that SR's siteId.
    // If that siteId is found in the user's childSites, they have
    // access.
    return smartRecorderIds.map(srId => {
      const srAccess: AccessResult = {
        resourceType: 'smartRecorder',
        resourceId: srId,
        resourceName: null,
        hasAccess: false,
      };
      const matchingSrs = smartRecorders.filter(sr => {
        return sr !== undefined && sr.srId === srId;
      });
      if (matchingSrs.length > 0) {
        const srSiteId = matchingSrs[0].siteId;
        const matchingSites = childSites.filter(site => {
          return site.siteId === srSiteId;
        });
        if (matchingSites.length > 0 || superuser) {
          srAccess.resourceName = matchingSrs[0].srSn;
          srAccess.hasAccess = true;
        }
      }
      return srAccess;
    });
  }

  private getSmartRecorderAccessBySns(
    smartRecorderSns: string[],
    smartRecorders: SmartRecorder[],
    childSites: Site[],
    superuser: boolean
  ): AccessResult[] {
    // For each given srSn, declare a new AccessResults.
    // Find that SR's siteId.
    // If that siteId is found in the user's childSites, they have
    // access.
    return smartRecorderSns.map(srSn => {
      const srAccess: AccessResult = {
        resourceType: 'smartRecorder',
        resourceId: null,
        resourceName: srSn,
        hasAccess: false,
      };
      const matchingSrs = smartRecorders.filter(sr => {
        return sr !== undefined && sr.srSn.toUpperCase() === srSn.toUpperCase();
      });
      if (matchingSrs.length > 0) {
        const srSiteId = matchingSrs[0].siteId;
        const matchingSites = childSites.filter(site => {
          return site.siteId === srSiteId;
        });
        if (matchingSites.length > 0 || superuser) {
          srAccess.resourceId = matchingSrs[0].srId;
          srAccess.hasAccess = true;
        }
      }
      return srAccess;
    });
  }
}
