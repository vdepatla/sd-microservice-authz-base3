import { Config } from '../Config';
import { ExtractRefreshTime } from '../models/ExtractRefreshTime';
import { MomentUtil } from '../MomentUtil';

export class AuthorizationExtractDataUtil {
  public static isExtractDataLatest(
    indicesToCheck: string[],
    refreshTimes: ExtractRefreshTime[],
    config: Config
  ): boolean {
    for (const index of indicesToCheck) {
      const toleranceMinutes = config.ElasticDeltaRefreshIndices.includes(index)
        ? config.ElasticDeltaRefreshMinutesWithTolerance
        : config.ElasticFullRefreshIndices.includes(index)
        ? config.ElasticFullRefreshMinutesWithTolerance
        : undefined;
      if (
        !toleranceMinutes ||
        !refreshTimes ||
        !refreshTimes.find(
          a => a.index === index && MomentUtil.durationMinFromCurrent(a.refreshTimeUnix) <= toleranceMinutes
        )
      ) {
        return false;
      }
    }
    return true;
  }
}
