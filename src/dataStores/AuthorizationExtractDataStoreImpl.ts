import { AuthorizationExtractDataStore } from './AuthorizationExtractDataStore';
import { injectable, inject } from 'inversify';
import { ElasticsearchClient } from '../models/ElasticSearch/ElasticsearchClient';
import { RequestParams } from '@elastic/elasticsearch';
import { ElasticsearchQueryFactory } from '../models/ElasticSearch/Queries/ElasticsearchQueryFactory';
import { Config } from '../Config';
import { ExtractRefreshTime } from '../models/ExtractRefreshTime';
import * as moment from 'moment-timezone';
import { DataStore } from '../models/DataStore';
import { Timezone } from '../models/Timezone';
import {
  errorCounter,
  histogram,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  LoggingService,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';

@injectable()
export class AuthorizationExtractDataStoreImpl implements AuthorizationExtractDataStore {
  public static metricLabels = {
    [MetricLabels.DataStore]: DataStore.AuthorizationExtractDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Elasticsearch,
  };

  @inject(InversifyTypes.ElasticsearchClient)
  private elasticsearchClient: ElasticsearchClient;

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @histogram(AuthorizationExtractDataStoreImpl.metricLabels)
  @errorCounter(AuthorizationExtractDataStoreImpl.metricLabels)
  public async getLastRefreshTime(indices: string[]): Promise<ExtractRefreshTime[]> {
    const functionName = 'AuthorizationExtractDataStoreImpl.getLastRefreshTime';

    const body = ElasticsearchQueryFactory.getProcessExtractRefreshTime(indices);

    const search: RequestParams.Search = {
      body,
      index: this.config.ElasticIndexProcessExtractRefresh,
    };

    try {
      const esClient = await this.elasticsearchClient.getClient();
      this.loggingService.log(ApplicationLogs.MakingElasticSearchCallToGetExtractRefreshTime, { functionName, search });

      const result = await esClient.search(search);
      const response = result.body;

      const extractResponse = [];
      if (response && response.hits && response.hits.hits.length) {
        extractResponse.push(...response.hits.hits);
      }

      return extractResponse.map(rs => {
        return {
          index: rs._source.doc.index_name,
          refreshTimeUnix: moment
            .tz(rs._source.doc.last_updated_timestamp, 'YYYY-MM-DD hh:mm:ss:zzz', Timezone.Pacific)
            .utc()
            .unix(),
        };
      });
    } catch (error) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetExtractRefreshTimeFromSql,
        { functionName, indices, maxRetries: this.config.ElasticMaxRetries, search },
        error
      );
      throw error;
    }
  }
}
