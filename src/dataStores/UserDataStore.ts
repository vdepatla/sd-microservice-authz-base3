import { User } from "../models/User";
import { PeopleLoginCredentialDataResponse } from "../models/ElasticSearch/PeopleLoginCredentialDataResponse";

/**
 * Interface for the User data store.
 */
export interface UserDataStore {
  getUser(companyHash: string, username: string): Promise<User>;
  getPeopleLoginCredential(
    companyHash: string,
    username: string
  ): Promise<PeopleLoginCredentialDataResponse>;
}
