import { AuthorizationResult } from '../models/AuthorizationResult';
import { LoginEnabled } from '../models/LoginEnabled';

export interface AuthorizationDataStoreSql {
  getPermissions(companyHash: string, username: string, resources: string[]): Promise<AuthorizationResult[]>;
  getLoginEnabled(companyHash: string, username: string): Promise<LoginEnabled>;
}
