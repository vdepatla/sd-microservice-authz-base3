import 'reflect-metadata';
import * as sql from 'mssql';
import { SmartRecorder } from '../models/SmartRecorder';
import { injectable, inject } from 'inversify';
import { SmartRecorderDataStore } from './SmartRecorderDataStore';
import { Config } from '../Config';
import { SmartRecorderQueryBuilder } from './SmartRecorderQueryBuilder';
import {
  errorCounter,
  histogram,
  logExecution,
  DALLabelValues,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  LoggingService,
  LogLevel,
} from '@sd-microservice-base/instrumentation';
import { DataStore } from '../models/DataStore';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { ConnectionPoolManager } from '../sql/ConnectionPoolManager';

/**
 * Used to retrieve SR information
 */
@injectable()
export class SmartRecorderDataStoreImpl implements SmartRecorderDataStore {
  public static metricLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.SmartRecorderDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.CoreOLTP,
  };

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @inject(InversifyTypes.ConnectionPoolManager)
  private connectionPoolManager: ConnectionPoolManager;

  /**
   * Retrieves SR info given a SR ID or SR SN.
   * @param srIds List of SR Ids.
   * @param srSns List of SR Sns.
   * @returns SmartRecorder[] for requested SRs
   */
  @histogram(SmartRecorderDataStoreImpl.metricLabels)
  @errorCounter(SmartRecorderDataStoreImpl.metricLabels)
  @logExecution(LogLevel.Debug, true)
  public async getSmartRecorders(srIds: string[], srSns: string[]): Promise<SmartRecorder[]> {
    const functionName = 'SmartRecorderDataStoreImpl.getSmartRecorders';

    try {
      const conn = await this.connectionPoolManager.getReadOnlyConnectionPool();
      const req = new sql.Request(conn);
      const query = SmartRecorderQueryBuilder.buildQuery(srIds, srSns);
      if (srIds !== undefined && srIds.length > 0) {
        // Because we need to add each value individually, we're going to
        // create an object that looks like
        // {@srId0: srIds[0], @srIds1: srIds[1], ...}
        const keyValueSrIds = srIds.reduce<{ [key: string]: string }>((obj, item, idx) => {
          obj[`@srIds${idx}`] = item;
          return obj;
        }, {});
        // Now let's add each value as input
        Object.keys(keyValueSrIds).forEach(key => {
          // The slice is to strip the '@' since that goofs things up
          req.input(key.slice(1), keyValueSrIds[key]);
        });
      }
      if (srSns !== undefined && srSns.length > 0) {
        // Because we need to add each value individually, we're going to
        // create an object that looks like
        // {@srSns0: srSns[0], @srSns1: srSns[1], ...}
        const keyValueSrSns = srSns.reduce<{ [key: string]: string }>((obj, item, idx) => {
          obj[`@srSns${idx}`] = item;
          return obj;
        }, {});
        // Now let's add each value as input
        Object.keys(keyValueSrSns).forEach(key => {
          // The slice is to strip the '@' since that goofs things up
          req.input(key.slice(1), keyValueSrSns[key]);
        });
      }
      const result = await req.query(query);
      const srs = result.recordset.map(rs => {
        return {
          srId: rs.CameraId.toString(),
          srSn: rs.CameraSN,
          siteId: rs.SiteId,
        };
      });
      return srs.length !== 0 ? srs : [];
    } catch (err) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetSmartRecorders,
        {
          functionName,
          db: {
            host: this.config.SQLServer,
            name: this.config.SQLDatabase,
          },
        },
        err
      );
      throw err;
    }
  }
}
