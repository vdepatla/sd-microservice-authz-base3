import { Company } from '../models/Company';

export interface CompanyDataStoreSql {
  getCompanies(companyIds: string[]): Promise<Company[]>;
}
