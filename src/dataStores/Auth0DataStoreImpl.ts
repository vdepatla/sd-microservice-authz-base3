import 'reflect-metadata';
import { injectable, inject } from 'inversify';
import { AccessTokenResponse } from '../models/AccessTokenResponse';
import { Config } from '../Config';
import * as rp from 'request-promise-native';
import { Auth0DataStore } from './Auth0DataStore';
import { DataStore } from '../models/DataStore';
import {
  errorCounter,
  histogram,
  MetricLabels,
  ExternalResourceActions,
  DALLabelValues,
  LoggingService,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';

@injectable()
export class Auth0DataStoreImpl implements Auth0DataStore {
  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  public static metricLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.Auth0DataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: 'Auth0',
  };

  @histogram(Auth0DataStoreImpl.metricLabels)
  @errorCounter(Auth0DataStoreImpl.metricLabels)
  public async getM2MToken(): Promise<AccessTokenResponse> {
    const functionName = 'AccessToken.getM2MToken';

    try {
      const options = {
        uri: `${this.config.Auth0Issuer}oauth/token`,
        timeout: this.config.Auth0RequestTimeout,
        json: true,
        method: 'post',
        body: {
          client_id: this.config.Auth0ClientId,
          client_secret: this.config.Auth0SigningKey,
          audience: this.config.Auth0Audience,
          grant_type: 'client_credentials',
        },
      };

      const response = await rp.post(options);

      this.loggingService.log(ApplicationLogs.CreatedMachineToMachine, { functionName, response });

      return {
        accessToken: response.access_token || '',
        expiryTimeSeconds: response.expires_in || 0,
      };
    } catch (err) {
      this.loggingService.log(
        ApplicationLogs.CouldNotCreateMachineToMachineToken,
        { functionName, ClientID: this.config.Auth0ClientId },
        err
      );
      throw err;
    }
  }
}
