import { Vehicle } from "../models/Vehicle";

/**
 * Interface for the vehicle data store.
 */
export interface VehicleDataStore {
  getVehicles(vehicleIds: string[]): Promise<Vehicle[]>;
}
