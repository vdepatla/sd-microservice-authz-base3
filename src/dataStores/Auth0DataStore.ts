import { AccessTokenResponse } from "../models/AccessTokenResponse";

export interface Auth0DataStore {
  getM2MToken(): Promise<AccessTokenResponse>;
}
