import 'reflect-metadata';
import { injectable, inject } from 'inversify';
import { AuthorizationDataStore } from './AuthorizationDataStore';
import { AuthorizationResult } from '../models/AuthorizationResult';
import { RolePermissionsDataResponse } from '../models/ElasticSearch/RolePermissionsDataResponse';
import { ApplicationMembershipDataResponse } from '../models/ElasticSearch/ApplicationMembershipDataResponse';
import { LoginEnabled } from '../models/LoginEnabled';
import { Config } from '../Config';
import { DataStore } from '../models/DataStore';
import { UserDataStore } from './UserDataStore';
import { RolePermissionsDataFields } from '../models/ElasticSearch/RolePermissionsDataFields';
import { ElasticsearchQueryFactory } from '../models/ElasticSearch/Queries/ElasticsearchQueryFactory';
import { SortOrder } from '../models/ElasticSearch/SortOrder';
import { Constants } from '../models/Constants';
import { RequestParams, ApiResponse } from '@elastic/elasticsearch';
import { SearchResponse } from '../models/ElasticSearch/SearchResponse';
import { ElasticsearchClient } from '../models/ElasticSearch/ElasticsearchClient';
import { AuthorizationExtractDataStore } from './AuthorizationExtractDataStore';
import { AuthorizationDataStoreSql } from './AuthorizationDataStoreSql';
import { ExtractRefreshTimeCache } from './ExtractRefreshTimeCache';
import { AuthorizationExtractDataUtil } from './AuthorizationExtractDataUtil';
import {
  errorCounter,
  histogram,
  logExecution,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  DALLabelValues,
  LoggingService,
  LogLevel,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { ForbiddenError } from '@sd-microservice-base/security/dist/model/errors/ForbiddenError';

@injectable()
export class AuthorizationDataStoreImpl implements AuthorizationDataStore {
  @inject(InversifyTypes.UserDataStore)
  private userDataStore: UserDataStore;

  @inject(InversifyTypes.ElasticsearchClient)
  private elasticsearchClient: ElasticsearchClient;

  @inject(InversifyTypes.AuthorizationExtractDataStore)
  private authorizationExtractDataStore: AuthorizationExtractDataStore;

  @inject(InversifyTypes.ExtractRefreshTimeCache)
  private extractRefreshTimeCache: ExtractRefreshTimeCache;

  @inject(InversifyTypes.AuthorizationDataStoreSql)
  private authorizationDataStoreSql: AuthorizationDataStoreSql;

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  public static metricLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.AuthorizationDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Elasticsearch,
  };

  public async getPermissions(
    companyHash: string,
    username: string,
    resources: string[]
  ): Promise<AuthorizationResult[]> {
    try {
      return await this.getPermissionsElastic(companyHash, username, resources);
    } catch (error) {
      return await this.authorizationDataStoreSql.getPermissions(companyHash, username, resources);
    }
  }

  public async getLoginEnabled(companyHash: string, username: string): Promise<LoginEnabled> {
    try {
      return await this.getLoginEnabledElastic(companyHash, username);
    } catch (error) {
      return await this.authorizationDataStoreSql.getLoginEnabled(companyHash, username);
    }
  }

  @errorCounter(AuthorizationDataStoreImpl.metricLabels)
  @histogram(AuthorizationDataStoreImpl.metricLabels)
  @logExecution(LogLevel.Debug, true)
  private async getLoginEnabledElastic(companyHash: string, username: string): Promise<LoginEnabled> {
    const functionName = 'AuthorizationDataStoreImpl.getLoginEnabledElastic';

    if (this.config.UseSqlOnly) {
      this.loggingService.log(ApplicationLogs.SqlFallBackTurnedOn, { functionName });
      return this.authorizationDataStoreSql.getLoginEnabled(companyHash, username);
    }

    let search: RequestParams.Search;

    try {
      const indices = [this.config.ElasticIndexPeopleLoginCredential, this.config.ElasticIndexApplicationMembership];

      // get index refresh times from cache ..
      const cachedRefreshTimes = await this.extractRefreshTimeCache.getExtractRefreshTimes(indices);

      if (!AuthorizationExtractDataUtil.isExtractDataLatest(indices, cachedRefreshTimes, this.config)) {
        const processExtractResponse = await this.authorizationExtractDataStore.getLastRefreshTime(indices);

        // set refresh times in cache ..
        await this.extractRefreshTimeCache.setExtractRefreshTimes(processExtractResponse);

        // last refresh is greater than configured minutes, fallback to sql
        if (!AuthorizationExtractDataUtil.isExtractDataLatest(indices, processExtractResponse, this.config)) {
          this.loggingService.log(ApplicationLogs.ExtractInformationIsNotLatest, {
            functionName,
            companyHash,
            username,
            processExtractResponse,
            deltaRefreshTolerance: this.config.ElasticDeltaRefreshMinutesWithTolerance,
            fullRefreshTolerance: this.config.ElasticFullRefreshMinutesWithTolerance,
          });
          return this.authorizationDataStoreSql.getLoginEnabled(companyHash, username);
        }
      }

      let response = true;
      const peopleLoginCredential = await this.userDataStore.getPeopleLoginCredential(companyHash, username);

      if (
        peopleLoginCredential &&
        peopleLoginCredential.siteHash &&
        peopleLoginCredential.site_isActive &&
        peopleLoginCredential.people_isActive &&
        peopleLoginCredential.company_isActive
      ) {
        // check if application membership is active and enabled for user login
        const body = ElasticsearchQueryFactory.getLoginEnabledApplicationMembership(
          peopleLoginCredential.siteHash,
          Constants.EnableUserLoginApplicationId
        );

        search = {
          body,
          index: this.config.ElasticIndexApplicationMembership,
        };

        this.loggingService.log(ApplicationLogs.MakingElasticSearchCallToGetApplicationMembership, {
          functionName,
          companyHash,
          username,
          search,
        });

        const esClient = await this.elasticsearchClient.getClient();
        const result: ApiResponse<SearchResponse<ApplicationMembershipDataResponse>> = await esClient.search(search);
        const responseBody = result.body;

        if (responseBody.hits && responseBody.hits.total > 0) {
          response = !!peopleLoginCredential.isLocked;
        } else {
          this.loggingService.log(ApplicationLogs.UserSiteNotEnabled, {
            functionName,
            companyHash,
            username,
            siteHash: peopleLoginCredential.siteHash,
            search,
          });
        }
      }

      return {
        isLocked: response,
      };
    } catch (error) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetApplicationMembershipFromElasticsearch,
        {
          search,
          companyHash,
          username,
          functionName,
          maxRetries: this.config.ElasticMaxRetries,
        },
        error
      );
      throw error;
    }
  }

  @errorCounter(AuthorizationDataStoreImpl.metricLabels)
  @histogram(AuthorizationDataStoreImpl.metricLabels)
  @logExecution(LogLevel.Debug, true)
  private async getPermissionsElastic(
    companyHash: string,
    username: string,
    resources: string[]
  ): Promise<AuthorizationResult[]> {
    const functionName = 'AuthorizationDataStoreImpl.getPermissionsElastic';

    if (this.config.UseSqlOnly) {
      this.loggingService.log(ApplicationLogs.SqlFallBackTurnedOn, {
        functionName,
      });
      return this.authorizationDataStoreSql.getPermissions(companyHash, username, resources);
    }

    let search: RequestParams.Search;

    try {
      const indices = [this.config.ElasticIndexPeopleLoginCredential, this.config.ElasticIndexRolePermissions];

      // get index refresh times from cache ..
      const cachedRefreshTimes = await this.extractRefreshTimeCache.getExtractRefreshTimes(indices);

      if (!AuthorizationExtractDataUtil.isExtractDataLatest(indices, cachedRefreshTimes, this.config)) {
        const processExtractResponse = await this.authorizationExtractDataStore.getLastRefreshTime(indices);

        // set refresh times in cache ..
        await this.extractRefreshTimeCache.setExtractRefreshTimes(processExtractResponse);

        // last refresh is greater than configured minutes, fallback to sql
        if (!AuthorizationExtractDataUtil.isExtractDataLatest(indices, processExtractResponse, this.config)) {
          this.loggingService.log(ApplicationLogs.ExtractInformationIsNotLatest, {
            functionName,
            processExtractResponse,
            deltaRefreshTolerance: this.config.ElasticDeltaRefreshMinutesWithTolerance,
            maxRetries: this.config.ElasticMaxRetries,
          });
          return this.authorizationDataStoreSql.getPermissions(companyHash, username, resources);
        }
      }

      const peopleLoginCredential = await this.userDataStore.getPeopleLoginCredential(companyHash, username);

      if (!peopleLoginCredential) {
        throw new ForbiddenError();
      }

      const roleIds = peopleLoginCredential.role_list.split(',').map(Number);
      const rolePermissions = ElasticsearchQueryFactory.getRolePermissions(resources, roleIds, peopleLoginCredential);
      const body = {
        size: this.config.RolePermissionsQueryLimit,
        query: rolePermissions,
        sort: {
          [RolePermissionsDataFields.AuthPermissionId]: SortOrder.Ascending,
        },
      };

      search = {
        body,
        index: this.config.ElasticIndexRolePermissions,
      };

      const esClient = await this.elasticsearchClient.getClient();

      this.loggingService.log(ApplicationLogs.MakingElasticSearchCallToGetPermissions, {
        functionName,
        companyHash,
        username,
        search,
      });

      let result: ApiResponse<SearchResponse<RolePermissionsDataResponse>> = await esClient.search(search);
      const response = result.body;

      const allPermissions: RolePermissionsDataResponse[] = [];
      while (response.hits && response.hits.hits.length) {
        allPermissions.push(...response.hits.hits.map(r => r._source));

        this.loggingService.log(
          {
            level: LogLevel.Verbose,
            message: `Received ${allPermissions.length} of ${response.hits.total} permissions.`,
          },
          {
            functionName,
            companyHash,
            username,
            search,
          }
        );

        if (response.hits.total === allPermissions.length) {
          // received all permissions no paging
          break;
        }

        const body = {
          size: this.config.RolePermissionsQueryLimit,
          query: rolePermissions,
          search_after: response.hits.hits[response.hits.hits.length - 1].sort,
          sort: {
            [RolePermissionsDataFields.AuthPermissionId]: SortOrder.Ascending,
          },
        };

        search = {
          body,
          index: this.config.ElasticIndexRolePermissions,
        };

        result = await esClient.search(search);
      }

      return allPermissions.map(rs => {
        return {
          resource: rs.resource,
          role: rs.name,
          permission: rs.permission,
          userScope: rs.userScope,
          permissionScope: rs.permissionScope,
        };
      });
    } catch (error) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetPermissionsFromElasticsearch,
        {
          functionName,
          companyHash,
          username,
          search,
          maxRetries: this.config.ElasticMaxRetries,
        },
        error
      );
      throw error;
    }
  }
}
