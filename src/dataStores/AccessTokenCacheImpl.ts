import 'reflect-metadata';
import { injectable, inject } from 'inversify';
import { AccessTokenResponse } from '../models/AccessTokenResponse';
import { DataStore } from '../models/DataStore';
import { AccessTokenCache } from './AccessTokenCache';
import {
  histogram,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  DALLabelValues,
  LoggingService,
} from '@sd-microservice-base/instrumentation';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { RedisDataStore } from '@sd-microservice-base/redis';
import { InversifyTypes } from '../models/InversifyTypes';

@injectable()
export class AccessTokenCacheImpl implements AccessTokenCache {
  public static readMetricLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.AccessTokenCache,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Redis,
  };

  public static writeMetricLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.AccessTokenCache,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Create,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.Redis,
  };

  @inject(InversifyTypes.RedisDataStore)
  private redisDataStore: RedisDataStore;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @histogram(AccessTokenCacheImpl.readMetricLabels)
  private async readCacheValue(key: string): Promise<string> {
    const functionName = 'AccessTokenCacheImpl.readCacheValue';

    // First, try and read.
    try {
      const cachedValue = await this.redisDataStore.client.get(key);
      if (cachedValue) {
        this.loggingService.log(ApplicationLogs.FoundValueInRedis, { functionName, key });
        return cachedValue;
      }
    } catch (readError) {
      this.loggingService.log(ApplicationLogs.RedisReadError, { functionName, key }, readError);
      return undefined;
    }
    this.loggingService.log(ApplicationLogs.CouldNotFindValueInRedis, { functionName, key });
  }

  @histogram(AccessTokenCacheImpl.writeMetricLabels)
  private async writeCacheValue(key: string, fallbackFunction: () => Promise<AccessTokenResponse>): Promise<string> {
    const functionName = 'AccessTokenCacheImpl.writeCacheValue';
    /**
     * If the read failed or there was a cache miss, run the fn.
     * If the fn throws, it will be handled outside of this fn by the caller.
     */
    const accessTokenResp = await fallbackFunction();

    // Try to write.
    try {
      await this.redisDataStore.client.set(key, accessTokenResp.accessToken, 'EX', accessTokenResp.expiryTimeSeconds);
    } catch (writeError) {
      this.loggingService.log(ApplicationLogs.RedisWriteError, { functionName, key }, writeError);
    }

    // Return the new value, regardless of whether we could write to cache.
    return accessTokenResp.accessToken;
  }

  public async getAccessToken(key: string, fallbackFunction: () => Promise<AccessTokenResponse>): Promise<string> {
    const accessToken = await this.readCacheValue(key);

    if (accessToken) {
      return accessToken;
    }

    return await this.writeCacheValue(key, fallbackFunction);
  }
}
