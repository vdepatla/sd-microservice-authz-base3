import { AccessTokenResponse } from '../models/AccessTokenResponse';

export interface AccessTokenCache {
  getAccessToken(key: string, fallbackFunction: () => Promise<AccessTokenResponse>): Promise<string>;
}
