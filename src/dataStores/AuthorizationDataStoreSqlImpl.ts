import 'reflect-metadata';
import { injectable, inject } from 'inversify';
import { AuthorizationDataStoreSql } from './AuthorizationDataStoreSql';
import { AuthorizationResult } from '../models/AuthorizationResult';
import { LoginEnabled } from '../models/LoginEnabled';
import * as AuthorizationQueryBuilder from '../services/AuthorizationQueryBuilder';
import * as sql from 'mssql';
import { DataStore } from '../models/DataStore';
import {
  errorCounter,
  histogram,
  logExecution,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  DALLabelValues,
  LogLevel,
  LoggingService,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { ConnectionPoolManager } from '../sql/ConnectionPoolManager';

@injectable()
export class AuthorizationDataStoreSqlImpl implements AuthorizationDataStoreSql {
  public static metricsLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.AuthorizationDataStoreSQL,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.CoreOLTP,
  };

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @inject(InversifyTypes.ConnectionPoolManager)
  private connectionPoolManager: ConnectionPoolManager;

  @histogram(AuthorizationDataStoreSqlImpl.metricsLabels)
  @errorCounter(AuthorizationDataStoreSqlImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async getPermissions(
    companyHash: string,
    username: string,
    resources: string[]
  ): Promise<AuthorizationResult[]> {
    let queryString = '';

    try {
      const conn = await this.connectionPoolManager.getReadOnlyConnectionPool();

      queryString = AuthorizationQueryBuilder.getPermissionsQuery(resources);

      const request = new sql.Request(conn);
      request.input('username', sql.VarChar(254), username);
      request.input('companyHash', sql.Char(4), companyHash);
      const result = await request.query(queryString);

      return result.recordset.map(rs => {
        return {
          resource: rs.resource,
          role: rs.role,
          permission: rs.permission,
          userScope: rs.userScope,
          permissionScope: rs.permissionScope,
        };
      });
    } catch (error) {
      this.loggingService.log(ApplicationLogs.CouldNotGetPermissionsFromSql, { query: queryString }, error);
      throw error;
    }
  }

  @histogram(AuthorizationDataStoreSqlImpl.metricsLabels)
  @errorCounter(AuthorizationDataStoreSqlImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async getLoginEnabled(companyHash: string, username: string): Promise<LoginEnabled> {
    let queryString = '';

    try {
      const conn = await this.connectionPoolManager.getReadOnlyConnectionPool();

      queryString = AuthorizationQueryBuilder.getLoginEnabledQuery();

      const request = new sql.Request(conn);
      request.input('username', sql.VarChar(254), username);
      request.input('companyHash', sql.Char(4), companyHash);
      const result = await request.query(queryString);

      let response = true;

      if (result.recordset && result.recordset.length > 0) {
        response = !!result.recordset[0].IsLocked;
      }

      return {
        isLocked: response,
      };
    } catch (error) {
      this.loggingService.log(ApplicationLogs.CouldNotGetApplicationMembershipFromSql, { query: queryString }, error);
      throw error;
    }
  }
}
