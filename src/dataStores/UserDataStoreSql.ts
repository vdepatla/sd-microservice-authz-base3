import { User } from '../models/User';

/**
 * Interface for the User data store.
 */
export interface UserDataStoreSql {
  getUser(companyHash: string, username: string): Promise<User>;
}
