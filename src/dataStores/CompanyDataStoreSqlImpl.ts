import 'reflect-metadata';
import { CompanyDataStoreSql } from './CompanyDataStoreSql';
import { injectable, inject } from 'inversify';
import { Company } from '../models/Company';
import * as sql from 'mssql';
import { DataStore } from '../models/DataStore';
import {
  errorCounter,
  histogram,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  LoggingService,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { ConnectionPoolManager } from '../sql/ConnectionPoolManager';

@injectable()
export class CompanyDataStoreSqlImpl implements CompanyDataStoreSql {
  public static metricLabels = {
    [MetricLabels.DataStore]: DataStore.CompanyDataStoreSql,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.CoreOLTP,
  };

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @inject(InversifyTypes.ConnectionPoolManager)
  private connectionPoolManager: ConnectionPoolManager;

  @errorCounter(CompanyDataStoreSqlImpl.metricLabels)
  @histogram(CompanyDataStoreSqlImpl.metricLabels)
  public async getCompanies(companyIds: string[]): Promise<Company[]> {
    const functionName = 'CompanyDataStoreSqlImpl.getCompanyName';
    let query;

    try {
      const pool = await this.connectionPoolManager.getReadOnlyConnectionPool();
      const req = new sql.Request(pool);

      // Because we need to add each value individually, we're going to
      // create an object that looks like
      // {@companyIds0: companyIds[0], @companyIds1: companyIds[1], ...}
      const keyValueCompanyIds = companyIds.reduce<{ [key: string]: string }>((obj, item, idx) => {
        obj[`@companyIds${idx}`] = item;
        return obj;
      }, {});

      const keyString = Object.keys(keyValueCompanyIds).join(',');

      // const companyIdsForQuery = companyIds.map(x => `'${x}'`).join(',');

      // tslint:disable-next-line:no-multiline-string
      query = `SELECT CompanyId, CompanyName
                     FROM Company (NOLOCK) c 
                     WHERE c.CompanyId in (${keyString})
                     `;

      // Now let's add each value as input
      Object.keys(keyValueCompanyIds).forEach(key => {
        // The slice is to strip the '@' since that goofs things up
        req.input(key.slice(1), keyValueCompanyIds[key]);
      });

      const result = await req.query(query);
      const companyData: Company[] = result.recordset.map(rs => {
        return {
          CompanyName: rs.CompanyName,
          EntityIdentityKey: rs.CompanyId,
        };
      });

      return companyData;
    } catch (error) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetCompaniesFromSql,
        {
          functionName,
          companyIds,
          query,
        },
        error
      );

      throw error;
    }
  }
}
