import 'reflect-metadata';
import * as sql from 'mssql';
import { Vehicle } from '../models/Vehicle';
import { injectable, inject } from 'inversify';
import { VehicleDataStore } from './VehicleDataStore';
import { Config } from '../Config';
import { DataStore } from '../models/DataStore';
import {
  histogram,
  logExecution,
  MetricLabels,
  ExternalResourceActions,
  ExternalResources,
  DALLabelValues,
  errorCounter,
  LoggingService,
  LogLevel,
} from '@sd-microservice-base/instrumentation';
import { InversifyTypes } from '../models/InversifyTypes';
import { ApplicationLogs } from '../models/ApplicationLogs';
import { ConnectionPoolManager } from '../sql/ConnectionPoolManager';

/**
 * Used to retrieve vehicle information
 */
@injectable()
export class VehicleDataStoreImpl implements VehicleDataStore {
  public static metricsLabels: DALLabelValues = {
    [MetricLabels.DataStore]: DataStore.VehicleDataStore,
    [MetricLabels.ExternalResourceAction]: ExternalResourceActions.Read,
    [MetricLabels.ExternalResourceLocation]: ExternalResources.CoreOLTP,
  };

  @inject(InversifyTypes.Config)
  private config: Config;

  @inject(InversifyTypes.LoggingService)
  private loggingService: LoggingService;

  @inject(InversifyTypes.ConnectionPoolManager)
  private connectionPoolManager: ConnectionPoolManager;

  /**
   * Retrieves vehicle info given a vehicle ID.
   * @param vehicleId The vehicle ID.
   * @returns Vehicle for requested vehicle
   */

  @histogram(VehicleDataStoreImpl.metricsLabels)
  @errorCounter(VehicleDataStoreImpl.metricsLabels)
  @logExecution(LogLevel.Debug, true)
  public async getVehicles(vehicleIds: string[]): Promise<Vehicle[]> {
    const functionName = 'VehicleDataStoreImpl.getVehicles';

    try {
      const conn = await this.connectionPoolManager.getReadOnlyConnectionPool();
      const req = new sql.Request(conn);

      // Because we need to add each value individually, we're going to
      // create an object that looks like
      // {@vehicleIds0: vehicleIds[0], @vehicleIds1: vehicleIds[1], ...}
      const keyValueVehicleIds = vehicleIds.reduce<{ [key: string]: string }>((obj, item, idx) => {
        obj[`@vehicleIds${idx}`] = item;
        return obj;
      }, {});
      const keyString = Object.keys(keyValueVehicleIds).join(',');
      const query = `SELECT
                VehicleId,
                VehicleSN,
                VIN,
                SiteId
            FROM dbo.Vehicle v (NOLOCK)
            WHERE v.IsActive = 1 AND v.VehicleId IN (${keyString})`;

      // Now let's add each value as input
      Object.keys(keyValueVehicleIds).forEach(key => {
        // The slice is to strip the '@' since that goofs things up
        req.input(key.slice(1), keyValueVehicleIds[key]);
      });
      const result = await req.query(query);
      const vehicles = result.recordset.map(rs => {
        return {
          vehicleId: rs.VehicleId.toString(),
          vehicleSn: rs.VehicleSN,
          vehicleVin: rs.VIN,
          siteId: rs.SiteId,
        };
      });
      return vehicles.length !== 0 ? vehicles : [];
    } catch (err) {
      this.loggingService.log(
        ApplicationLogs.CouldNotGetVehicles,
        {
          functionName,
          db: {
            host: this.config.SQLServer,
            name: this.config.SQLDatabase,
          },
        },
        err
      );
      throw err;
    }
  }
}
