import { AccessResult } from "../models/AccessResult";
import { User } from "../models/User";
import { Site } from "../models/Site";

export interface AccessDataStore {
  canUserAccessCompanies(
    companyIds: string[],
    userInfo: User,
    superuser: boolean
  ): Promise<AccessResult[]>;
  canUserAccessSites(
    siteIds: string[],
    childSites: Site[],
    superuser: boolean
  ): Promise<AccessResult[]>;
  canUserAccessVehicles(
    vehicleIds: string[],
    childSites: Site[],
    superuser: boolean
  ): Promise<AccessResult[]>;
  canUserAccessSmartRecorders(
    smartRecorderIds: string[],
    smartRecorderSns: string[],
    childSites: Site[],
    superuser: boolean
  ): Promise<AccessResult[]>;
}
