pipeline {
    agent { label 'Node' }

    environment {
        VERSION = "1.0.0"
        APPNAME = "sd-microservice-authorization"
        SONARPROJECTKEY = "${APPNAME}"
        SONARSOURCES = "src"
        SONARCOVERAGEEXCLUSIONS = "src/**/Config.ts,src/*config.ts,src/**/types.ts,src/**/server.ts,src/models/**"
    }

    stages {
      stage('Init') {
        steps {
          script {
            sh "set -x"
            // create full version // take first 7 chars of the Git commit
            fullVersion = env.VERSION + '-' + env.GIT_COMMIT.substring(0,7) + '-' + env.BUILD_NUMBER
            println "fullVersion=" + fullVersion
            currentBuild.displayName = fullVersion

            // set prBuild bool
            if (env.BRANCH_NAME.toLowerCase().startsWith("pr-")) {
              prBuild = true;
              println "set prBuild to true"
            } else {
              prBuild = false;
            }
            println "prBuild=" + prBuild

            // set releaseBuild bool
            if (prBuild != true) {
              if (env.BRANCH_NAME.toLowerCase().startsWith("release/")) { // this is a release build (i.e. it will go to Integration env)
                releaseBuild = true;
                println "set releaseBuild to true"
                dockerUrl = env.DOCKER_REGISTRY + '/' + env.CD_PROJECT_INT + '/' + env.APPNAME + ':' + fullVersion
                println "set dockerUrl to " + dockerUrl
              } else { // this is not a release build (i.e. it will go to Devci env)
                releaseBuild = false;
                // although we set dockerUrl here, it doesn't mean it has to be used (i.e. a PR build)
                dockerUrl = env.DOCKER_REGISTRY + '/' + env.CD_PROJECT_DEVCI + '/' + env.APPNAME + ':' + fullVersion
                println "set dockerUrl to " + dockerUrl
              }
            }
          }

          sh 'printenv'
        }
      }

      stage('UnitTest') {
          steps {
            sh "npx yarn --pure-lockfile"
            sh 'npx yarn run test:coverage'
          }
      }

      stage('Build') {
        steps {
          sh "npx yarn --pure-lockfile"
          sh "NODE_ENV=production npx yarn run build"
          // Remove node_modules and install just the production dependencies, for a smaller bundle size.
          sh "echo rm -rf node_modules"
          sh "npx yarn --pure-lockfile --production"
        }
      }


      stage('SonarQubeScanner') {
          // https://github.com/SonarSource/SonarTS-example
        steps {
            script {
                scannerHome = tool "${SONARQUBE_SCANNER_TOOL}";
            }
            withSonarQubeEnv("${SONARQUBE_ENV}") {
                sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=${SONARPROJECTKEY} -Dsonar.sources=${SONARSOURCES} -Dsonar.typescript.lcov.reportPaths=coverage/lcov.info -Dsonar.coverage.exclusions=${SONARCOVERAGEEXCLUSIONS}"
            }
        }
      }

      stage('DockerBuild') {
        when {
          expression {
            prBuild != true;
          }
        }
        steps {
          sh 'docker build -t=' + dockerUrl + ' .'
        }
      }
      
      stage('S3HelmChartPush') {
        when {
          expression {
            prBuild != true;
          }
        }
        steps {
          sh 'helm package deploy/${APPNAME} --app-version ' + fullVersion + ' --version ' + fullVersion
          sh 'aws s3 cp ${APPNAME}-' + fullVersion + '.tgz s3://${HELM_CHARTS_S3_BUCKET}/${APPNAME}/${APPNAME}-' + fullVersion + '.tgz --profile ${HELM_CHARTS_S3_PROFILE}'
        }
      }

      stage('DockerPush') {
        when {
          expression {
            prBuild != true;
          }
        }
        steps {
          sh 'docker push ' + dockerUrl
          sh 'docker rmi ' + dockerUrl
        }
      }

      stage('Cleanup') {
        steps {
          // TODO clean up build output (however the Dockerfile is taking everything from the workspace (aka .))
          sh 'echo rm -rf build'
        }
      }
    }

    post {
      always {
          echo "Entered post-build section"
          junit '.junit_output/test-results.xml'
          sh "set +x"
      }
      success {
          slackSend (color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] - v${fullVersion}' (${env.BUILD_URL})")
      }
      failure {
          slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] - v${fullVersion}' (${env.BUILD_URL})")
      }
    }
}