# Smartdrive microservice: sd-microservice-new-authz



## Quick Start Instructions

### Instructions to run this project locally

1. Install development dependencies using yarn into this project's directory. 

`yarn install`

2. Start the webpack development script to watch your changes as you work.

`yarn build:dev:watch`

3. Start the dev server using NPM scripts, which will watch for changes and reload after webpack builds.

`yarn start:dev`

4. Use the server. By default, the server will run on port 3000. Access `http://localhost:3000/gql` to introspect the schema.

### Instructions to "test" this project

1. make sure "yarn" has been run on the project so that jest (test runner) is installed
2. from the command line run `yarn test`, or `yarn test:dev`.

### Instructions to run from a docker container

1. Make sure `yarn` has been run on the project and `yarn build` has created `dist/server.js`.
2. from the command line run

    ```Shell
    docker build -f devops/Dockerfile -t sd-microservice-yourservicename .
    ```

3. from the command line run

    ```Shell
    docker run -p 3001:3001 sd-microservice-yourservicename
    ```

3. confirm that the service is running: from any browser navigate to <http://localhost:3001/status>